package com.mky.mkcompany.service;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.io.Serializable;
import java.util.List;

@Transactional
@Service
public abstract class BaseService<T, ID extends Serializable> {
	abstract protected JpaRepository<T, ID> getDao();

	public T get(ID id) {
		return getDao().findOne(id);
	}

	public void save(T entity) {
		getDao().save(entity);
	}

	public void update(T entity) {
		getDao().save(entity);
	}

	public void delete(T entity) {
		getDao().delete(entity);
	}


	public List<T> findAll() {
		return getDao().findAll();
	}
}
