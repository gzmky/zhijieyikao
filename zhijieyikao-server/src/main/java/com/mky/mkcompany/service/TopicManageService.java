package com.mky.mkcompany.service;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.mky.mkcompany.controller.domain.*;
import com.mky.mkcompany.dao.*;
import com.mky.mkcompany.exception.WafException;
import com.mky.mkcompany.model.*;
import com.mky.mkcompany.tool.DateUtil;
import com.mky.mkcompany.tool.ReadExcel;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.util.ObjectUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.persistence.EntityManager;
import javax.servlet.http.HttpServletRequest;
import java.sql.Timestamp;
import java.util.*;

/**
 * Created by Lenovo on 2017/9/1.
 */
@Service
public class TopicManageService {
    private final Logger LOGGER = LoggerFactory.getLogger(getClass());
    @Autowired
    private QuestTypeDao questTypeDao;
    @Autowired
    private TestInfoDao testInfoDao;
    @Autowired
    private SubjectChapterDao subjectChapterDao;
    @Autowired
    private SubjectCategoryDao subjectCategoryDao;

    @Autowired
    private EntityManager em;

    @Autowired
    private TestQuestDao testQuestDao;
    @Autowired
    private PracticQuestDao practicQuestDao;
    @Autowired
    private CourseSubjectDao courseSubjectDao;
    @Autowired
    private CourseCategoryDao courseCategoryDao;
    @Autowired
    private CourseInfoDao courseInfoDao;
    @Autowired
    private CourseQuestDao courseQuestDao;

    @Autowired
    private UserDoPracticQuestDao userDoPracticQuestDao;
    @Autowired
    private UserCollectPracticQuestDao userCollectPracticQuestDao;
    @Autowired
    private UserTestDao userTestDao;
    @Autowired
    private UserDoTestDao userDoTestDao;
    @Autowired
    private UserDoTestQuestDao userDoTestQuestDao;
    @Autowired
    private UserDoTestSaveQuestDao userDoTestSaveQuestDao;
    @Autowired
    private UserCourseDao userCourseDao;
    @Autowired
    private UserDoCourseDao userDoCourseDao;
    @Autowired
    private UserDoCourseQuestDao userDoCourseQuestDao;
    @Autowired
    private UserDoCourseSaveQuestDao userDoCourseSaveQuestDao;


    public JSONObject getQuestTypeList(String name,int page,int size){
        JSONObject res = new JSONObject();
        Sort sort=new Sort(Sort.Direction.DESC,"createTime");
        Pageable pageable = new PageRequest(page-1, size, sort);
        Page<QuestType> questTypePage=null;
        if("all".equals(name)){
            questTypePage=questTypeDao.findAll(pageable);
        }else {
            questTypePage=questTypeDao.findByName(name,pageable);
        }
        res.put("data",questTypePage.getContent());
        res.put("total",questTypePage.getTotalElements());
        return res;
    }

    public JSONObject saveQuestType(QuestType questTypeReq){
        JSONObject res = new JSONObject();
        QuestType questTypeOld=questTypeDao.findById(questTypeReq.getId());
        QuestType questTypeOldName=questTypeDao.findByName(questTypeReq.getName());
        if(questTypeOldName!=null && !questTypeOldName.getId().equals(questTypeReq.getId())){
            throw new WafException("", "题型重复！", HttpStatus.NOT_ACCEPTABLE);
        }
        if(questTypeOld==null){
            QuestType questType=new QuestType();
            questType.setId(UUID.randomUUID().toString().replace("-",""));
            questType.setName(questTypeReq.getName());
            questType.setCreateTime(DateUtil.getNowTimestamp());
            questType.setUpdateTime(DateUtil.getNowTimestamp());
            questTypeDao.save(questType);
        }else {
            questTypeOld.setName(questTypeReq.getName());
            questTypeOld.setUpdateTime(DateUtil.getNowTimestamp());
            questTypeDao.save(questTypeOld);
        }
        return res;
    }

    public void delQuestType(String id){
        questTypeDao.delete(id);
    }



    public JSONObject getSubjectCategoryList(String name,String examType,int page,int size){
        JSONObject res = new JSONObject();
        Sort sort=new Sort(Sort.Direction.DESC,"createTime");
        Pageable pageable = new PageRequest(page-1, size, sort);
        Page<SubjectCategory> subjectCategoryPage=null;
        if("all".equals(name)&&"all".equals(examType)){
            subjectCategoryPage=subjectCategoryDao.findAll(pageable);
        }else if(!"all".equals(name)&&!"all".equals(examType)){
            subjectCategoryPage=subjectCategoryDao.findByNameLikeAndExamType(name,examType,pageable);
        }else if("all".equals(name)&&!"all".equals(examType)){
            subjectCategoryPage=subjectCategoryDao.findByExamType(examType,pageable);
        }else if(!"all".equals(name)&&"all".equals(examType)){
            subjectCategoryPage=subjectCategoryDao.findByNameLike(name,pageable);
        }
        res.put("data",subjectCategoryPage.getContent());
        res.put("total",subjectCategoryPage.getTotalElements());
        return res;
    }

    public JSONObject saveSubjectCategory(SubjectCategory subjectCategoryReq){
        JSONObject res = new JSONObject();
        SubjectCategory subjectCategoryOld=subjectCategoryDao.findById(subjectCategoryReq.getId());
        SubjectCategory subjectCategoryOldName=subjectCategoryDao.findByNameAndExamType(subjectCategoryReq.getName(),subjectCategoryReq.getExamType());
        if(subjectCategoryOldName!=null && !subjectCategoryOldName.getId().equals(subjectCategoryReq.getId())){
            throw new WafException("", "科目分类重复！", HttpStatus.NOT_ACCEPTABLE);
        }
        if(subjectCategoryOld==null){
            SubjectCategory subjectCategory=new SubjectCategory();
            subjectCategory.setId(UUID.randomUUID().toString().replace("-",""));
            subjectCategory.setName(subjectCategoryReq.getName());
            subjectCategory.setExamType(subjectCategoryReq.getExamType());
            subjectCategory.setCreateTime(DateUtil.getNowTimestamp());
            subjectCategory.setUpdateTime(DateUtil.getNowTimestamp());
            subjectCategoryDao.save(subjectCategory);
        }else{
            subjectCategoryOld.setName(subjectCategoryReq.getName());
            subjectCategoryOld.setExamType(subjectCategoryReq.getExamType());
            subjectCategoryOld.setUpdateTime(DateUtil.getNowTimestamp());
            subjectCategoryDao.save(subjectCategoryOld);
        }
        return res;
    }

    public void delSubjectCategory(String id){
        List<SubjectChapter> subjectChapterList=subjectChapterDao.findBySubjectId(id);
        List<String> chapterIds=new ArrayList<>();
        for(SubjectChapter subjectChapter:subjectChapterList){
            chapterIds.add(subjectChapter.getId());
        }

        List<PracticQuest> practicQuestList=practicQuestDao.findByChapterIdIn(chapterIds);
        List<String> questIds=new ArrayList<>();
        for(PracticQuest practicQuest:practicQuestList){
            questIds.add(practicQuest.getId());
        }
        if(questIds.size()!=0){
            userDoPracticQuestDao.deleteByQuestId(questIds);
            userCollectPracticQuestDao.deleteByQuestIds(questIds);
            userDoTestQuestDao.deleteByQuestIds(questIds);
            userDoCourseQuestDao.deleteByQuestIds(questIds);
        }
        if(chapterIds.size()!=0){
            practicQuestDao.deleteByChapterIds(chapterIds);
            testQuestDao.deleteByChapterIds(chapterIds);
        }
        subjectChapterDao.deleteBySubjectId(id);
        subjectCategoryDao.deleteById(id);
    }

    public JSONObject getSubjectChapterList(String examType,String subjectId,int page,int size){
        JSONObject res = new JSONObject();
        Sort sort=new Sort(Sort.Direction.DESC,"createTime");
        Pageable pageable = new PageRequest(page-1, size, sort);
        Page<SubjectChapter> subjectChapterPage=null;
        List<SubjectChapter> subjectChapterList=new ArrayList<>();
        if("all".equals(examType)&&"all".equals(subjectId)){
            subjectChapterPage=subjectChapterDao.findAll(pageable);
            subjectChapterList=subjectChapterPage.getContent();
        }else if(!"all".equals(examType)&&!"all".equals(subjectId)){
            SubjectCategory subjectCategory=subjectCategoryDao.findByIdAndExamType(subjectId,examType);
            subjectChapterList=subjectChapterDao.findBySubjectId(subjectCategory.getId());
        }else if("all".equals(examType)&&!"all".equals(subjectId)){
            SubjectCategory subjectCategory=subjectCategoryDao.findById(subjectId);
            subjectChapterList=subjectChapterDao.findBySubjectId(subjectCategory.getId());
        }else if(!"all".equals(examType)&&"all".equals(subjectId)){
            List<SubjectCategory> subjectCategories=subjectCategoryDao.findByExamType(examType);
            List<String> subjectIds=new ArrayList<>();
            for(SubjectCategory subjectCategory:subjectCategories){
                subjectIds.add(subjectCategory.getId());
            }
            subjectChapterList=subjectChapterDao.findBySubjectIdIn(subjectIds);
        }
        List<String> subjectIds=new ArrayList<>();
        for(SubjectChapter subjectChapter:subjectChapterList){
            subjectIds.add(subjectChapter.getSubjectId());
        }
        JSONObject jsonSubjectId=new JSONObject();
        List<SubjectCategory> subjectCategoryList=subjectCategoryDao.findByIdIn(subjectIds);
        for(SubjectCategory subjectCategory:subjectCategoryList){
            jsonSubjectId.put(subjectCategory.getId(),subjectCategory);
        }
        List<SubjectChapterRes> subjectChapterResList=new ArrayList<>();
        for(SubjectChapter subjectChapter:subjectChapterList){
            SubjectChapterRes subjectChapterRes=new SubjectChapterRes();
            SubjectCategory subjectCategory=(SubjectCategory)jsonSubjectId.get(subjectChapter.getSubjectId());
            subjectChapterRes.setExamType(subjectCategory.getExamType());
            subjectChapterRes.setSubjectId(subjectCategory.getId());
            subjectChapterRes.setSubjectName(subjectCategory.getName());
            subjectChapterRes.setId(subjectChapter.getId());
            subjectChapterRes.setName(subjectChapter.getName());
            subjectChapterRes.setCreateTime(subjectChapter.getCreateTime());
            subjectChapterRes.setUpdateTime(subjectChapter.getUpdateTime());
            subjectChapterResList.add(subjectChapterRes);
        }
        int offset=(page-1)*size;
        int limit=page*size;
        int total=subjectChapterResList.size();
        if(limit>total){
            limit=total;
        }
        List<SubjectChapterRes>data=new ArrayList<>();
        if(total>0 && offset<limit) {
            data = subjectChapterResList.subList(offset, limit);
        }
        res.put("data",data);
        res.put("total",total);
        return res;
    }

    public JSONObject saveSubjectChapter(SubjectChapter subjectChapterReq){
        JSONObject res = new JSONObject();
        SubjectChapter subjectChapterOld=subjectChapterDao.findById(subjectChapterReq.getId());
        SubjectChapter subjectChapterOldName=subjectChapterDao.findByNameAndSubjectId(subjectChapterReq.getName(),subjectChapterReq.getSubjectId());
        if(subjectChapterOldName!=null && !subjectChapterOldName.getId().equals(subjectChapterReq.getId())){
            throw new WafException("", "章节名称重复！", HttpStatus.NOT_ACCEPTABLE);
        }
        if(subjectChapterOld==null){
            SubjectChapter subjectChapter=new SubjectChapter();
            subjectChapter.setId(UUID.randomUUID().toString().replace("-",""));
            subjectChapter.setName(subjectChapterReq.getName());
            subjectChapter.setSubjectId(subjectChapterReq.getSubjectId());
            subjectChapter.setCreateTime(DateUtil.getNowTimestamp());
            subjectChapter.setUpdateTime(DateUtil.getNowTimestamp());
            subjectChapterDao.save(subjectChapter);
        }else{
            subjectChapterOld.setName(subjectChapterReq.getName());
            subjectChapterOld.setSubjectId(subjectChapterReq.getSubjectId());
            subjectChapterOld.setUpdateTime(DateUtil.getNowTimestamp());
            subjectChapterDao.save(subjectChapterOld);
        }
        return res;
    }

    public void delSubjectChapter(String id){
        List<PracticQuest> practicQuestList=practicQuestDao.findByChapterId(id);
        List<String> questIds=new ArrayList<>();
        for(PracticQuest practicQuest:practicQuestList){
            questIds.add(practicQuest.getId());
        }
        if(questIds.size()!=0){
            userDoPracticQuestDao.deleteByQuestId(questIds);
            userCollectPracticQuestDao.deleteByQuestIds(questIds);
            userDoTestQuestDao.deleteByQuestIds(questIds);
            userDoCourseQuestDao.deleteByQuestIds(questIds);
        }
        practicQuestDao.deleteByChapterId(id);
        testQuestDao.deleteByChapterId(id);
        subjectChapterDao.deleteById(id);
    }

    public JSONObject getTestInfoList(String name,String examType,int page,int size){
        JSONObject res = new JSONObject();
        Sort sort=new Sort(Sort.Direction.DESC,"createTime");
        Pageable pageable = new PageRequest(page-1, size, sort);
        Page<TestInfo> testInfoPage=null;
        List<TestInfo> testInfoList=new ArrayList<>();
        if("all".equals(name)&&"all".equals(examType)){
            testInfoPage=testInfoDao.findAll(pageable);
        }else if(!"all".equals(name)&&!"all".equals(examType)){
            testInfoPage=testInfoDao.findByNameLikeAndExamType(name,examType,pageable);
        }else if("all".equals(name)&&!"all".equals(examType)){
            testInfoPage=testInfoDao.findByExamType(examType,pageable);
        }else if(!"all".equals(name)&&"all".equals(examType)){
            testInfoPage=testInfoDao.findByNameLike(name,pageable);
        }
        testInfoList=testInfoPage.getContent();
        if(testInfoList.size()==0){
            testInfoList=new ArrayList<>();
        }
        res.put("data",testInfoList);
        res.put("total",testInfoPage.getTotalElements());
        return res;
    }

    public JSONObject saveTestInfo(TestInfo testInfoReq){
        JSONObject res = new JSONObject();
        TestInfo testInfoOld=testInfoDao.findById(testInfoReq.getId());
        TestInfo testInfoOldName=testInfoDao.findByNameAndExamType(testInfoReq.getName(),testInfoReq.getExamType());
        if(testInfoOldName!=null && !testInfoOldName.getId().equals(testInfoReq.getId())){
            throw new WafException("", "试卷名称重复！", HttpStatus.NOT_ACCEPTABLE);
        }
        if(testInfoOld==null){
            TestInfo testInfo=new TestInfo();
            testInfo.setId(UUID.randomUUID().toString().replace("-",""));
            testInfo.setName(testInfoReq.getName());
            testInfo.setExamType(testInfoReq.getExamType());
            testInfo.setTestScore(testInfoReq.getTestScore());
            testInfo.setPassScore(testInfoReq.getPassScore());
            testInfo.setTestTime(testInfoReq.getTestTime());
            testInfo.setCreateTime(DateUtil.getNowTimestamp());
            testInfo.setUpdateTime(DateUtil.getNowTimestamp());
            testInfoDao.save(testInfo);
        }else{
            testInfoOld.setName(testInfoReq.getName());
            testInfoOld.setExamType(testInfoReq.getExamType());
            testInfoOld.setTestScore(testInfoReq.getTestScore());
            testInfoOld.setPassScore(testInfoReq.getPassScore());
            testInfoOld.setTestTime(testInfoReq.getTestTime());
            testInfoOld.setUpdateTime(DateUtil.getNowTimestamp());
            testInfoDao.save(testInfoOld);
        }
        return res;
    }

    public void delTestInfo(String id){
        userDoTestQuestDao.deleteByTestId(id);
        userDoTestSaveQuestDao.deleteByTestId(id);
        testQuestDao.deleteByTestId(id);
        testInfoDao.deleteById(id);
    }

    public void delQuestList(String id,String type){
        if("模拟试卷题库".equals(type)){
            testQuestDao.deleteById(id);
        }else if("练习题库表".equals(type)){
            userDoPracticQuestDao.deleteByQuestId(id);
            userCollectPracticQuestDao.deleteByQuestId(id);
            userDoTestQuestDao.deleteByQuestId(id);
            userDoCourseQuestDao.deleteByQuestId(id);
            practicQuestDao.deleteById(id);
        }else if("课程".equals(type)){
            courseQuestDao.deleteById(id);
        }
    }


    public JSONObject saveQuest(QuestReq questReq){
        JSONObject res = new JSONObject();
        String[] optionTypes=questReq.getRightAnswer().split(",");
        if("模拟试卷题库".equals(questReq.getType())){
            TestQuest testQuestOld=testQuestDao.findById(questReq.getId());
            TestQuest testQuestOldOldName=testQuestDao.findByTitleAndExamType(questReq.getTitle(),questReq.getExamType());
            if(testQuestOldOldName!=null && !testQuestOldOldName.getId().equals(questReq.getId())){
                throw new WafException("", "题目标题重复！", HttpStatus.NOT_ACCEPTABLE);
            }
            if(testQuestOld==null){
                TestQuest testQuest=new TestQuest();
                testQuest.setId(UUID.randomUUID().toString().replace("-",""));
                testQuest.setType(questReq.getType());
                testQuest.setTestId(questReq.getTestId());
                testQuest.setExamType(questReq.getExamType());
                testQuest.setQuestType(questReq.getQuestType());
                testQuest.setTitle(questReq.getTitle());
                testQuest.setDescribes(questReq.getDescribes());
                testQuest.setTitlePicUrl(questReq.getTitlePicUrl());
                testQuest.setOptiona(questReq.getOptiona());
                testQuest.setOptionb(questReq.getOptionb());
                testQuest.setOptionc(questReq.getOptionc());
                testQuest.setOptiond(questReq.getOptiond());
                testQuest.setOptione(questReq.getOptione());
                if(optionTypes.length!=1){
                    testQuest.setOptionType(2);
                }else {
                    testQuest.setOptionType(1);
                }
                testQuest.setRightAnswer(questReq.getRightAnswer());
                testQuest.setAnswerParse(questReq.getAnswerParse());
                testQuest.setTestCentre(questReq.getTestCentre());
                testQuest.setChapterId(questReq.getChapterId());
                testQuest.setCreateTime(DateUtil.getNowTimestamp());
                testQuest.setUpdateTime(DateUtil.getNowTimestamp());
                testQuestDao.save(testQuest);
            }else {
                testQuestOld.setExamType(questReq.getExamType());
                testQuestOld.setType(questReq.getType());
                testQuestOld.setTestId(questReq.getTestId());
                testQuestOld.setQuestType(questReq.getQuestType());
                testQuestOld.setTitle(questReq.getTitle());
                testQuestOld.setDescribes(questReq.getDescribes());
                testQuestOld.setTitlePicUrl(questReq.getTitlePicUrl());
                testQuestOld.setOptiona(questReq.getOptiona());
                testQuestOld.setOptionb(questReq.getOptionb());
                testQuestOld.setOptionc(questReq.getOptionc());
                testQuestOld.setOptiond(questReq.getOptiond());
                testQuestOld.setOptione(questReq.getOptione());
                if(optionTypes.length!=1){
                    testQuestOld.setOptionType(2);
                }else {
                    testQuestOld.setOptionType(1);
                }
                testQuestOld.setRightAnswer(questReq.getRightAnswer());
                testQuestOld.setAnswerParse(questReq.getAnswerParse());
                testQuestOld.setTestCentre(questReq.getTestCentre());
                testQuestOld.setChapterId(questReq.getChapterId());
                testQuestOld.setUpdateTime(DateUtil.getNowTimestamp());
                testQuestDao.save(testQuestOld);
            }
        }else if("练习题库表".equals(questReq.getType())){
            PracticQuest practicQuestOld=practicQuestDao.findById(questReq.getId());
            PracticQuest practicQuestOldName=practicQuestDao.findByTitleAndExamType(questReq.getTitle(),questReq.getExamType());
            if(practicQuestOldName!=null && !practicQuestOldName.getId().equals(questReq.getId())){
                throw new WafException("", "题目标题重复！", HttpStatus.NOT_ACCEPTABLE);
            }
            if(practicQuestOld==null){
                PracticQuest practicQuest=new PracticQuest();
                practicQuest.setId(UUID.randomUUID().toString().replace("-",""));
                practicQuest.setType(questReq.getType());
                practicQuest.setExamType(questReq.getExamType());
                practicQuest.setQuestType(questReq.getQuestType());
                practicQuest.setTitle(questReq.getTitle());
                practicQuest.setDescribes(questReq.getDescribes());
                practicQuest.setTitlePicUrl(questReq.getTitlePicUrl());
                practicQuest.setOptiona(questReq.getOptiona());
                practicQuest.setOptionb(questReq.getOptionb());
                practicQuest.setOptionc(questReq.getOptionc());
                practicQuest.setOptiond(questReq.getOptiond());
                practicQuest.setOptione(questReq.getOptione());
                if(optionTypes.length!=1){
                    practicQuest.setOptionType(2);
                }else {
                    practicQuest.setOptionType(1);
                }
                practicQuest.setRightAnswer(questReq.getRightAnswer());
                practicQuest.setAnswerParse(questReq.getAnswerParse());
                practicQuest.setTestCentre(questReq.getTestCentre());
                practicQuest.setChapterId(questReq.getChapterId());
                practicQuest.setCreateTime(DateUtil.getNowTimestamp());
                practicQuest.setUpdateTime(DateUtil.getNowTimestamp());
                practicQuestDao.save(practicQuest);
            }else {
                practicQuestOld.setExamType(questReq.getExamType());
                practicQuestOld.setType(questReq.getType());
                practicQuestOld.setQuestType(questReq.getQuestType());
                practicQuestOld.setTitle(questReq.getTitle());
                practicQuestOld.setDescribes(questReq.getDescribes());
                practicQuestOld.setTitlePicUrl(questReq.getTitlePicUrl());
                practicQuestOld.setOptiona(questReq.getOptiona());
                practicQuestOld.setOptionb(questReq.getOptionb());
                practicQuestOld.setOptionc(questReq.getOptionc());
                practicQuestOld.setOptiond(questReq.getOptiond());
                practicQuestOld.setOptione(questReq.getOptione());
                if(optionTypes.length!=1){
                    practicQuestOld.setOptionType(2);
                }else {
                    practicQuestOld.setOptionType(1);
                }
                practicQuestOld.setRightAnswer(questReq.getRightAnswer());
                practicQuestOld.setAnswerParse(questReq.getAnswerParse());
                practicQuestOld.setTestCentre(questReq.getTestCentre());
                practicQuestOld.setChapterId(questReq.getChapterId());
                practicQuestOld.setUpdateTime(DateUtil.getNowTimestamp());
                practicQuestDao.save(practicQuestOld);
            }
        }else if("课程".equals(questReq.getType())){
            CourseInfo courseInfo=courseInfoDao.findById(questReq.getCourseId());
            if(courseInfo==null){
                throw new WafException("", "不存在该课程信息！", HttpStatus.NOT_ACCEPTABLE);
            }
            CourseQuest courseQuestOldName=courseQuestDao.findByTitleAndExamType(questReq.getTitle(),questReq.getExamType());
            if(courseQuestOldName!=null && !courseQuestOldName.getId().equals(questReq.getId())){
                throw new WafException("", "题目标题重复！", HttpStatus.NOT_ACCEPTABLE);
            }
            if(courseQuestOldName==null){
                CourseQuest courseQuest=new CourseQuest();
                courseQuest.setId(UUID.randomUUID().toString().replace("-",""));
                courseQuest.setType(questReq.getType());
                courseQuest.setExamType(questReq.getExamType());
                courseQuest.setCourseId(questReq.getCourseId());
                courseQuest.setQuestType(questReq.getQuestType());
                courseQuest.setTitle(questReq.getTitle());
                courseQuest.setDescribes(questReq.getDescribes());
                courseQuest.setTitlePicUrl(questReq.getTitlePicUrl());
                courseQuest.setOptiona(questReq.getOptiona());
                courseQuest.setOptionb(questReq.getOptionb());
                courseQuest.setOptionc(questReq.getOptionc());
                courseQuest.setOptiond(questReq.getOptiond());
                courseQuest.setOptione(questReq.getOptione());
                if(optionTypes.length!=1){
                    courseQuest.setOptionType(2);
                }else {
                    courseQuest.setOptionType(1);
                }
                courseQuest.setRightAnswer(questReq.getRightAnswer());
                courseQuest.setAnswerParse(questReq.getAnswerParse());
                courseQuest.setTestCentre(questReq.getTestCentre());
                courseQuest.setChapterId(questReq.getChapterId());
                courseQuest.setCreateTime(DateUtil.getNowTimestamp());
                courseQuest.setUpdateTime(DateUtil.getNowTimestamp());
                courseQuestDao.save(courseQuest);
            }else {
                courseQuestOldName.setExamType(questReq.getExamType());
                courseQuestOldName.setType(questReq.getType());
                courseQuestOldName.setCourseId(questReq.getCourseId());
                courseQuestOldName.setQuestType(questReq.getQuestType());
                courseQuestOldName.setTitle(questReq.getTitle());
                courseQuestOldName.setDescribes(questReq.getDescribes());
                courseQuestOldName.setTitlePicUrl(questReq.getTitlePicUrl());
                courseQuestOldName.setOptiona(questReq.getOptiona());
                courseQuestOldName.setOptionb(questReq.getOptionb());
                courseQuestOldName.setOptionc(questReq.getOptionc());
                courseQuestOldName.setOptiond(questReq.getOptiond());
                courseQuestOldName.setOptione(questReq.getOptione());
                if(optionTypes.length!=1){
                    courseQuestOldName.setOptionType(2);
                }else {
                    courseQuestOldName.setOptionType(1);
                }
                courseQuestOldName.setRightAnswer(questReq.getRightAnswer());
                courseQuestOldName.setAnswerParse(questReq.getAnswerParse());
                courseQuestOldName.setTestCentre(questReq.getTestCentre());
                courseQuestOldName.setChapterId(questReq.getChapterId());
                courseQuestOldName.setUpdateTime(DateUtil.getNowTimestamp());
                courseQuestDao.save(courseQuestOldName);
            }
        }else {
            throw new WafException("", "请点击题目归属", HttpStatus.NOT_ACCEPTABLE);
        }
        return res;
    }


    public JSONObject getQuestList(QuestListReq questListReq){
        JSONObject res = new JSONObject();
        List<QuestListRes> questListResList=new ArrayList<>();
        List<PracticQuest> practicQuestList=new ArrayList<>();
        List<TestQuest> testQuestList=new ArrayList<>();
        List<CourseQuest> courseQuestList=new ArrayList<>();
        Map<String,Object> mapSubjectCategory=new HashMap<>();
        Map<String,Object> mapSubjectChapter=new HashMap<>();
        Map<String,Object> mapCourseCategory=new HashMap<>();
        Map<String,Object> mapCourseSubject=new HashMap<>();
        List<String> chapterIds=new ArrayList<>();//包括章节id
        List<String> chapterIdsV2=new ArrayList<>();//包括课程科目id
        //现在默认就是传科目，相当于查询所有。
        if("科目".equals(questListReq.getType())){
            //学科，章节
            List<SubjectCategory> subjectCategoryList=new ArrayList<>();
            List<CourseCategory> courseCategoryList=new ArrayList<>();
            if("all".equals(questListReq.getDescribes())){
                subjectCategoryList=subjectCategoryDao.findAll();
                courseCategoryList=courseCategoryDao.findAll();
            }else {
                subjectCategoryList=subjectCategoryDao.findByNameLike(questListReq.getDescribes());
                courseCategoryList=courseCategoryDao.findByNameLike(questListReq.getDescribes());
            }
            List<String> subjectIds=new ArrayList<>();
            for(SubjectCategory subjectCategory:subjectCategoryList){
                subjectIds.add(subjectCategory.getId());
                mapSubjectCategory.put(subjectCategory.getId(),subjectCategory);
            }
            List<SubjectChapter> subjectChapterList=subjectChapterDao.findBySubjectIdIn(subjectIds);
            for(SubjectChapter subjectChapter:subjectChapterList){
                chapterIds.add(subjectChapter.getId());
                mapSubjectChapter.put(subjectChapter.getId(),subjectChapter);
            }

            //课程分类，课程科目
            List<String> courseCategoryIds=new ArrayList<>();
            for (CourseCategory courseCategory:courseCategoryList){
                courseCategoryIds.add(courseCategory.getId());
                mapCourseCategory.put(courseCategory.getId(),courseCategory);
            }
            List<CourseSubject> courseSubjectList=courseSubjectDao.findByCourseCategoryIdIn(courseCategoryIds);
            for (CourseSubject courseSubject:courseSubjectList){
                chapterIdsV2.add(courseSubject.getId());
                mapCourseSubject.put(courseSubject.getId(),courseSubject);
            }
        }else if("章节".equals(questListReq.getType())){
            //这里还没有用到，还没有考虑课程列表的情况
            List<SubjectChapter> subjectChapterList=subjectChapterDao.findByNameLike(questListReq.getDescribes());
            for(SubjectChapter subjectChapter:subjectChapterList){
                chapterIds.add(subjectChapter.getId());
                mapSubjectChapter.put(subjectChapter.getId(),subjectChapter);
            }
        }
        if("all".equals(questListReq.getExamType())){
            practicQuestList=practicQuestDao.findByChapterIdIn(chapterIds);
            testQuestList=testQuestDao.findByChapterIdIn(chapterIds);
            courseQuestList=courseQuestDao.findByChapterIdIn(chapterIdsV2);
        }else {
            practicQuestList=practicQuestDao.findByChapterIdInAndExamType(chapterIds,questListReq.getExamType());
            testQuestList=testQuestDao.findByChapterIdInAndExamType(chapterIds,questListReq.getExamType());
            courseQuestList=courseQuestDao.findByChapterIdInAndExamType(chapterIdsV2,questListReq.getExamType());
        }

        //获取试卷和课程信息
        List<TestInfo> testInfoList=testInfoDao.findAll();
        List<CourseInfo> courseInfoList=courseInfoDao.findAll();
        Map<String,Object> mapTestInfo=new HashMap<>();
        Map<String,Object> mapCourseInfo=new HashMap<>();
        for(TestInfo testInfo:testInfoList){
            mapTestInfo.put(testInfo.getId(),testInfo);
        }
        for(CourseInfo courseInfo:courseInfoList){
            mapCourseInfo.put(courseInfo.getId(),courseInfo);
        }

        for(CourseQuest courseQuest:courseQuestList){
            CourseSubject courseSubject=(CourseSubject)mapCourseSubject.get(courseQuest.getChapterId());
            CourseCategory courseCategory=(CourseCategory)mapCourseCategory.get(courseSubject.getCourseCategoryId());
            CourseInfo courseInfo=(CourseInfo)mapCourseInfo.get(courseQuest.getCourseId());
            if(courseInfo==null){
                courseInfo=new CourseInfo();
            }
            QuestListRes questListRes=new QuestListRes("createTime",2);
            questListRes.setId(courseQuest.getId());
            questListRes.setType(courseQuest.getType());

            questListRes.setCourseId(courseInfo.getId());
            questListRes.setCourseName(courseInfo.getName());
            questListRes.setSubjectId(courseCategory.getId());
            questListRes.setSubjectName(courseCategory.getName());
            questListRes.setChapterId(courseSubject.getId());
            questListRes.setChapterName(courseSubject.getName());

            questListRes.setQuestType(courseQuest.getQuestType());
            questListRes.setExamType(courseQuest.getExamType());
            questListRes.setTitle(courseQuest.getTitle());
            questListRes.setDescribes(courseQuest.getDescribes());
            questListRes.setTitlePicUrl(courseQuest.getTitlePicUrl());
            questListRes.setOptiona(courseQuest.getOptiona());
            questListRes.setOptionb(courseQuest.getOptionb());
            questListRes.setOptionc(courseQuest.getOptionc());
            questListRes.setOptiond(courseQuest.getOptiond());
            questListRes.setOptione(courseQuest.getOptione());
            questListRes.setOptionType(courseQuest.getOptionType());
            questListRes.setRightAnswer(courseQuest.getRightAnswer());
            questListRes.setAnswerParse(courseQuest.getAnswerParse());
            questListRes.setTestCentre(courseQuest.getTestCentre());
            questListRes.setCreateTime(courseQuest.getCreateTime());
            questListRes.setUpdateTime(courseQuest.getUpdateTime());
            questListResList.add(questListRes);
        }
        for(TestQuest testQuest:testQuestList){
            SubjectChapter subjectChapter=(SubjectChapter)mapSubjectChapter.get(testQuest.getChapterId());
            SubjectCategory subjectCategory=(SubjectCategory)mapSubjectCategory.get(subjectChapter.getSubjectId());
            TestInfo testInfo=(TestInfo)mapTestInfo.get(testQuest.getTestId());
            if(testInfo==null){
                testInfo=new TestInfo();
            }
            QuestListRes questListRes=new QuestListRes("createTime",2);
            questListRes.setId(testQuest.getId());
            questListRes.setType(testQuest.getType());

            questListRes.setTestId(testInfo.getId());
            questListRes.setTestName(testInfo.getName());
            questListRes.setSubjectId(subjectCategory.getId());
            questListRes.setSubjectName(subjectCategory.getName());
            questListRes.setChapterId(subjectChapter.getId());
            questListRes.setChapterName(subjectChapter.getName());
            questListRes.setQuestType(testQuest.getQuestType());
            questListRes.setExamType(testQuest.getExamType());
            questListRes.setTitle(testQuest.getTitle());
            questListRes.setDescribes(testQuest.getDescribes());
            questListRes.setTitlePicUrl(testQuest.getTitlePicUrl());
            questListRes.setOptiona(testQuest.getOptiona());
            questListRes.setOptionb(testQuest.getOptionb());
            questListRes.setOptionc(testQuest.getOptionc());
            questListRes.setOptiond(testQuest.getOptiond());
            questListRes.setOptione(testQuest.getOptione());
            questListRes.setOptionType(testQuest.getOptionType());
            questListRes.setRightAnswer(testQuest.getRightAnswer());
            questListRes.setAnswerParse(testQuest.getAnswerParse());
            questListRes.setTestCentre(testQuest.getTestCentre());
            questListRes.setCreateTime(testQuest.getCreateTime());
            questListRes.setUpdateTime(testQuest.getUpdateTime());
            questListResList.add(questListRes);
        }
        for(PracticQuest practicQuest:practicQuestList){
            SubjectChapter subjectChapter=(SubjectChapter)mapSubjectChapter.get(practicQuest.getChapterId());
            SubjectCategory subjectCategory=(SubjectCategory)mapSubjectCategory.get(subjectChapter.getSubjectId());
            QuestListRes questListRes=new QuestListRes("createTime",2);
            questListRes.setId(practicQuest.getId());
            questListRes.setType(practicQuest.getType());

            questListRes.setSubjectId(subjectCategory.getId());
            questListRes.setSubjectName(subjectCategory.getName());
            questListRes.setChapterId(subjectChapter.getId());
            questListRes.setChapterName(subjectChapter.getName());
            questListRes.setQuestType(practicQuest.getQuestType());
            questListRes.setExamType(practicQuest.getExamType());
            questListRes.setTitle(practicQuest.getTitle());
            questListRes.setDescribes(practicQuest.getDescribes());
            questListRes.setTitlePicUrl(practicQuest.getTitlePicUrl());
            questListRes.setOptiona(practicQuest.getOptiona());
            questListRes.setOptionb(practicQuest.getOptionb());
            questListRes.setOptionc(practicQuest.getOptionc());
            questListRes.setOptiond(practicQuest.getOptiond());
            questListRes.setOptione(practicQuest.getOptione());
            questListRes.setOptionType(practicQuest.getOptionType());
            questListRes.setRightAnswer(practicQuest.getRightAnswer());
            questListRes.setAnswerParse(practicQuest.getAnswerParse());
            questListRes.setTestCentre(practicQuest.getTestCentre());
            questListRes.setCreateTime(practicQuest.getCreateTime());
            questListRes.setUpdateTime(practicQuest.getUpdateTime());
            questListResList.add(questListRes);
        }

        Collections.sort(questListResList);
        //实现分页
        int offset=(questListReq.getPage()-1)*questListReq.getSize();
        int limit=questListReq.getPage()*questListReq.getSize();
        int total=questListResList.size();
        if(limit>total){
            limit=total;
        }
        List<QuestListRes>data=new ArrayList<>();
        if(total>0 && offset<limit) {
            data = questListResList.subList(offset, limit);
        }
        res.put("data",data);
        res.put("total",total);
        return res;
    }







    public JSONObject getQuestTypeListXiaLa(){
        JSONObject res = new JSONObject();
        List<QuestType> questTypes=questTypeDao.findAll();
        if(questTypes.size()==0){
            questTypes=new ArrayList<>();
        }
        res.put("data",questTypes);
        return res;
    }

    public JSONObject getSubjectCategoryListXiaLa(String examType){
        JSONObject res = new JSONObject();
        List<SubjectCategory> subjectCategoryListGroup=subjectCategoryDao.findSubjectCategoryGroupByExamType(examType);
        List<SubjectCategoryListXiaLaRes> subjectCategoryListXiaLaResList=new ArrayList<>();
        for(SubjectCategory subjectCategory:subjectCategoryListGroup){
            SubjectCategoryListXiaLaRes subjectCategoryListXiaLaRes=new SubjectCategoryListXiaLaRes();
            subjectCategoryListXiaLaRes.setSubjectId(subjectCategory.getId());
            subjectCategoryListXiaLaRes.setSubjectName(subjectCategory.getName());
            subjectCategoryListXiaLaResList.add(subjectCategoryListXiaLaRes);
        }

        res.put("data",subjectCategoryListXiaLaResList);
        return res;
    }

    public JSONObject getSubjectChapterListXiaLa(String subjectId){
        JSONObject res = new JSONObject();
        List<SubjectChapter> subjectChapterList=subjectChapterDao.findBySubjectId(subjectId);
        if(subjectChapterList.size()==0){
            subjectChapterList=new ArrayList<>();
        }
        res.put("data",subjectChapterList);
        return res;
    }

    public JSONObject getQuestXiaLa(String type){
        JSONObject res = new JSONObject();
        List<QuestXiaLaRes> questXiaLaResList=new ArrayList<>();
        if("模拟试卷题库".equals(type)){
            List<TestInfo> testInfoList=testInfoDao.findAll();
            for(TestInfo testInfo:testInfoList){
                QuestXiaLaRes questXiaLaRes=new QuestXiaLaRes();
                questXiaLaRes.setId(testInfo.getId());
                questXiaLaRes.setName(testInfo.getName());
                questXiaLaResList.add(questXiaLaRes);
            }
        }else if("课程".equals(type)){
            List<CourseInfo> courseInfoList=courseInfoDao.findAll();
            if(courseInfoList.size()==0){
                courseInfoList=new ArrayList<>();
            }
            for(CourseInfo courseInfo:courseInfoList){
                QuestXiaLaRes questXiaLaRes=new QuestXiaLaRes();
                questXiaLaRes.setId(courseInfo.getId());
                questXiaLaRes.setName(courseInfo.getName());
                questXiaLaResList.add(questXiaLaRes);
            }
        }else {
            throw new WafException("", "请点击题目归属", HttpStatus.NOT_ACCEPTABLE);
        }
        res.put("data",questXiaLaResList);
        return res;
    }



    public String readExcelFile(MultipartFile file) {
        String result = "";
        //创建处理EXCEL的类
        ReadExcel readExcel = new ReadExcel();
        //解析excel，获取上传的事件单
        List<ExcelVo> excelVoList = readExcel.getExcelInfo(file);
        List<TestQuest> testQuestList=new ArrayList<>();
        List<PracticQuest> practicQuestList=new ArrayList<>();
        List<CourseQuest> courseQuestList=new ArrayList<>();
        //至此已经将excel中的数据转换到list里面了,接下来就可以操作list,可以进行保存到数据库,或者其他操作,
        for(ExcelVo excelVo:excelVoList){
            if(ObjectUtils.isEmpty(excelVo.getType())){
                throw new WafException("", "题目归属不能为空", HttpStatus.NOT_ACCEPTABLE);
            }
            if(ObjectUtils.isEmpty(excelVo.getExamType())){
                throw new WafException("", "考试类型不能为空", HttpStatus.NOT_ACCEPTABLE);
            }
            if(ObjectUtils.isEmpty(excelVo.getSubjectName())){
                throw new WafException("", "学科不能为空", HttpStatus.NOT_ACCEPTABLE);
            }
            if(ObjectUtils.isEmpty(excelVo.getChapterName())){
                throw new WafException("", "章节不能为空", HttpStatus.NOT_ACCEPTABLE);
            }
            if(ObjectUtils.isEmpty(excelVo.getQuestType())){
                throw new WafException("", "题型类别不能为空", HttpStatus.NOT_ACCEPTABLE);
            }
            if(ObjectUtils.isEmpty(excelVo.getTitle())){
                throw new WafException("", "题型标题不能为空", HttpStatus.NOT_ACCEPTABLE);
            }
            if(ObjectUtils.isEmpty(excelVo.getOptiona())){
                throw new WafException("", "选项a不能为空", HttpStatus.NOT_ACCEPTABLE);
            }
            if(ObjectUtils.isEmpty(excelVo.getOptionb())){
                throw new WafException("", "选项b不能为空", HttpStatus.NOT_ACCEPTABLE);
            }
            if(ObjectUtils.isEmpty(excelVo.getOptionc())){
                throw new WafException("", "选项c不能为空", HttpStatus.NOT_ACCEPTABLE);
            }
            if(ObjectUtils.isEmpty(excelVo.getOptiond())){
                throw new WafException("", "选项d不能为空", HttpStatus.NOT_ACCEPTABLE);
            }
            if(ObjectUtils.isEmpty(excelVo.getOptione())){
                throw new WafException("", "选项e不能为空", HttpStatus.NOT_ACCEPTABLE);
            }
            if(ObjectUtils.isEmpty(excelVo.getRightAnswer())){
                throw new WafException("", "正确答案不能为空", HttpStatus.NOT_ACCEPTABLE);
            }
            if("模拟试卷题库".equals(excelVo.getType())){
                if(ObjectUtils.isEmpty(excelVo.getName())){
                    throw new WafException("", "试卷名称不能为空", HttpStatus.NOT_ACCEPTABLE);
                }
            }
            if("课程题库".equals(excelVo.getType())){
                if(ObjectUtils.isEmpty(excelVo.getName())){
                    throw new WafException("", "课程名称不能为空", HttpStatus.NOT_ACCEPTABLE);
                }
            }
            QuestType questType=questTypeDao.findByName(excelVo.getQuestType());
            if(questType==null){
                throw new WafException("", "题型类别<"+excelVo.getQuestType()+">不存在，请添加", HttpStatus.NOT_ACCEPTABLE);
            }

            String[] optionTypes=excelVo.getRightAnswer().split(",");
            if("模拟试卷题库".equals(excelVo.getType())){
                SubjectCategory subjectCategory=subjectCategoryDao.findByNameAndExamType(excelVo.getSubjectName(),excelVo.getExamType());
                if(subjectCategory==null){
                    throw new WafException("", "模拟试卷题库不存在<"+excelVo.getSubjectName()+">科目，请添加", HttpStatus.NOT_ACCEPTABLE);
                }
                SubjectChapter subjectChapter=subjectChapterDao.findBySubjectIdAndName(subjectCategory.getId(),excelVo.getChapterName());
                if(subjectChapter==null){
                    throw new WafException("", "模拟试卷题库该科目<"+excelVo.getSubjectName()+"-"+excelVo.getChapterName()+">下的章节不存在,请添加", HttpStatus.NOT_ACCEPTABLE);
                }
                TestInfo testInfo=testInfoDao.findByNameAndExamType(excelVo.getName(),excelVo.getExamType());
                if(testInfo==null){
                    throw new WafException("", "模拟试卷题库不存在<"+excelVo.getName()+">试卷名称，请添加", HttpStatus.NOT_ACCEPTABLE);
                }

                TestQuest testQuestOld=testQuestDao.findByTitleAndExamType(excelVo.getTitle(),excelVo.getExamType());
                if(testQuestOld==null){
                    TestQuest testQuest=new TestQuest();
                    testQuest.setId(UUID.randomUUID().toString().replace("-",""));
                    testQuest.setType(excelVo.getType());
                    testQuest.setTestId(testInfo.getId());
                    testQuest.setExamType(excelVo.getExamType());
                    testQuest.setQuestType(excelVo.getQuestType());
                    testQuest.setTitle(excelVo.getTitle());
                    testQuest.setOptiona(excelVo.getOptiona());
                    testQuest.setOptionb(excelVo.getOptionb());
                    testQuest.setOptionc(excelVo.getOptionc());
                    testQuest.setOptiond(excelVo.getOptiond());
                    testQuest.setOptione(excelVo.getOptione());
                    if(optionTypes.length!=1){
                        testQuest.setOptionType(2);
                    }else {
                        testQuest.setOptionType(1);
                    }
                    testQuest.setRightAnswer(excelVo.getRightAnswer());
                    testQuest.setAnswerParse(excelVo.getAnswerParse());
                    testQuest.setTestCentre(excelVo.getTestCentre());
                    testQuest.setChapterId(subjectChapter.getId());
                    testQuest.setCreateTime(DateUtil.getNowTimestamp());
                    testQuest.setUpdateTime(DateUtil.getNowTimestamp());
                    testQuestList.add(testQuest);
                }else {
                    testQuestOld.setExamType(excelVo.getExamType());
                    testQuestOld.setType(excelVo.getType());
                    testQuestOld.setTestId(testInfo.getId());
                    testQuestOld.setQuestType(excelVo.getQuestType());
                    testQuestOld.setTitle(excelVo.getTitle());
                    testQuestOld.setOptiona(excelVo.getOptiona());
                    testQuestOld.setOptionb(excelVo.getOptionb());
                    testQuestOld.setOptionc(excelVo.getOptionc());
                    testQuestOld.setOptiond(excelVo.getOptiond());
                    testQuestOld.setOptione(excelVo.getOptione());
                    if(optionTypes.length!=1){
                        testQuestOld.setOptionType(2);
                    }else {
                        testQuestOld.setOptionType(1);
                    }
                    testQuestOld.setRightAnswer(excelVo.getRightAnswer());
                    testQuestOld.setAnswerParse(excelVo.getAnswerParse());
                    testQuestOld.setTestCentre(excelVo.getTestCentre());
                    testQuestOld.setChapterId(subjectChapter.getId());
                    testQuestOld.setUpdateTime(DateUtil.getNowTimestamp());
                    testQuestList.add(testQuestOld);
                }
            }else if("练习题库".equals(excelVo.getType())){
                SubjectCategory subjectCategory=subjectCategoryDao.findByNameAndExamType(excelVo.getSubjectName(),excelVo.getExamType());
                if(subjectCategory==null){
                    throw new WafException("", "练习题库不存在<"+excelVo.getSubjectName()+">科目，请添加", HttpStatus.NOT_ACCEPTABLE);
                }
                SubjectChapter subjectChapter=subjectChapterDao.findBySubjectIdAndName(subjectCategory.getId(),excelVo.getChapterName());
                if(subjectChapter==null){
                    throw new WafException("", "练习题库该科目<"+excelVo.getSubjectName()+"-"+excelVo.getChapterName()+">下的章节不存在,请添加", HttpStatus.NOT_ACCEPTABLE);
                }

                PracticQuest practicQuestOld=practicQuestDao.findByTitleAndExamType(excelVo.getTitle(),excelVo.getExamType());
                if(practicQuestOld==null){
                    PracticQuest practicQuest=new PracticQuest();
                    practicQuest.setId(UUID.randomUUID().toString().replace("-",""));
                    String type="练习题库表";
                    practicQuest.setType(type);
                    practicQuest.setExamType(excelVo.getExamType());
                    practicQuest.setQuestType(excelVo.getQuestType());
                    practicQuest.setTitle(excelVo.getTitle());
                    practicQuest.setOptiona(excelVo.getOptiona());
                    practicQuest.setOptionb(excelVo.getOptionb());
                    practicQuest.setOptionc(excelVo.getOptionc());
                    practicQuest.setOptiond(excelVo.getOptiond());
                    practicQuest.setOptione(excelVo.getOptione());
                    if(optionTypes.length!=1){
                        practicQuest.setOptionType(2);
                    }else {
                        practicQuest.setOptionType(1);
                    }
                    practicQuest.setRightAnswer(excelVo.getRightAnswer());
                    practicQuest.setAnswerParse(excelVo.getAnswerParse());
                    practicQuest.setTestCentre(excelVo.getTestCentre());
                    practicQuest.setChapterId(subjectChapter.getId());
                    practicQuest.setCreateTime(DateUtil.getNowTimestamp());
                    practicQuest.setUpdateTime(DateUtil.getNowTimestamp());
                    practicQuestList.add(practicQuest);
                }else {
                    practicQuestOld.setExamType(excelVo.getExamType());
                    String type="练习题库表";
                    practicQuestOld.setType(type);
                    practicQuestOld.setQuestType(excelVo.getQuestType());
                    practicQuestOld.setTitle(excelVo.getTitle());
                    practicQuestOld.setOptiona(excelVo.getOptiona());
                    practicQuestOld.setOptionb(excelVo.getOptionb());
                    practicQuestOld.setOptionc(excelVo.getOptionc());
                    practicQuestOld.setOptiond(excelVo.getOptiond());
                    practicQuestOld.setOptione(excelVo.getOptione());
                    if(optionTypes.length!=1){
                        practicQuestOld.setOptionType(2);
                    }else {
                        practicQuestOld.setOptionType(1);
                    }
                    practicQuestOld.setRightAnswer(excelVo.getRightAnswer());
                    practicQuestOld.setAnswerParse(excelVo.getAnswerParse());
                    practicQuestOld.setTestCentre(excelVo.getTestCentre());
                    practicQuestOld.setChapterId(subjectChapter.getId());
                    practicQuestOld.setUpdateTime(DateUtil.getNowTimestamp());
                    practicQuestList.add(practicQuestOld);
                }
            }else if("课程题库".equals(excelVo.getType())){
                CourseCategory courseCategory=courseCategoryDao.findByNameAndExamType(excelVo.getSubjectName(),excelVo.getExamType());
                if(courseCategory==null){
                    throw new WafException("", "课程题库不存在<"+excelVo.getSubjectName()+">科目，请添加", HttpStatus.NOT_ACCEPTABLE);
                }
                CourseSubject courseSubject=courseSubjectDao.findByNameAndCourseCategoryId(excelVo.getChapterName(),courseCategory.getId());
                if(courseSubject==null){
                    throw new WafException("", "课程题库该科目<"+excelVo.getSubjectName()+"-"+excelVo.getChapterName()+">下的课程科目不存在,请添加", HttpStatus.NOT_ACCEPTABLE);
                }
                CourseInfo courseInfo=courseInfoDao.findByNameAndExamType(excelVo.getName(),excelVo.getExamType());
                if(courseInfo==null){
                    throw new WafException("", "课程题库不存在<"+excelVo.getName()+">课程名称，请添加", HttpStatus.NOT_ACCEPTABLE);
                }

                CourseQuest courseQuestOld=courseQuestDao.findByTitleAndExamType(excelVo.getTitle(),excelVo.getExamType());
                if(courseQuestOld==null){
                    CourseQuest courseQuest=new CourseQuest();
                    courseQuest.setId(UUID.randomUUID().toString().replace("-",""));
                    String type="课程";
                    courseQuest.setType(type);
                    courseQuest.setExamType(excelVo.getExamType());
                    courseQuest.setCourseId(courseInfo.getId());
                    courseQuest.setQuestType(excelVo.getQuestType());
                    courseQuest.setTitle(excelVo.getTitle());
                    courseQuest.setOptiona(excelVo.getOptiona());
                    courseQuest.setOptionb(excelVo.getOptionb());
                    courseQuest.setOptionc(excelVo.getOptionc());
                    courseQuest.setOptiond(excelVo.getOptiond());
                    courseQuest.setOptione(excelVo.getOptione());
                    if(optionTypes.length!=1){
                        courseQuest.setOptionType(2);
                    }else {
                        courseQuest.setOptionType(1);
                    }
                    courseQuest.setRightAnswer(excelVo.getRightAnswer());
                    courseQuest.setAnswerParse(excelVo.getAnswerParse());
                    courseQuest.setTestCentre(excelVo.getTestCentre());
                    courseQuest.setChapterId(courseSubject.getId());
                    courseQuest.setCreateTime(DateUtil.getNowTimestamp());
                    courseQuest.setUpdateTime(DateUtil.getNowTimestamp());
                    courseQuestList.add(courseQuest);
                }else {
                    courseQuestOld.setExamType(excelVo.getExamType());
                    String type="课程";
                    courseQuestOld.setType(type);
                    courseQuestOld.setCourseId(courseInfo.getId());
                    courseQuestOld.setQuestType(excelVo.getQuestType());
                    courseQuestOld.setTitle(excelVo.getTitle());
                    courseQuestOld.setOptiona(excelVo.getOptiona());
                    courseQuestOld.setOptionb(excelVo.getOptionb());
                    courseQuestOld.setOptionc(excelVo.getOptionc());
                    courseQuestOld.setOptiond(excelVo.getOptiond());
                    courseQuestOld.setOptione(excelVo.getOptione());
                    if(optionTypes.length!=1){
                        courseQuestOld.setOptionType(2);
                    }else {
                        courseQuestOld.setOptionType(1);
                    }
                    courseQuestOld.setRightAnswer(excelVo.getRightAnswer());
                    courseQuestOld.setAnswerParse(excelVo.getAnswerParse());
                    courseQuestOld.setTestCentre(excelVo.getTestCentre());
                    courseQuestOld.setChapterId(courseSubject.getId());
                    courseQuestOld.setUpdateTime(DateUtil.getNowTimestamp());
                    courseQuestList.add(courseQuestOld);
                }
            }
        }
        testQuestDao.save(testQuestList);
        practicQuestDao.save(practicQuestList);
        courseQuestDao.save(courseQuestList);
        return result;
    }









}
