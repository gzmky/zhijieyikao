package com.mky.mkcompany.service;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.mky.mkcompany.controller.domain.DoPracticeAllReq;
import com.mky.mkcompany.controller.domain.DoPracticeReq;
import com.mky.mkcompany.dao.*;
import com.mky.mkcompany.exception.WafException;
import com.mky.mkcompany.model.*;
import com.mky.mkcompany.tool.Arith;
import com.mky.mkcompany.tool.DateUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.repository.Query;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import java.sql.Timestamp;
import java.util.*;

/**
 * Created by Lenovo on 2017/9/1.
 */
@Service
public class DoExerciseService {
    private final Logger LOGGER = LoggerFactory.getLogger(getClass());
    @Autowired
    private PracticQuestDao practicQuestDao;
    @Autowired
    private UserDoPracticQuestDao userDoPracticQuestDao;
    @Autowired
    private UserCollectPracticQuestDao userCollectPracticQuestDao;
    @Autowired
    private SubjectCategoryDao subjectCategoryDao;
    @Autowired
    private SubjectChapterDao subjectChapterDao;
    @Autowired
    private UserQuestSeqDao userQuestSeqDao;


    public JSONObject getRandomPractice(String examType,String userId,int page,int size){
        JSONObject res = new JSONObject();
        Sort sort=new Sort(Sort.Direction.ASC,"createTime");
        Pageable pageable = new PageRequest(page-1, size, sort);
        Page<PracticQuest> practicQuests=practicQuestDao.findPracticQuestPage(examType,pageable);
        List<UserDoPracticQuest>userDoPracticQuests=userDoPracticQuestDao.findObjsByExamType(examType,userId);
       List<UserCollectPracticQuest>userCollectPracticQuests=userCollectPracticQuestDao.findCollQuestsByUserId(examType,userId);
        UserQuestSeq userQuestSeq=userQuestSeqDao.findByExamTypeAndUserIdAndSubjectCategoryIdAndSubjectChapterId(examType,
                userId,"all","all");
        List<PracticQuest>practicQuestList=practicQuests.getContent();
        Map<String,UserDoPracticQuest>userDoPracticQuestMap=new HashMap<>();
        Map<String,UserCollectPracticQuest>userCollectPracticQuestMap=new HashMap<>();
        for(UserDoPracticQuest userDoPracticQuest:userDoPracticQuests){
            userDoPracticQuestMap.put(userDoPracticQuest.getQuestId(),userDoPracticQuest);
        }
        for(UserCollectPracticQuest userCollectPracticQuest:userCollectPracticQuests){
            userCollectPracticQuestMap.put(userCollectPracticQuest.getQuestId(),userCollectPracticQuest);
        }
        JSONArray array=new JSONArray();
        for(PracticQuest practicQuest:practicQuestList){
            UserDoPracticQuest userDoPracticQuest=userDoPracticQuestMap.get(practicQuest.getId());
            JSONObject json =(JSONObject)JSONObject.toJSON(practicQuest);
            UserCollectPracticQuest userCollectPracticQuest=userCollectPracticQuestMap.get(practicQuest.getId());
            if(userDoPracticQuest!=null){
                json.put("result",userDoPracticQuest.getResult());
                json.put("answerResult",userDoPracticQuest.getAnswerResult());
            }else{
                json.put("result",null);
                json.put("answerResult",0);
            }
            int isCollect=0;
            if(userCollectPracticQuest!=null){
                isCollect=1;
            }
            json.put("isCollect",isCollect);
            array.add(json);
        }
        Integer seq=1;
        if(userQuestSeq!=null){
            seq=userQuestSeq.getSeq();
        }
        long totalQuestNum=practicQuests.getTotalElements();
        long userQuestTrueNum=userDoPracticQuestDao.findCountQuestByAnswerResult(examType,userId,1);
        long userQuestFalseNum=userDoPracticQuestDao.findCountQuestByAnswerResult(examType,userId,2);
        long userDoQuestNum=userQuestTrueNum+userQuestFalseNum;
        long userNoDoQuestNum=totalQuestNum-userDoQuestNum;
        double questCorrect=0;
        if(userDoQuestNum!=0){
            questCorrect= Arith.div((double)userQuestTrueNum,(double)userDoQuestNum,3);
        }
        long userQuestCollect=userCollectPracticQuestDao.findCountCollQuestByUserId(examType,userId);
        res.put("userQuestTrueNum",userQuestTrueNum);
        res.put("userQuestFalseNum",userQuestFalseNum);
        res.put("userDoQuestNum",userDoQuestNum);
        res.put("userNoDoQuestNum",userNoDoQuestNum);
        res.put("questCorrect",questCorrect);
        res.put("userQuestCollect",userQuestCollect);
        res.put("data",array);
        res.put("seq",seq);
        res.put("total",practicQuests.getTotalElements());
        return res;
    }

    public JSONObject getRandomPracticeByType(String examType,String type,String userId,int page,int size){
        JSONObject res = new JSONObject();
       List<PracticQuest> practicQuests=new ArrayList<>();
        long total=0;
        Sort sort=new Sort(Sort.Direction.ASC,"createTime");
        Pageable pageable = new PageRequest(page-1, size, sort);
        if("do".equals(type)){
            Page<PracticQuest> practicQuestsDoTrue=userDoPracticQuestDao.findPracticQuestByAnswerResult(
                    examType,userId,1,pageable);
            Page<PracticQuest> practicQuestsDoFalse=userDoPracticQuestDao.findPracticQuestByAnswerResult(
                    examType,userId,2,pageable);
            total=practicQuestsDoTrue.getTotalElements()+practicQuestsDoFalse.getTotalElements();
            practicQuests.addAll(practicQuestsDoTrue.getContent());
            practicQuests.addAll(practicQuestsDoFalse.getContent());
        }else if("noDo".equals(type)){
            List<String> questIds=userDoPracticQuestDao.findDoPracticQuestByUserId(examType,userId);
           if(questIds.size()==0){
               questIds.add("0");
           }
            List<PracticQuest>practicQuestsNoDo=practicQuestDao.findByIdNotIn(questIds);
            total=practicQuestsNoDo.size();
            practicQuests.addAll(practicQuestsNoDo);
        }else if("doTrue".equals(type)){
            Page<PracticQuest> practicQuestsDoTrue=userDoPracticQuestDao.findPracticQuestByAnswerResult(
                    examType,userId,1,pageable);
            total=practicQuestsDoTrue.getTotalElements();
            practicQuests.addAll(practicQuestsDoTrue.getContent());
        }else if("doFalse".equals(type)){
            Page<PracticQuest> practicQuestsDoFalse=userDoPracticQuestDao.findPracticQuestByAnswerResult(
                    examType,userId,2,pageable);
            total=practicQuestsDoFalse.getTotalElements();
            practicQuests.addAll(practicQuestsDoFalse.getContent());
        }else if("collect".equals(type)){
            Page<PracticQuest> practicQuestsColl=userCollectPracticQuestDao.findCollQuestByUserId(
                    examType,userId,pageable);
            total=practicQuestsColl.getTotalElements();
            practicQuests.addAll(practicQuestsColl.getContent());
        }
        List<UserDoPracticQuest>userDoPracticQuests=userDoPracticQuestDao.findObjsByExamType(examType,userId);
        List<UserCollectPracticQuest>userCollectPracticQuests=userCollectPracticQuestDao.findCollQuestsByUserId(examType,userId);
        UserQuestSeq userQuestSeq=userQuestSeqDao.findByExamTypeAndUserIdAndSubjectCategoryIdAndSubjectChapterId(examType,
                userId,"all","all");
        Map<String,UserDoPracticQuest>userDoPracticQuestMap=new HashMap<>();
        Map<String,UserCollectPracticQuest>userCollectPracticQuestMap=new HashMap<>();
        for(UserDoPracticQuest userDoPracticQuest:userDoPracticQuests){
            userDoPracticQuestMap.put(userDoPracticQuest.getQuestId(),userDoPracticQuest);
        }
        for(UserCollectPracticQuest userCollectPracticQuest:userCollectPracticQuests){
            userCollectPracticQuestMap.put(userCollectPracticQuest.getQuestId(),userCollectPracticQuest);
        }
        JSONArray array=new JSONArray();
        for(PracticQuest practicQuest:practicQuests){
            UserDoPracticQuest userDoPracticQuest=userDoPracticQuestMap.get(practicQuest.getId());
            JSONObject json =(JSONObject)JSONObject.toJSON(practicQuest);
            UserCollectPracticQuest userCollectPracticQuest=userCollectPracticQuestMap.get(practicQuest.getId());
            if(userDoPracticQuest!=null){
                json.put("result",userDoPracticQuest.getResult());
                json.put("answerResult",userDoPracticQuest.getAnswerResult());
            }else{
                json.put("result",null);
                json.put("answerResult",0);
            }
            int isCollect=0;
            if(userCollectPracticQuest!=null){
                isCollect=1;
            }
            json.put("isCollect",isCollect);
            array.add(json);
        }
        Integer seq=1;
        if(userQuestSeq!=null){
            seq=userQuestSeq.getSeq();
        }
        Page<PracticQuest> practicQuestPage=practicQuestDao.findPracticQuestPage(examType,pageable);
        long totalQuestNum=practicQuestPage.getTotalElements();
        long userQuestTrueNum=userDoPracticQuestDao.findCountQuestByAnswerResult(examType,userId,1);
        long userQuestFalseNum=userDoPracticQuestDao.findCountQuestByAnswerResult(examType,userId,2);
        long userDoQuestNum=userQuestTrueNum+userQuestFalseNum;
        long userNoDoQuestNum=totalQuestNum-userDoQuestNum;
        double questCorrect=0;
        if(userDoQuestNum!=0){
            questCorrect= Arith.div((double)userQuestTrueNum,(double)userDoQuestNum,3);
        }
        long userQuestCollect=userCollectPracticQuestDao.findCountCollQuestByUserId(examType,userId);
        res.put("userQuestTrueNum",userQuestTrueNum);
        res.put("userQuestFalseNum",userQuestFalseNum);
        res.put("userDoQuestNum",userDoQuestNum);
        res.put("userNoDoQuestNum",userNoDoQuestNum);
        res.put("questCorrect",questCorrect);
        res.put("userQuestCollect",userQuestCollect);
        res.put("seq",seq);
        res.put("data",array);
        res.put("total",total);
        return res;
    }

    public void saveDoPractice(DoPracticeAllReq data, String userId){
        Timestamp createTime= DateUtil.getNowTimestamp();
        Timestamp updateTime= DateUtil.getNowTimestamp();
        List<UserDoPracticQuest>userDoPracticQuests=new ArrayList<>();
        List<DoPracticeReq>doPracticeReqList=data.getData();
        for(DoPracticeReq doPracticeReq:doPracticeReqList){
            String id= UUID.randomUUID().toString().replace("-","");
            UserDoPracticQuest userDoPracticQuest=userDoPracticQuestDao.findByQuestIdAndUserId(doPracticeReq.getQuestId(),userId);
            if(userDoPracticQuest==null){
                userDoPracticQuest=new UserDoPracticQuest();
                userDoPracticQuest.setId(id);
                userDoPracticQuest.setUserId(userId);
                userDoPracticQuest.setQuestId(doPracticeReq.getQuestId());
                userDoPracticQuest.setCreateTime(createTime);
            }
            userDoPracticQuest.setResult(doPracticeReq.getResult());
            userDoPracticQuest.setAnswerResult(doPracticeReq.getAnswerResult());
            userDoPracticQuest.setUpdateTime(updateTime);
            userDoPracticQuests.add(userDoPracticQuest);
        }
        UserQuestSeq userQuestSeq=userQuestSeqDao.findByExamTypeAndUserIdAndSubjectCategoryIdAndSubjectChapterId(data.getExamType(),
                userId,data.getSubjectCategoryId(),data.getSubjectCategoryId());
        if(userQuestSeq==null){
            userQuestSeq=new UserQuestSeq();
            userQuestSeq.setId(UUID.randomUUID().toString().replace("-",""));
            userQuestSeq.setUserId(userId);
            userQuestSeq.setSeq(data.getSeq());
            userQuestSeq.setExamType(data.getExamType());
            userQuestSeq.setSubjectCategoryId(data.getSubjectCategoryId());
            userQuestSeq.setSubjectChapterId(data.getSubjectChapterId());
            userQuestSeq.setCreateTime(createTime);
            userQuestSeq.setUpdateTime(updateTime);
        }else{
            userQuestSeq.setSeq(data.getSeq());
            userQuestSeq.setUpdateTime(updateTime);
        }
        userQuestSeqDao.save(userQuestSeq);
        userDoPracticQuestDao.save(userDoPracticQuests);
    }

    public void collectPractice(String questId,String userId){
        Timestamp createTime= DateUtil.getNowTimestamp();
        Timestamp updateTime= DateUtil.getNowTimestamp();
        UserCollectPracticQuest userCollectPracticQuest=userCollectPracticQuestDao.findByUserIdAndQuestId(userId,questId);
        if(userCollectPracticQuest==null) {
            userCollectPracticQuest=new UserCollectPracticQuest();
            String id = UUID.randomUUID().toString().replace("-", "");
            userCollectPracticQuest.setId(id);
            userCollectPracticQuest.setUserId(userId);
            userCollectPracticQuest.setQuestId(questId);
            userCollectPracticQuest.setCreateTime(createTime);
            userCollectPracticQuest.setUpdateTime(updateTime);
            userCollectPracticQuestDao.save(userCollectPracticQuest);
        }else{
            throw new WafException("","已收藏", HttpStatus.NOT_ACCEPTABLE);
        }
    }

    public void collectCancelPractice(String questId,String userId){
        UserCollectPracticQuest userCollectPracticQuest=userCollectPracticQuestDao.findByUserIdAndQuestId(userId,questId);
        if(userCollectPracticQuest!=null) {
            userCollectPracticQuestDao.delete(userCollectPracticQuest);
        }
    }

    public JSONObject getChapterPracticeHomePage(String examType,String userId) {
        JSONObject res = new JSONObject();
        List<SubjectCategory>subjectCategories=subjectCategoryDao.findSubjectCategory(examType);
        JSONArray array=new JSONArray();
        for(SubjectCategory subjectCategory:subjectCategories){
            long totalQuestNum=practicQuestDao.findCountBySubjectId(examType
                    ,subjectCategory.getId());
            long userQuestTrueNum=userDoPracticQuestDao.findCountQuestByAnswerResultSubId(examType,userId,subjectCategory.getId(),1);
            long userQuestFalseNum=userDoPracticQuestDao.findCountQuestByAnswerResultSubId(examType,userId,subjectCategory.getId(),2);
            long userDoQuestNum=userQuestTrueNum+userQuestFalseNum;
            double questCorrect=0;
            if(userDoQuestNum!=0){
                questCorrect= Arith.div((double)userQuestTrueNum,(double)userDoQuestNum,3);
            }
            JSONObject json=new JSONObject();
            json.put("id",subjectCategory.getId());
            json.put("name",subjectCategory.getName());
            json.put("questCorrect",questCorrect);
            json.put("num",totalQuestNum);
            array.add(json);
        }
        res.put("data",array);
        return res;
    }

    public JSONObject getChapterPracticeChapInfo(String examType,String userId,String subjectCategoryId) {
        JSONObject res = new JSONObject();
        List<SubjectChapter>subjectChapters=subjectChapterDao.findSubjectChapter(subjectCategoryId);
        JSONArray array=new JSONArray();
        for(SubjectChapter subjectChapter:subjectChapters){
            long totalQuestNum=practicQuestDao.findCountByChapterId(examType
                    ,subjectChapter.getId());
            long userQuestTrueNum=userDoPracticQuestDao.findCountQuestByAnswerResultChapId(examType,userId,subjectChapter.getId(),1);
            long userQuestFalseNum=userDoPracticQuestDao.findCountQuestByAnswerResultChapId(examType,userId,subjectChapter.getId(),2);
            long userDoQuestNum=userQuestTrueNum+userQuestFalseNum;
            double questCorrect=0;
            if(userDoQuestNum!=0){
                questCorrect= Arith.div((double)userQuestTrueNum,(double)userDoQuestNum,3);
            }
            JSONObject json=new JSONObject();
            json.put("id",subjectChapter.getId());
            json.put("name",subjectChapter.getName());
            json.put("questCorrect",questCorrect);
            json.put("num",totalQuestNum);
            array.add(json);
        }
        res.put("data",array);
        return res;
    }

    public JSONObject getChapterPractice(String examType,String userId,String subjectCategoryId,String subjectChapterId,int page,int size){
        JSONObject res = new JSONObject();
        Page<PracticQuest> practicQuestPage=null;
        long totalQuestNum=0;
        long userQuestTrueNum=0;
        long userQuestFalseNum=0;
        long userDoQuestNum=0;
        long userNoDoQuestNum=0;
        double questCorrect=0;
        long userQuestCollect=0;
        Sort sort=new Sort(Sort.Direction.ASC,"createTime");
        Pageable pageable = new PageRequest(page-1, size, sort);
        if("all".equals(subjectChapterId)){
            practicQuestPage=practicQuestDao.findPracticQuestPageBySubjectId(examType
                    ,subjectCategoryId,pageable);
            totalQuestNum=practicQuestPage.getTotalElements();
             userQuestTrueNum=userDoPracticQuestDao.findCountQuestByAnswerResultSubId(examType,userId,subjectCategoryId,1);
            userQuestFalseNum=userDoPracticQuestDao.findCountQuestByAnswerResultSubId(examType,userId,subjectCategoryId,2);
            userQuestCollect=userCollectPracticQuestDao.findCountCollQuestByUserIdSubId(examType,userId,subjectCategoryId);

        }else{
            practicQuestPage=practicQuestDao.findPracticQuestPageByChapterId(examType
                    ,subjectChapterId,pageable);
            totalQuestNum=practicQuestPage.getTotalElements();
            userQuestTrueNum=userDoPracticQuestDao.findCountQuestByAnswerResultChapId(examType,userId,subjectChapterId,1);
            userQuestFalseNum=userDoPracticQuestDao.findCountQuestByAnswerResultChapId(examType,userId,subjectChapterId,2);
            userQuestCollect=userCollectPracticQuestDao.findCountCollQuestByUserIdChapId(examType,userId,subjectChapterId);

        }
        userDoQuestNum=userQuestTrueNum+userQuestFalseNum;
        userNoDoQuestNum=totalQuestNum-userDoQuestNum;
        if(userDoQuestNum!=0){
            questCorrect= Arith.div((double)userQuestTrueNum,(double)userDoQuestNum,3);
        }
        UserQuestSeq userQuestSeq=userQuestSeqDao.findByExamTypeAndUserIdAndSubjectCategoryIdAndSubjectChapterId(examType,
                userId,subjectCategoryId,subjectChapterId);
        Integer seq=1;
        if(userQuestSeq!=null){
            seq=userQuestSeq.getSeq();
        }
        List<UserDoPracticQuest>userDoPracticQuests=userDoPracticQuestDao.findObjsByExamType(examType,userId);
        List<UserCollectPracticQuest>userCollectPracticQuests=userCollectPracticQuestDao.findCollQuestsByUserId(examType,userId);
        Map<String,UserDoPracticQuest>userDoPracticQuestMap=new HashMap<>();
        Map<String,UserCollectPracticQuest>userCollectPracticQuestMap=new HashMap<>();
        for(UserDoPracticQuest userDoPracticQuest:userDoPracticQuests){
            userDoPracticQuestMap.put(userDoPracticQuest.getQuestId(),userDoPracticQuest);
        }
        for(UserCollectPracticQuest userCollectPracticQuest:userCollectPracticQuests){
            userCollectPracticQuestMap.put(userCollectPracticQuest.getQuestId(),userCollectPracticQuest);
        }
        JSONArray array=new JSONArray();
        List<PracticQuest>practicQuests=practicQuestPage.getContent();
        for(PracticQuest practicQuest:practicQuests){
            UserDoPracticQuest userDoPracticQuest=userDoPracticQuestMap.get(practicQuest.getId());
            JSONObject json =(JSONObject)JSONObject.toJSON(practicQuest);
            UserCollectPracticQuest userCollectPracticQuest=userCollectPracticQuestMap.get(practicQuest.getId());
            if(userDoPracticQuest!=null){
                json.put("result",userDoPracticQuest.getResult());
                json.put("answerResult",userDoPracticQuest.getAnswerResult());
            }else{
                json.put("result",null);
                json.put("answerResult",0);
            }
            int isCollect=0;
            if(userCollectPracticQuest!=null){
                isCollect=1;
            }
            json.put("isCollect",isCollect);
            array.add(json);
        }

        res.put("seq",seq);
        res.put("userQuestTrueNum",userQuestTrueNum);
        res.put("userQuestFalseNum",userQuestFalseNum);
        res.put("userDoQuestNum",userDoQuestNum);
        res.put("userNoDoQuestNum",userNoDoQuestNum);
        res.put("questCorrect",questCorrect);
        res.put("userQuestCollect",userQuestCollect);
        res.put("data",array);
        res.put("seq",seq);
        res.put("total",practicQuestPage.getTotalElements());
        return res;
    }

    public JSONObject getChapterPracticeByType(String examType,String type,
                                               String subjectCategoryId,String subjectChapterId,
                                               String userId,int page,int size){
        JSONObject res = new JSONObject();
        List<PracticQuest> practicQuests=new ArrayList<>();
        long total=0;
        Sort sort=new Sort(Sort.Direction.ASC,"createTime");
        Pageable pageable = new PageRequest(page-1, size, sort);
        if("do".equals(type)){
            if("all".equals(subjectChapterId)) {
                Page<PracticQuest> practicQuestsDoTrue = userDoPracticQuestDao.findPracticQuestByAnswerResultSubId(
                        examType, userId, subjectCategoryId, 1, pageable);
                Page<PracticQuest> practicQuestsDoFalse = userDoPracticQuestDao.findPracticQuestByAnswerResultSubId(
                        examType,userId,subjectCategoryId,2, pageable);
                total = practicQuestsDoTrue.getTotalElements() + practicQuestsDoFalse.getTotalElements();
                practicQuests.addAll(practicQuestsDoTrue.getContent());
                practicQuests.addAll(practicQuestsDoFalse.getContent());
            } else {
                Page<PracticQuest> practicQuestsDoTrue = userDoPracticQuestDao.findPracticQuestByAnswerResultChapId(
                        examType, userId, subjectChapterId, 1, pageable);
                Page<PracticQuest> practicQuestsDoFalse = userDoPracticQuestDao.findPracticQuestByAnswerResultChapId(
                        examType,userId,subjectChapterId,2, pageable);
                total = practicQuestsDoTrue.getTotalElements() + practicQuestsDoFalse.getTotalElements();
                practicQuests.addAll(practicQuestsDoTrue.getContent());
                practicQuests.addAll(practicQuestsDoFalse.getContent());
            }
        }else if("noDo".equals(type)){
            if("all".equals(subjectChapterId)) {
                List<String> questIds=userDoPracticQuestDao.findDoPracticQuestBySubId(examType,userId,subjectCategoryId);
                if(questIds.size()==0){
                    questIds.add("0");
                }
                List<PracticQuest>practicQuestsNoDo=practicQuestDao.findByIdNotIn(examType,subjectCategoryId,questIds);
                total=practicQuestsNoDo.size();
                practicQuests.addAll(practicQuestsNoDo);
            }else{
                List<String> questIds=userDoPracticQuestDao.findPracticQuestByChapId(examType,userId,subjectChapterId);
                if(questIds.size()==0){
                    questIds.add("0");
                }
                List<PracticQuest>practicQuestsNoDo=practicQuestDao.findByIdNotInAndChapterId(examType,subjectChapterId,questIds);
                total=practicQuestsNoDo.size();
                practicQuests.addAll(practicQuestsNoDo);
            }
        }else if("doTrue".equals(type)){
            if("all".equals(subjectChapterId)) {
                Page<PracticQuest> practicQuestsDoTrue = userDoPracticQuestDao.findPracticQuestByAnswerResultSubId(
                        examType,userId,subjectCategoryId, 1, pageable);
                total = practicQuestsDoTrue.getTotalElements();
                practicQuests.addAll(practicQuestsDoTrue.getContent());
            }else{
                Page<PracticQuest> practicQuestsDoTrue = userDoPracticQuestDao.findPracticQuestByAnswerResultChapId(
                        examType,userId,subjectChapterId, 1, pageable);
                total = practicQuestsDoTrue.getTotalElements();
                practicQuests.addAll(practicQuestsDoTrue.getContent());
            }
        }else if("doFalse".equals(type)){
            if("all".equals(subjectChapterId)) {
                Page<PracticQuest> practicQuestsDoFalse = userDoPracticQuestDao.findPracticQuestByAnswerResultSubId(
                        examType,userId,subjectCategoryId, 2, pageable);
                total = practicQuestsDoFalse.getTotalElements();
                practicQuests.addAll(practicQuestsDoFalse.getContent());
            }else{
                Page<PracticQuest> practicQuestsDoFalse = userDoPracticQuestDao.findPracticQuestByAnswerResultChapId(
                        examType,userId,subjectChapterId, 2, pageable);
                total = practicQuestsDoFalse.getTotalElements();
                practicQuests.addAll(practicQuestsDoFalse.getContent());
            }
        }else if("collect".equals(type)){
            if("all".equals(subjectChapterId)) {
                Page<PracticQuest> practicQuestsColl = userCollectPracticQuestDao.findCollQuestByUserIdSubId(
                        examType, userId,subjectCategoryId, pageable);
                total = practicQuestsColl.getTotalElements();
                practicQuests.addAll(practicQuestsColl.getContent());
            }else{
                Page<PracticQuest> practicQuestsColl = userCollectPracticQuestDao.findCollQuestByUserIdChapId(
                        examType, userId,subjectChapterId, pageable);
                total = practicQuestsColl.getTotalElements();
                practicQuests.addAll(practicQuestsColl.getContent());
            }
        }
        UserQuestSeq userQuestSeq=userQuestSeqDao.findByExamTypeAndUserIdAndSubjectCategoryIdAndSubjectChapterId(examType,
                userId,subjectCategoryId,subjectChapterId);
        Integer seq=1;
        if(userQuestSeq!=null){
            seq=userQuestSeq.getSeq();
        }
        List<UserDoPracticQuest>userDoPracticQuests=userDoPracticQuestDao.findObjsByExamType(examType,userId);
        List<UserCollectPracticQuest>userCollectPracticQuests=userCollectPracticQuestDao.findCollQuestsByUserId(examType,userId);
        Map<String,UserDoPracticQuest>userDoPracticQuestMap=new HashMap<>();
        Map<String,UserCollectPracticQuest>userCollectPracticQuestMap=new HashMap<>();
        for(UserDoPracticQuest userDoPracticQuest:userDoPracticQuests){
            userDoPracticQuestMap.put(userDoPracticQuest.getQuestId(),userDoPracticQuest);
        }
        for(UserCollectPracticQuest userCollectPracticQuest:userCollectPracticQuests){
            userCollectPracticQuestMap.put(userCollectPracticQuest.getQuestId(),userCollectPracticQuest);
        }
        JSONArray array=new JSONArray();
        for(PracticQuest practicQuest:practicQuests){
            UserDoPracticQuest userDoPracticQuest=userDoPracticQuestMap.get(practicQuest.getId());
            JSONObject json =(JSONObject)JSONObject.toJSON(practicQuest);
            UserCollectPracticQuest userCollectPracticQuest=userCollectPracticQuestMap.get(practicQuest.getId());
            if(userDoPracticQuest!=null){
                json.put("result",userDoPracticQuest.getResult());
                json.put("answerResult",userDoPracticQuest.getAnswerResult());
            }else{
                json.put("result",null);
                json.put("answerResult",0);
            }
            int isCollect=0;
            if(userCollectPracticQuest!=null){
                isCollect=1;
            }
            json.put("isCollect",isCollect);
            array.add(json);
        }
        Page<PracticQuest> practicQuestPage=practicQuestDao.findPracticQuestPage(examType,pageable);
        long totalQuestNum=practicQuestPage.getTotalElements();
        long userQuestTrueNum=userDoPracticQuestDao.findCountQuestByAnswerResult(examType,userId,1);
        long userQuestFalseNum=userDoPracticQuestDao.findCountQuestByAnswerResult(examType,userId,2);
        long userDoQuestNum=userQuestTrueNum+userQuestFalseNum;
        long userNoDoQuestNum=totalQuestNum-userDoQuestNum;
        double questCorrect=0;
        if(userDoQuestNum!=0){
            questCorrect= Arith.div((double)userQuestTrueNum,(double)userDoQuestNum,3);
        }
        long userQuestCollect=userCollectPracticQuestDao.findCountCollQuestByUserId(examType,userId);
        res.put("userQuestTrueNum",userQuestTrueNum);
        res.put("userQuestFalseNum",userQuestFalseNum);
        res.put("userDoQuestNum",userDoQuestNum);
        res.put("userNoDoQuestNum",userNoDoQuestNum);
        res.put("questCorrect",questCorrect);
        res.put("userQuestCollect",userQuestCollect);
        res.put("seq",seq);
        res.put("data",array);
        res.put("total",total);
        return res;
    }
}
