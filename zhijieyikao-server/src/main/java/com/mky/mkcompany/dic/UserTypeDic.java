package com.mky.mkcompany.dic;

public class UserTypeDic {
	/**
	 * 1 企业用户（唯一）
	 */
	public static final Integer COMPANYUSER = 1;
	/**
	 * 2 企业煤矿管理用户（唯一）
	 */
	public static final Integer COALUSER = 2;
	/**
	 * 3 企业煤矿普通用户 （多个）
	 */
	public static final Integer COALUSERMULIT = 3;
	/**
	 * 4 政府管理员
	 */
	public static final Integer GOVERNMENT = 4;
	/**
	 * 5 政府普通用户
	 */
	public static final Integer GOVERNMENTCOMMON = 5;
	
}
