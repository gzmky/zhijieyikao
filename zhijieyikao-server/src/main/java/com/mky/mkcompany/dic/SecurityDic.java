package com.mky.mkcompany.dic;

/**
 * Created by Administrator on 2016/9/20.
 */
public final class SecurityDic {
	/**
	 * 存储当前登录用户id的字段名
	 */
	public static final String CURRENT_USER_ID = "CURRENT_USER_ID";

	/**
	 * 存储前端传过来的用户id
	 */
	public static final String FRONT_USER_ID = "FRONT_USER_ID";

	/**
	 * token有效期（分钟）
	 */
	public static final int TOKEN_EXPIRES_HOUR = 60;

	/**
	 * token延长有效期（分钟）
	 */
	public static final int TOKEN_ADD_EXPIRES_HOUR = 6;

	/**
	 * 存放Authorization的header字段
	 */
	public static final String AUTHORIZATION = "Authorization";
}
