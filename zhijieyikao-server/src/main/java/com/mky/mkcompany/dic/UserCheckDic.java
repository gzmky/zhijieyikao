package com.mky.mkcompany.dic;

public class UserCheckDic {

	public static final Integer NO_CHECK= 0;//未审核

	public static final Integer PASS_CHECK = 1;//已通过审核

	public static final Integer NO_PASS_CHECK = 2;//未通过审核
}
