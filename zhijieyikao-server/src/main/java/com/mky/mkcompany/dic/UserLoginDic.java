package com.mky.mkcompany.dic;

/**
 * Created by Administrator on 2016/10/26.
 */
public class UserLoginDic {
    /**
     * 密码重试次数禁用有效期（分钟）
     */
    public static final int RETRY_COUNT_EXPIRES_MINUTES= 60;
    /**
     * 最大重试次数
     */
    public static final int MAX_RETRY_TIMES = 5;

    public static final String  PROJ_NAME ="mkcompany-gather-"; //存储redis用户id登录信息前缀key
    public static final String  PROJ_NAME_USERNAME ="mkcompany-gather-username"; //存储redis用户名登录信息前缀key
}
