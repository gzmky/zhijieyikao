package com.mky.mkcompany.dic;

/**
 * 用户的删除状态 或者禁用状态
 */
public final class DelStatusDic {
	/**
	 * 0 正常使用 禁用
	 */
	public static final Integer NOT_DELETE = 0;

	/**
	 * 1 已删除 启用
	 */
	public static final Integer IS_DELETE = 1;

}
