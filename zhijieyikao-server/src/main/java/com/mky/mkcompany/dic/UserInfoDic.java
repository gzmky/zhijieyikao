package com.mky.mkcompany.dic;

/**
 * Created by Administrator on 2016/12/30.
 */
public class UserInfoDic {
    public static final int PHONE_V_CODE_TIME=15;
    public static final int CONFIRM_EMAIL_TIME=24;
    public static final String CONFIRM_EMAIL="mky_confirmEmail";
    public static final String SMS_V_CODE="sms_v_code";
    public static final String UPLOAD_HEAD="upload_head";
    public static final String USER_AUTHEN="user_authen";
    public static final int USER_AUTHEN_TIME=1;
    public static final int UPLOAD_HEAD_TIME=24;
}
