package com.mky.mkcompany.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.stereotype.Service;

/**
 * 常量配置读取类
 *
 */
@Service
@RefreshScope
public class ConstantsConfig {
    @Value(value = "${spring.profiles.active}")
    private  String profilesActive;

    public String getProfilesActive() {
        return profilesActive;
    }

    public void setProfilesActive(String profilesActive) {
        this.profilesActive = profilesActive;
    }
}
