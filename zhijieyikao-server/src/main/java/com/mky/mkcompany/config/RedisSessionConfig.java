package com.mky.mkcompany.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.session.data.redis.config.annotation.web.http.EnableRedisHttpSession;

@Configuration
@EnableRedisHttpSession(redisNamespace = "mkcompany-gather")
public class RedisSessionConfig {  
}
