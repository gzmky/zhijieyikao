package com.mky.mkcompany.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSenderImpl;

import java.util.Properties;

/**
 * Created by Administrator on 2017/1/11.
 */
@Configuration
public class EmailConfig {

    @Bean
    public JavaMailSenderImpl javaMailSender(){
        JavaMailSenderImpl javaMailSender=new JavaMailSenderImpl();
        javaMailSender.setHost("smtp.qq.com");
        javaMailSender.setUsername("1060616964@qq.com");
        javaMailSender.setPassword("tygmhkyymgwkbahj");
        javaMailSender.setPort(465);
        javaMailSender.setProtocol("smtps");
        javaMailSender.setDefaultEncoding("UTF-8");
        Properties properties=new Properties();
        properties.setProperty("mail.smtps.auth","true");
        properties.setProperty("mail.smtp.ssl.enable","true");
        properties.setProperty("mail.transport.protocol","smtps");
        javaMailSender.setJavaMailProperties(properties);
        return  javaMailSender;
    }

    @Bean
    public SimpleMailMessage simpleMailMessage(){
        SimpleMailMessage simpleMailMessage=new SimpleMailMessage();
        simpleMailMessage.setFrom("1060616964@qq.com");
        return simpleMailMessage;
    }
}
