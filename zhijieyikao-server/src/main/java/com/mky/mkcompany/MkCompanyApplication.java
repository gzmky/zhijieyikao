package com.mky.mkcompany;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.context.annotation.Bean;
import org.springframework.web.filter.ShallowEtagHeaderFilter;

/**
 * Spring-Boot启动类
 * @author ScienJus
 * @date 2015/7/31.
 */
@SpringBootApplication
public class MkCompanyApplication {
    public static void main(String[] args) {
        new SpringApplicationBuilder(MkCompanyApplication.class).web(true).run(args);
    }
}
