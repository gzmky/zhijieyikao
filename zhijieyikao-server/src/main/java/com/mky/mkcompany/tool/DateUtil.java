package com.mky.mkcompany.tool;

import com.alibaba.fastjson.JSONArray;
import org.apache.commons.lang.StringUtils;

import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

/**
 * 时间工具类
 * 
 * @author zhurz
 */
public final class DateUtil {
	private DateUtil() {
		super();
	}

	// 直接使用静态变量会导致线程不安全
	// public final public SimpleDateFormat DATE_TIME_FORMAT = new
	// SimpleDateFormat("yyyy-MM-dd
	// HH:mm:ss");
	// public final public SimpleDateFormat DATE_TIME_FORMAT2 = new
	// SimpleDateFormat("yyyy-MM-dd
	// HH:mm:ss.SSS");
	// public final public SimpleDateFormat DATE_TIME_FORMAT3 = new
	// SimpleDateFormat("yyyyMMddHHmmss");
	// public final public SimpleDateFormat DATE_TIME_FORMAT4 = new
	// SimpleDateFormat("yyyy-MM-dd
	// HH:mm");
	// public final public SimpleDateFormat DATE_TIME_FORMAT5 = new
	// SimpleDateFormat("yyyy/MM/dd
	// HH:mm");
	// public final public SimpleDateFormat DATE_FORMAT = new
	// SimpleDateFormat("yyyyMMdd");
	// public final public SimpleDateFormat DATE_FORMAT2 = new
	// SimpleDateFormat("yyyy-MM-dd");
	// public final public SimpleDateFormat TIME_FORMAT = new
	// SimpleDateFormat("HHmmss");

	public final static SimpleDateFormat DATE_TIME_FORMAT() {
		return new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
	}

	public final static SimpleDateFormat DATE_TIME_FORMAT2() {
		return new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");
	}

	public final static SimpleDateFormat DATE_TIME_FORMAT3() {
		return new SimpleDateFormat("yyyyMMddHHmmss");
	}

	public final static SimpleDateFormat DATE_TIME_FORMAT4() {
		return new SimpleDateFormat("yyyy-MM-dd HH:mm");
	}

	public final static SimpleDateFormat DATE_TIME_FORMAT5() {
		return new SimpleDateFormat("yyyy/MM/dd HH:mm");
	}

	public final static SimpleDateFormat DATE_FORMAT() {
		return new SimpleDateFormat("yyyyMMdd");
	}

	public final static SimpleDateFormat DATE_FORMAT2() {
		return new SimpleDateFormat("yyyy-MM-dd");
	}

	public final static SimpleDateFormat TIME_FORMAT() {
		return new SimpleDateFormat("HHmmss");
	}

	/**
	 * 将日期字符串转换为 零点开始的日期(Date)对象
	 * 
	 * @param dateStr
	 *            日期字符串(yyyy-MM-dd)
	 * @return
	 */
	public static Date dateStr2BeginDate(String dateStr) {
		if (StringUtils.isNotBlank(dateStr)) {
			try {
				SimpleDateFormat beginFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
				return beginFormat.parse(dateStr + " 00:00:00");
			} catch (ParseException e) {
			}
		}
		return null;
	}

	/**
	 * 将日期字符串转换为 59分59秒999毫秒结束的日期(Date)对象
	 * 
	 * @param dateStr
	 *            日期字符串(yyyy-MM-dd)
	 * @return
	 */
	public static Date dateStr2EndDate(String dateStr) {
		if (StringUtils.isNotBlank(dateStr)) {
			try {
				SimpleDateFormat endFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
				return endFormat.parse(dateStr + " 23:59:59");
			} catch (ParseException e) {
			}
		}
		return null;
	}

	public static Date parseDateTime(String dateStr) {
		try {
			return DATE_TIME_FORMAT().parse(dateStr);
		} catch (Exception e) {
		}
		return null;
	}

	public static Date parseDate(String dateStr) {
		try {
			return DATE_TIME_FORMAT2().parse(dateStr);
		} catch (Exception e) {
		}
		return null;
	}

	/**
	 * 格式化日期 为yyyy-MM-dd HH:mm:ss
	 * 
	 * @param date
	 * @return
	 */
	public static String formatDateTime(Date date) {
		if (date != null) {
			return DATE_TIME_FORMAT().format(date);
		}
		return "";
	}

	/**
	 * 格式化日期为指定格式
	 * 
	 * @param date
	 * @param formatStr
	 * @return
	 */
	public static String formatDateTime(Date date, String formatStr) {
		if (date != null) {
			if (StringUtils.isNotBlank(formatStr)) {
				SimpleDateFormat format = new SimpleDateFormat(formatStr);
				return format.format(date);
			} else {
				return DATE_TIME_FORMAT().format(date);
			}
		}
		return "";
	}

	/**
	 * 格式化时间戳日期为指定格式
	 *
	 * @param timestamp
	 * @param formatStr
	 * @return
	 */
	public static String formatTimestampTime(Timestamp timestamp, String formatStr) {
		if (timestamp != null) {
			Date date=new Date(timestamp.getTime());
			if (StringUtils.isNotBlank(formatStr)) {
				SimpleDateFormat format = new SimpleDateFormat(formatStr);
				return format.format(date);
			} else {
				return DATE_TIME_FORMAT().format(date);
			}
		}
		return "";
	}


	public static Date parseDateTime(String dateStr, String formatStr) {
		if (dateStr != null) {
			if (!StringUtils.isBlank(formatStr)) {
				SimpleDateFormat format = new SimpleDateFormat(formatStr);
				try {
					return format.parse(dateStr);
				} catch (ParseException e) {
				}
			} else {
				try {
					return DATE_TIME_FORMAT().parse(dateStr);
				} catch (ParseException e) {
				}
			}
		}
		return null;
	}

	/**
	 * 格式化yyyyMMdd
	 * 
	 * @param dateStr
	 * @return
	 */
	public static Date parseDate2(String dateStr) {
		try {
			if (StringUtils.isBlank(dateStr)) {
				return null;
			}
			return DATE_FORMAT().parse(dateStr);
		} catch (ParseException e) {
		}
		return null;
	}

	/**
	 * 格式化yyyyMMddHHmmss
	 * 
	 * @param dateStr
	 * @return
	 */
	public static Date parseDate4(String dateStr) {
		try {
			if (StringUtils.isBlank(dateStr)) {
				return null;
			}
			return DATE_TIME_FORMAT3().parse(dateStr);
		} catch (ParseException e) {
		}
		return null;
	}

	/**
	 * 
	 * @方法功能说明：格式化yyyy-MM-dd HH:mm @修改者名字：tandeng @修改时间：2014年8月5日上午9:50:
	 *                       19 @修改内容： @参数：@param
	 *                       dateStr @参数：@return @return:Date @throws
	 */
	public static Date parseDate5(String dateStr) {
		try {
			if (StringUtils.isBlank(dateStr)) {
				return null;
			}
			return DATE_TIME_FORMAT4().parse(dateStr);
		} catch (ParseException e) {
		}
		return null;
	}

	/**
	 * 
	 * @方法功能说明：格式化yyyy/MM/dd HH:mm @修改者名字：tandeng @修改时间：2014年8月5日上午9:50:
	 *                       19 @修改内容： @参数：@param
	 *                       dateStr @参数：@return @return:Date @throws
	 */
	public static Date parseDate6(String dateStr) {
		try {
			if (StringUtils.isBlank(dateStr)) {
				return null;
			}
			return DATE_TIME_FORMAT5().parse(dateStr);
		} catch (ParseException e) {
		}
		return null;
	}

	/**
	 * YYYY-MM-DD
	 * 
	 * @param dateStr
	 * @return
	 */
	public static Date parseDate3(String dateStr) {
		try {
			return DATE_FORMAT2().parse(dateStr);
		} catch (ParseException e) {
		}
		return null;
	}

	/**
	 * HHmmss
	 * 
	 * @param timeStr
	 * @return
	 */
	public static Date parseTime(String timeStr) {
		try {
			return TIME_FORMAT().parse(timeStr);
		} catch (ParseException e) {
		}
		return null;
	}

	/**
	 * 获取当前时间戳
	 */
	public static Timestamp  getNowTimestamp() {
		return new Timestamp(new Date().getTime());
	}

	/**
	 * 今天之前几天
     */
	public static String getDay(int i,String formatStr){
		Date date=new Date();//取时间
		Calendar calendar = new GregorianCalendar();
		calendar.setTime(date);
		calendar.add(calendar.DATE,i);//把日期往后增加一天.整数往后推,负数往前移动
		date=calendar.getTime(); //这个时间就是日期往后推一天的结果
		SimpleDateFormat formatter = new SimpleDateFormat(formatStr);
		String dateString = formatter.format(date);
		return dateString;
	}

	/**
	 * 今天之前几月
	 */
	public static String getMonth(int i,String formatStr){
		Date date=new Date();//取时间
		Calendar calendar = new GregorianCalendar();
		calendar.setTime(date);
		calendar.add(calendar.MONTH,i); //把日期往后增加一月.整数往后推,负数往前移动
		date=calendar.getTime(); //这个时间就是日期往后推一月的结果
		SimpleDateFormat formatter = new SimpleDateFormat(formatStr);
		String dateString = formatter.format(date);
		return dateString;
	}

	/**
	 * 今天之前几年
	 */
	public static String getYear(int i,String formatStr){
		Date date=new Date();//取时间
		Calendar calendar = new GregorianCalendar();
		calendar.setTime(date);
		calendar.add(calendar.YEAR,i);//把日期往后增加一天.整数往后推,负数往前移动
		date=calendar.getTime(); //这个时间就是日期往后推一天的结果
		SimpleDateFormat formatter = new SimpleDateFormat(formatStr);
		String dateString = formatter.format(date);
		return dateString;
	}

	/**
	 * 今天之前几天
	 */
	public static long getDate(int i){
		Date date=new Date();//取时间
		Calendar calendar = new GregorianCalendar();
		calendar.setTime(date);
		calendar.add(calendar.DATE,i);//把日期往后增加一天.整数往后推,负数往前移动
		date=calendar.getTime(); //这个时间就是日期往后推一天的结果
		long resDate=date.getTime();
		return resDate;
	}

	/**
	 * 今天之前几天
	 */
	public static String getDay(Calendar calendar,int i,String formatStr){
		calendar.add(calendar.DATE,i);//把日期往后增加一天.整数往后推,负数往前移动
		Date date=calendar.getTime(); //这个时间就是日期往后推一天的结果
		SimpleDateFormat formatter = new SimpleDateFormat(formatStr);
		String dateString = formatter.format(date);
		return dateString;
	}

	/**
	 * 今天之前几月
	 */
	public static String getMonth(Calendar calendar,int i,String formatStr){
		calendar.add(calendar.MONTH,i);//把日期往后增加.整数往后推,负数往前移动
		Date date=calendar.getTime(); //这个时间就是日期往后推一天的结果
		SimpleDateFormat formatter = new SimpleDateFormat(formatStr);
		String dateString = formatter.format(date);
		return dateString;
	}

	/**
	 * 把毫秒转化成日期
	 * @param dateFormat(日期格式，例如：MM/ dd/yyyy HH:mm:ss)
	 * @param millSec(毫秒数)
	 * @return
	 */
	public static  String transferLongToDate(String dateFormat,Long millSec){
		SimpleDateFormat sdf = new SimpleDateFormat(dateFormat);
		Date date= new Date(millSec);
		return sdf.format(date);
	}

	
	/**
	 * 日期格式字符串转换成时间戳
	 *
	 * @param dateStr 字符串日期
	 * @param format   如：yyyy-MM-dd HH:mm:ss
	 *
	 * @return
	 */
	public static int Date2TimeStamp(String dateStr, String format) {
		try {
			SimpleDateFormat sdf = new SimpleDateFormat(format);
			return (int) sdf.parse(dateStr).getTime() / 1000;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return 0;
	}

	/**
	 * 返回日期格式数组-每天的数组
	 * @param startTime
	 * @param endTime
     * @return
     */
	public static JSONArray dateTimeArray( String startTime, String endTime,String formatStr,String resFormatStr){
		JSONArray res=new JSONArray();
		Date startDate= DateUtil.parseDateTime(startTime,formatStr);//取时间
		if(!resFormatStr.contains("-")){
			endTime=endTime.replaceAll("-","");
		}
		int i=0;
		while(true) {
			Calendar calendar = new GregorianCalendar();
			calendar.setTime(startDate);
			String date = DateUtil.getMonth(calendar, i++, resFormatStr);
			res.add(date);
			if(date.equals(endTime)){
				break;
			}
		}
		return res;
	}

	/**
	 * 返回日期格式数组-每月的数组
	 * @param startTime
	 * @param endTime
	 * @return
	 */
	public static JSONArray dateMonthTimeArray( String startTime, String endTime,String formatStr,String resFormatStr){
		JSONArray res=new JSONArray();
		Date startDate= DateUtil.parseDateTime(startTime,formatStr);//取时间
		if(!resFormatStr.contains("-")){
			endTime=endTime.replaceAll("-","");
		}
		int i=0;
		while(true) {
			Calendar calendar = new GregorianCalendar();
			calendar.setTime(startDate);
			String date = DateUtil.getMonth(calendar, i++, resFormatStr);
			res.add(date);
			if(date.equals(endTime)){
				break;
			}
		}
		return res;
	}


	/**
	 * 返回最近-每月的数组
	 * @return
	 */
	public static JSONArray dateTimeMonthArray(String formatStr,int month){
		JSONArray res=new JSONArray();
		while(month<=0){
			StringBuilder m=new StringBuilder();
			m.append(getMonth(month,formatStr)).append("月");
			month++;
			res.add(m.toString());
		}
		return res;
	}

	/**
	 * 根据指定时间格式获取某月有多少天
	 */
	public static int getMonthOfDays(String dateStr,String formatStr){
		SimpleDateFormat format = new SimpleDateFormat(formatStr);
		try {
			Date date = format.parse(dateStr);
			Calendar calendar = new GregorianCalendar();
			calendar.setTime(date);
			return  calendar.getActualMaximum(Calendar.DAY_OF_MONTH);
		} catch (ParseException e) {
			e.printStackTrace();
		}
		return 31;
	}

	/**
	 * 判断指定两个日期的大小
	 */
	public static boolean isDate1ToDATE2(int dateType,int i,Timestamp timestamp){
		Date date=new Date();//取时间
		Calendar calendar = new GregorianCalendar();
		calendar.setTime(date);
		calendar.add(dateType,i);
		date=calendar.getTime();
		if(date.getTime()>timestamp.getTime()){
			return true;
		}
		return false;
	}

	/**
	 * 计算两个日期的相差的分数
	 */
	public static int computeDateDifferMinutes(Long startDate,Long endDate){
		int days = (int) ((endDate - startDate) / (1000*60));
		return days;
	}

}
