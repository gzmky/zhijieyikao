package com.mky.mkcompany.tool;


import org.apache.shiro.crypto.hash.SimpleHash;
import org.apache.shiro.util.ByteSource;

public final class PasswordUtil {
	private PasswordUtil() {
		super();
	}

	public static final String encryptPassword(String password, String salt) {
		String algorithmName = "md5";
		int hashIterations = 2;
		String newPassword = new SimpleHash(algorithmName, password, ByteSource.Util.bytes(salt),
			hashIterations).toHex();
		return newPassword;
	}
}
