package com.mky.mkcompany.tool;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.mky.mkcompany.controller.domain.ExcelVo;
import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.util.ObjectUtils;
import org.springframework.web.multipart.MultipartFile;

/**
 * Created by XinXi-8 on 2018/10/11.
 */
public class ReadExcel {

    // 总行数
    private int totalRows = 0;
    // 总条数
    private int totalCells = 0;
    // 错误信息接收器
    private String errorMsg;

    // 构造方法
    public ReadExcel() {
    }

    // 获取总行数
    public int getTotalRows() {
        return totalRows;
    }

    // 获取总列数
    public int getTotalCells() {
        return totalCells;
    }

    // 获取错误信息
    public String getErrorInfo() {
        return errorMsg;
    }

    /**
     * 读EXCEL文件，获取信息集合
     * @return
     */
    public List<ExcelVo> getExcelInfo(MultipartFile mFile) {
        String fileName = mFile.getOriginalFilename();// 获取文件名
//        List<Map<String, Object>> userList = new LinkedList<Map<String, Object>>();
        try {
            if (!validateExcel(fileName)) {// 验证文件名是否合格
                return null;
            }
            boolean isExcel2003 = true;// 根据文件名判断文件是2003版本还是2007版本
            if (isExcel2007(fileName)) {
                isExcel2003 = false;
            }
            return createExcel(mFile.getInputStream(), isExcel2003);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * 根据excel里面的内容读取客户信息
     *
     * @param is      输入流
     * @param isExcel2003   excel是2003还是2007版本
     * @return
     * @throws IOException
     */
    public List<ExcelVo> createExcel(InputStream is, boolean isExcel2003) {
        try {
            Workbook wb = null;
            if (isExcel2003) {// 当excel是2003时,创建excel2003
                wb = new HSSFWorkbook(is);
            } else {// 当excel是2007时,创建excel2007
                wb = new XSSFWorkbook(is);
            }
            return readExcelValue(wb);// 读取Excel里面客户的信息
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * 读取Excel里面客户的信息
     *
     * @param wb
     * @return
     */
    private List<ExcelVo> readExcelValue(Workbook wb) {
        // 得到第一个shell
        Sheet sheet = wb.getSheetAt(0);
        // 得到Excel的行数
        this.totalRows = sheet.getPhysicalNumberOfRows();
        // 得到Excel的列数(前提是有行数)
        if (totalRows > 1 && sheet.getRow(0) != null) {
            this.totalCells = sheet.getRow(0).getPhysicalNumberOfCells();
        }
        List<ExcelVo> excelVoList=new ArrayList<>();
        // 循环Excel行数
        for (int r = 1; r < totalRows; r++) {
            Row row = sheet.getRow(r);
            if (row == null) {
                continue;
            }
            ExcelVo excelVo=new ExcelVo();
            //循环Excel的列
            for (int c = 0; c < this.totalCells; c++) {
                Cell cell = row.getCell(c);
                if (null != cell) {
                    if (c == 0) {
                        //如果是纯数字,比如你写的是25,cell.getNumericCellValue()获得是25.0,通过截取字符串去掉.0获得25
//                        if (cell.getCellType() == HSSFCell.CELL_TYPE_NUMERIC) {
//                            String type = String.valueOf(cell.getNumericCellValue());
//                            type=type.substring(0, type.length() - 2 > 0 ? type.length() - 2 : 1);
//                            excelVo.setType(type);    //题目归属
//                        } else {
//                            String type =cell.getStringCellValue();
//                            excelVo.setType(type);    //题目归属
//                        }
                        excelVo.setType(cell.getStringCellValue());//题目归属
                    } else if (c == 1) {
                        excelVo.setExamType(cell.getStringCellValue());//考试类型
                    } else if (c == 2) {
                        excelVo.setSubjectName(cell.getStringCellValue());// 学科名称
                    }else if (c == 3) {
                        excelVo.setChapterName(cell.getStringCellValue());//章节名称
                    }else if (c == 4) {
                        excelVo.setQuestType(cell.getStringCellValue());//题型
                    }else if (c == 5) {
                        excelVo.setTitle(cell.getStringCellValue());//题目标题
                    }else if (c == 6) {
                        excelVo.setOptiona(cell.getStringCellValue());//a选项
                    }else if (c == 7) {
                        excelVo.setOptionb(cell.getStringCellValue());
                    }else if (c == 8) {
                        excelVo.setOptionc(cell.getStringCellValue());
                    }else if (c == 9) {
                        excelVo.setOptiond(cell.getStringCellValue());
                    }else if (c == 10) {
                        excelVo.setOptione(cell.getStringCellValue());
                    }else if (c == 11) {
                        excelVo.setRightAnswer(cell.getStringCellValue());//正确答案
                    }else if (c == 12) {
                        excelVo.setAnswerParse(cell.getStringCellValue());  //答案解析
                    }else if (c == 13) {
                        excelVo.setTestCentre(cell.getStringCellValue());//考点
                    }else if (c == 14) {
                        excelVo.setName(cell.getStringCellValue());//课程名称或者试卷名称
                    }
                }
            }
            excelVoList.add(excelVo);
        }
        return excelVoList;
    }

    /**
     * 验证EXCEL文件
     *
     * @param filePath
     * @return
     */
    public boolean validateExcel(String filePath) {
        if (filePath == null || !(isExcel2003(filePath) || isExcel2007(filePath))) {
            errorMsg = "文件名不是excel格式";
            return false;
        }
        return true;
    }

    // @描述：是否是2003的excel，返回true是2003
    public static boolean isExcel2003(String filePath) {
        return filePath.matches("^.+\\.(?i)(xls)$");
    }

    // @描述：是否是2007的excel，返回true是2007
    public static boolean isExcel2007(String filePath) {
        return filePath.matches("^.+\\.(?i)(xlsx)$");
    }
}

