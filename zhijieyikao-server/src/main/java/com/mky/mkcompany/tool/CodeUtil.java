package com.mky.mkcompany.tool;

import org.apache.log4j.Logger;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by Administrator on 2016/10/17.
 */
public class CodeUtil {
    private final static Logger logger = Logger.getLogger(CodeUtil.class);
    public static String encodeStr(String queryParam){
        if (!isChineseChar(queryParam))
        {
            try {
                queryParam = new String(queryParam.getBytes("iso8859-1"), "utf-8");
            }catch (Exception e){
                logger.error("解码出错，详情："+e.getMessage(),e);
            }
        }
        return queryParam;
    }
    // 判断中文
    public static boolean isChineseChar(String str)
    {
        boolean temp = false;
        Pattern p=Pattern.compile("[\u4e00-\u9fa5]");
        Matcher m=p.matcher(str);
        if(m.find()){
            temp =  true;
        }
        return temp;
    }
}


