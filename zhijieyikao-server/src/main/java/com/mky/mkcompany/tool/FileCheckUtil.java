package com.mky.mkcompany.tool;

import com.mky.mkcompany.exception.WafException;
import org.springframework.http.HttpStatus;

import javax.imageio.ImageIO;
import java.awt.*;
import java.io.IOException;
import java.io.InputStream;

/**
 * Created by Administrator on 2017/1/12.
 */
public class FileCheckUtil {
    /**
     * 判断上传图片是否合法
     */
    public static void uploadImageCheck(InputStream is){
        boolean valid=true;
        try {
            Image image= ImageIO.read(is);
            if (image == null) {
                valid = false;
            }
        } catch(IOException ex) {
            valid=false;
        }
        if(!valid){
            throw new WafException("","上传的文件不是图片", HttpStatus.VARIANT_ALSO_NEGOTIATES);
        }
    }
}
