package com.mky.mkcompany.tool;

import org.springframework.security.authentication.encoding.Md5PasswordEncoder;
import org.springframework.security.authentication.encoding.ShaPasswordEncoder;

public class UserPassWordEncodeHelper {
	/**
	 * 普通用户密码加密hash两次，再sha三次
	 * @param password
	 * @param salt
	 * @return
	 */
	public static String encodePassword(String password,String salt){
		Md5PasswordEncoder md5PasswordEncoder = new Md5PasswordEncoder();
		md5PasswordEncoder.setEncodeHashAsBase64(false);
		password = md5PasswordEncoder.encodePassword(password, salt);
		password = md5PasswordEncoder.encodePassword(password, salt);
		ShaPasswordEncoder shaPasswordEncoder = new ShaPasswordEncoder();
		password = shaPasswordEncoder.encodePassword(password, salt);
		password = shaPasswordEncoder.encodePassword(password, salt);
		password = shaPasswordEncoder.encodePassword(password, salt);
		
		return password;
		
	};
	
	public static boolean match(String password,String salt,String dbPassword){
		password = encodePassword(password, salt);
		return password.equals(dbPassword);		
	}
}
