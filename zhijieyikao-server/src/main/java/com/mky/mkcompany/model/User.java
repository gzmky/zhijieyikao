package com.mky.mkcompany.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.springframework.data.annotation.Version;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import java.io.Serializable;
import java.math.BigInteger;
import java.sql.Timestamp;

/**
 * 
 * @author xjp
 *用户信息
 */
@Entity
public class User implements Serializable {
    private static final long serialVersionUID = 1L;
    /**
     * 主键
     */
    @Id
    private String   id;//用户id

    /**
     * 用户名称
     */
    @Column
    private String  username;

    /**
     *密码
     */
    @Column
    private String password;

    @Column
    private Timestamp firstLoginTime; //首次登录时间（首次使用系统登录的时间）

    @Column
    private Timestamp lastLoginTime; //最近一次登录时间

    /**
     * 是否锁定
     */
    @Column
    private Integer  valid; // 0 禁用 1 启用

    /**
     * 删除状态,0 没删除  1 已删除
     */
    @Column
    private Integer  delStatus;

    /**
     * 超级用户传admin，普通用户传ordinary，默认传ordinary
     */
    @Column
    private String type;

    /**
     * 审核状态,0 未审核 1 审核通过 2 审核未通过
     */
    @Column
    private Integer  checkStatus;

    /**
     * 用户salt
     */
    @Column(length = 150)
    private String  salt;

    @Column
    private String mobile; //手机

    @Column(length = 128)
    private String email; //邮箱
    /**
     * 创建时间
     */
    private Timestamp createTime;

    /**
     */
    private Timestamp updateTime;

    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Timestamp getFirstLoginTime() {
        return firstLoginTime;
    }

    public void setFirstLoginTime(Timestamp firstLoginTime) {
        this.firstLoginTime = firstLoginTime;
    }

    public Timestamp getLastLoginTime() {
        return lastLoginTime;
    }

    public void setLastLoginTime(Timestamp lastLoginTime) {
        this.lastLoginTime = lastLoginTime;
    }

    public Integer getValid() {
        return valid;
    }

    public void setValid(Integer valid) {
        this.valid = valid;
    }

    public Integer getDelStatus() {
        return delStatus;
    }

    public void setDelStatus(Integer delStatus) {
        this.delStatus = delStatus;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Integer getCheckStatus() {
        return checkStatus;
    }

    public void setCheckStatus(Integer checkStatus) {
        this.checkStatus = checkStatus;
    }

    public String getSalt() {
        return salt;
    }

    public void setSalt(String salt) {
        this.salt = salt;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Timestamp getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Timestamp createTime) {
        this.createTime = createTime;
    }

    public Timestamp getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Timestamp updateTime) {
        this.updateTime = updateTime;
    }
}
