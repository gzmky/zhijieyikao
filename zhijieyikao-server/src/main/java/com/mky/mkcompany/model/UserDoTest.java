package com.mky.mkcompany.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import java.io.Serializable;
import java.sql.Timestamp;
import java.util.Date;

/**
 *用用户做试卷历史记录表
 */
@Entity
public class UserDoTest implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    private String   id;

    @Column
    private String userId;      //用户id

    @Column
    private String testId;      //试卷id,关联试卷信息表

    @Column
    private Double score;      //得分

    @Column
    private String result;      //结果（及格、不及格）

    @Column
    private String handPaperId;  //交卷时的id，唯一标识此次交卷

    @Column
    private Long startTime;       //开始时间

    @Column
    private Long endTime;        //交卷时间

    @Column
    private Timestamp createTime;       //创建时间

    @Column
    private Timestamp updateTime;        //修改时间

    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    public String getHandPaperId() {
        return handPaperId;
    }

    public void setHandPaperId(String handPaperId) {
        this.handPaperId = handPaperId;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getTestId() {
        return testId;
    }

    public void setTestId(String testId) {
        this.testId = testId;
    }

    public Double getScore() {
        return score;
    }

    public void setScore(Double score) {
        this.score = score;
    }

    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result;
    }

    public Long getStartTime() {
        return startTime;
    }

    public void setStartTime(Long startTime) {
        this.startTime = startTime;
    }

    public Long getEndTime() {
        return endTime;
    }

    public void setEndTime(Long endTime) {
        this.endTime = endTime;
    }

    public Timestamp getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Timestamp createTime) {
        this.createTime = createTime;
    }

    public Timestamp getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Timestamp updateTime) {
        this.updateTime = updateTime;
    }
}
