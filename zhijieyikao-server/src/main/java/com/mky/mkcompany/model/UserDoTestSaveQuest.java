package com.mky.mkcompany.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import java.io.Serializable;
import java.sql.Timestamp;
import java.util.Date;

/**
 * Created by Lenovo on 2018/9/16.
 */
@Entity
public class UserDoTestSaveQuest implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    private String   id;

    @Column
    private String userId;      //用户id

    @Column
    private String testId;      //试卷id

    @Column
    private Integer minute;      //分
    @Column
    private Integer second;      //秒
    @Column
    private Integer noDoQuestNum;      //未作答题目数量

    @Column
    private Long startTime;       //开始答卷时间

    @Column
    private Long endTime;        //交卷时间

    @Column
    private Timestamp createTime;       //创建时间

    @Column
    private Timestamp updateTime;        //修改时间

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getTestId() {
        return testId;
    }

    public void setTestId(String testId) {
        this.testId = testId;
    }

    public Integer getMinute() {
        return minute;
    }

    public void setMinute(Integer minute) {
        this.minute = minute;
    }

    public Integer getSecond() {
        return second;
    }

    public void setSecond(Integer second) {
        this.second = second;
    }

    public Integer getNoDoQuestNum() {
        return noDoQuestNum;
    }

    public void setNoDoQuestNum(Integer noDoQuestNum) {
        this.noDoQuestNum = noDoQuestNum;
    }

    public Timestamp getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Timestamp createTime) {
        this.createTime = createTime;
    }

    public Timestamp getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Timestamp updateTime) {
        this.updateTime = updateTime;
    }

    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    public Long getStartTime() {
        return startTime;
    }

    public void setStartTime(Long startTime) {
        this.startTime = startTime;
    }

    public Long getEndTime() {
        return endTime;
    }

    public void setEndTime(Long endTime) {
        this.endTime = endTime;
    }
}
