package com.mky.mkcompany.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.metamodel.Type;
import javax.xml.soap.Text;
import java.io.Serializable;
import java.sql.Timestamp;

/**
 * 消息发布
 */
@Entity
public class MessageRelease implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    private String   id;

    @Column
    private String title;      //标题

    @Column
    private Integer type;      //类型，1表示新闻，2表示banner

    @Column
    private String titlePicUrl;      //图片url

    @Column(columnDefinition="longtext")
    private String describes;      //题目内容

    @Column
    private Timestamp createTime;       //创建时间

    @Column
    protected Timestamp updateTime;        //修改时间

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getTitlePicUrl() {
        return titlePicUrl;
    }

    public void setTitlePicUrl(String titlePicUrl) {
        this.titlePicUrl = titlePicUrl;
    }

    public String getDescribes() {
        return describes;
    }

    public void setDescribes(String describes) {
        this.describes = describes;
    }

    public Timestamp getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Timestamp createTime) {
        this.createTime = createTime;
    }

    public Timestamp getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Timestamp updateTime) {
        this.updateTime = updateTime;
    }
}
