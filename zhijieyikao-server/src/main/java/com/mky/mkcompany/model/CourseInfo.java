package com.mky.mkcompany.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import java.io.Serializable;
import java.sql.Timestamp;

/**
 *课程信息表
 */
@Entity
public class CourseInfo implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    private String   id;

    @Column
    private String courseSubjectId;      //课程科目id，关联课程科目表

    @Column
    private String name;      //课程名称

    /**
     * 考试类型（护士执业、初级护师、主管护师、临床执业医师、临床助理医师、执业中药师、执业西药师、乡村全科执业助理医师）
     */
    @Column
    private String examType;

    @Column
    private String courseCover; //课程封面

    @Column
    private String url; //课程视频地址

    @Column
    private String courseTime; //课程时长   cover

    @Column
    private String speaker; //主讲人

    @Column
    private Double schedule; //听课进度

    @Column
    private String homework; //课后练习(无、正在学习、未学习)

    @Column
    private Timestamp createTime;       //创建时间

    @Column
    private Timestamp updateTime;        //修改时间

    public String getCourseCover() {
        return courseCover;
    }

    public void setCourseCover(String courseCover) {
        this.courseCover = courseCover;
    }

    public String getExamType() {
        return examType;
    }

    public void setExamType(String examType) {
        this.examType = examType;
    }

    public Timestamp getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Timestamp createTime) {
        this.createTime = createTime;
    }

    public Timestamp getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Timestamp updateTime) {
        this.updateTime = updateTime;
    }

    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getCourseTime() {
        return courseTime;
    }

    public void setCourseTime(String courseTime) {
        this.courseTime = courseTime;
    }

    public String getSpeaker() {
        return speaker;
    }

    public void setSpeaker(String speaker) {
        this.speaker = speaker;
    }

    public Double getSchedule() {
        return schedule;
    }

    public void setSchedule(Double schedule) {
        this.schedule = schedule;
    }

    public String getHomework() {
        return homework;
    }

    public void setHomework(String homework) {
        this.homework = homework;
    }

    public String getCourseSubjectId() {
        return courseSubjectId;
    }

    public void setCourseSubjectId(String courseSubjectId) {
        this.courseSubjectId = courseSubjectId;
    }
}
