package com.mky.mkcompany.model.view;

/**
 * Created by Lenovo on 2018/9/17.
 */
public class UserFalseQuest {
    private String questId;
    private String chapterName;
    private String subjectName;
    private String testCentre;

    public UserFalseQuest(String questId,String subjectName,String chapterName,String testCentre){
        this.questId=questId;
        this.subjectName=subjectName;
        this.chapterName=chapterName;
        this.testCentre=testCentre;
    }

    public String getQuestId() {
        return questId;
    }

    public void setQuestId(String questId) {
        this.questId = questId;
    }

    public String getChapterName() {
        return chapterName;
    }

    public void setChapterName(String chapterName) {
        this.chapterName = chapterName;
    }

    public String getSubjectName() {
        return subjectName;
    }

    public void setSubjectName(String subjectName) {
        this.subjectName = subjectName;
    }

    public String getTestCentre() {
        return testCentre;
    }

    public void setTestCentre(String testCentre) {
        this.testCentre = testCentre;
    }
}
