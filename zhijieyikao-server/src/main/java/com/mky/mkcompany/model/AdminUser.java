package com.mky.mkcompany.model;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import java.io.Serializable;
import java.sql.Timestamp;

/**
 *
 * @author xjp
 *后台用户管理系统用户信息
 */
@Entity
public class AdminUser implements Serializable {
    private static final long serialVersionUID = 1L;
    /**
     * 主键
     */
    @Id
    private String   id;//用户id


    /**
     * 用户名称
     */
    @Column
    private String  username;

    /**
     *密码
     */
    @JsonIgnore
    @Column
    private String password;

    @Column
    private String mobile; //手机

    @Column(length = 128)
    private String email; //邮箱

    /**
     * 是否锁定
     */
    @Column
    private Integer  valid; // 0 禁用 1 启用

    /**
     * 用户类型
     */
    @Column
    private String type;

    /**
     * 用户salt
     */
    @JsonIgnore
    @Column(length = 150)
    private String  salt;

    /**
     * 创建时间
     */
    @JsonIgnore
    private Timestamp createTime;

    /**
     */
    @JsonIgnore
    private Timestamp updateTime;

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Integer getValid() {
        return valid;
    }

    public void setValid(Integer valid) {
        this.valid = valid;
    }

    public String getSalt() {
        return salt;
    }

    public void setSalt(String salt) {
        this.salt = salt;
    }

    public Timestamp getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Timestamp createTime) {
        this.createTime = createTime;
    }

    public Timestamp getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Timestamp updateTime) {
        this.updateTime = updateTime;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
}
