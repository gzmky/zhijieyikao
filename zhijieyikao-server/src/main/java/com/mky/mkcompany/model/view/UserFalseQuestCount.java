package com.mky.mkcompany.model.view;


import java.util.Date;

/**
 * Created by Lenovo on 2018/9/17.
 */
public class UserFalseQuestCount {
    private String questId;
    private Date endTime;
    private long num;

    public UserFalseQuestCount(String questId,Date endTime,long  num){
        this.questId=questId;
        this.endTime=endTime;
        this.num=num;
    }


    public Date getEndTime() {
        return endTime;
    }

    public void setEndTime(Date endTime) {
        this.endTime = endTime;
    }

    public String getQuestId() {
        return questId;
    }

    public void setQuestId(String questId) {
        this.questId = questId;
    }

    public long getNum() {
        return num;
    }

    public void setNum(long num) {
        this.num = num;
    }
}
