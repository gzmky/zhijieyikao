package com.mky.mkcompany.controller.domain;

import java.io.Serializable;
import java.sql.Timestamp;

/**
 *课程科目
 */
public class CourseSubjectRes implements Serializable {
    private static final long serialVersionUID = 1L;

    private String examType;  //考试类型

    private String courseCategoryName;     //课程分类名称

    private String courseCategoryId;     //课程分类id

    private String id;                   //课程科目id

    private String name;                //课程科目名称

    private Timestamp createTime;       //创建时间

    private Timestamp updateTime;        //修改时间

    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    public String getExamType() {
        return examType;
    }

    public void setExamType(String examType) {
        this.examType = examType;
    }

    public String getCourseCategoryName() {
        return courseCategoryName;
    }

    public void setCourseCategoryName(String courseCategoryName) {
        this.courseCategoryName = courseCategoryName;
    }

    public String getCourseCategoryId() {
        return courseCategoryId;
    }

    public void setCourseCategoryId(String courseCategoryId) {
        this.courseCategoryId = courseCategoryId;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Timestamp getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Timestamp createTime) {
        this.createTime = createTime;
    }

    public Timestamp getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Timestamp updateTime) {
        this.updateTime = updateTime;
    }
}
