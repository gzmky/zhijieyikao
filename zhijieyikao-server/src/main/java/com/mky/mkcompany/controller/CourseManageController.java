package com.mky.mkcompany.controller;


import com.alibaba.fastjson.JSONObject;
import com.mky.mkcompany.authorization.annotation.Authorization;
import com.mky.mkcompany.controller.domain.CourseInfoReq;
import com.mky.mkcompany.controller.domain.QuestListReq;
import com.mky.mkcompany.controller.domain.QuestReq;
import com.mky.mkcompany.model.*;
import com.mky.mkcompany.service.CourseManageService;
import com.mky.mkcompany.service.TopicManageService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * 题型管理
 */
@RestController
public class CourseManageController {
	private final Logger LOGGER = LoggerFactory.getLogger(getClass());
	@Autowired
	private CourseManageService courseManageService;
	/**
	 *获取课程分类列表
	 */
	@RequestMapping(value="/v0.1/getCourseCategoryList/{name}/{examType}/{page}/{size}" , method = RequestMethod.GET, produces = { "application/json;charset=UTF-8" })
	@Authorization
	public Object getCourseCategoryList(@PathVariable("name")String name,
										 @PathVariable("examType")String examType,
										 @PathVariable("page")int page,
										 @PathVariable("size")int size) {
		JSONObject res = new JSONObject();
		res=courseManageService.getCourseCategoryList(name,examType,page,size);
		res.put("success",true);
		res.put("status",200);
		return res;
	}

	/**
	 *新增,修改课程分类表
	 */
	@RequestMapping(value="/v0.1/saveCourseCategory" , method = RequestMethod.POST, produces = { "application/json;charset=UTF-8" })
	@Authorization
	public Object saveCourseCategory(@RequestBody CourseCategory courseCategory) {
		JSONObject res = new JSONObject();
		res=courseManageService.saveCourseCategory(courseCategory);
		res.put("message","保存成功！");
		res.put("success",true);
		res.put("status",200);
		return res;
	}

	/**
	 *删除学科分类表
	 */
	@RequestMapping(value="/v0.1/delCourseCategory/{id}" , method = RequestMethod.DELETE, produces = { "application/json;charset=UTF-8" })
	@Authorization
	public Object delCourseCategory(@PathVariable("id")String id) {
		JSONObject res = new JSONObject();
		courseManageService.delCourseCategory(id);
		res.put("message","删除成功！");
		res.put("success",true);
		res.put("status",200);
		return res;
	}



	/**
	 *获取课程科目表
	 */
	@RequestMapping(value="/v0.1/getCourseSubjectList/{examType}/{courseCategoryId}/{page}/{size}" , method = RequestMethod.GET, produces = { "application/json;charset=UTF-8" })
	@Authorization
	public Object getCourseSubjectList(@PathVariable("examType")String examType,
										@PathVariable("courseCategoryId")String courseCategoryId,
										@PathVariable("page")int page,
										@PathVariable("size")int size) {
		JSONObject res = new JSONObject();
		res=courseManageService.getCourseSubjectList(examType,courseCategoryId,page,size);
		res.put("success",true);
		res.put("status",200);
		return res;
	}


	/**
	 *新增,修改课程科目表
	 */
	@RequestMapping(value="/v0.1/saveCourseSubject" , method = RequestMethod.POST, produces = { "application/json;charset=UTF-8" })
	@Authorization
	public Object saveCourseSubject(@RequestBody CourseSubject courseSubject) {
		JSONObject res = new JSONObject();
		res=courseManageService.saveCourseSubject(courseSubject);
		res.put("message","保存成功！");
		res.put("success",true);
		res.put("status",200);
		return res;
	}

	/**
	 *删除课程科目表
	 */
	@RequestMapping(value="/v0.1/delCourseSubject/{id}" , method = RequestMethod.DELETE, produces = { "application/json;charset=UTF-8" })
	@Authorization
	public Object delCourseSubject(@PathVariable("id")String id) {
		JSONObject res = new JSONObject();
		courseManageService.delCourseSubject(id);
		res.put("message","删除成功！");
		res.put("success",true);
		res.put("status",200);
		return res;
	}


	/**
	 *获取课程信息表
	 */
	@RequestMapping(value="/v0.1/getCourseInfoList" , method = RequestMethod.POST, produces = { "application/json;charset=UTF-8" })
	@Authorization
	public Object getCourseInfoList(@RequestBody CourseInfoReq courseInfoReq) {
		JSONObject res = new JSONObject();
		res=courseManageService.getCourseInfoList(courseInfoReq);
		res.put("success",true);
		res.put("status",200);
		return res;
	}


	/**
	 *新增,修改课程信息表
	 */
	@RequestMapping(value="/v0.1/saveCourseInfo" , method = RequestMethod.POST, produces = { "application/json;charset=UTF-8" })
	@Authorization
	public Object saveCourseInfo(@RequestBody CourseInfo courseInfo) {
		JSONObject res = new JSONObject();
		res=courseManageService.saveCourseInfo(courseInfo);
		res.put("message","保存成功！");
		res.put("success",true);
		res.put("status",200);
		return res;
	}

	/**
	 *删除课程信息表
	 */
	@RequestMapping(value="/v0.1/delCourseInfo/{id}" , method = RequestMethod.DELETE, produces = { "application/json;charset=UTF-8" })
	//@Authorization
	public Object delCourseInfo(@PathVariable("id")String id) {
		JSONObject res = new JSONObject();
		courseManageService.delCourseInfo(id);
		res.put("message","删除成功！");
		res.put("success",true);
		res.put("status",200);
		return res;
	}




	/**
	 *获取课程分类下拉信息
	 */
	@RequestMapping(value="/v0.1/getCourseCategoryListXiaLa/{examType}" , method = RequestMethod.GET, produces = { "application/json;charset=UTF-8" })
	@Authorization
	public Object getCourseCategoryListXiaLa(@PathVariable("examType")String examType) {
		JSONObject res = new JSONObject();
		res=courseManageService.getCourseCategoryListXiaLa(examType);
		res.put("success",true);
		res.put("status",200);
		return res;
	}

	/**
	 *获取课程科目下拉信息
	 */
	@RequestMapping(value="/v0.1/getCourseSubjectListXiaLa/{courseCategoryId}" , method = RequestMethod.GET, produces = { "application/json;charset=UTF-8" })
	@Authorization
	public Object getCourseSubjectListXiaLa(@PathVariable("courseCategoryId")String courseCategoryId) {
		JSONObject res = new JSONObject();
		res=courseManageService.getCourseSubjectListXiaLa(courseCategoryId);
		res.put("success",true);
		res.put("status",200);
		return res;
	}

	/**
	 *获取课程下拉信息,用于添加课程视频题目
	 */
	@RequestMapping(value="/v0.1/getCourseInfoListXiaLa/{courseSubject}" , method = RequestMethod.GET, produces = { "application/json;charset=UTF-8" })
	@Authorization
	public Object getCourseInfoListXiaLa(@PathVariable("courseCategoryId")String courseCategoryId) {
		JSONObject res = new JSONObject();
		res=courseManageService.getCourseInfoListXiaLa();
		res.put("success",true);
		res.put("status",200);
		return res;
	}
}