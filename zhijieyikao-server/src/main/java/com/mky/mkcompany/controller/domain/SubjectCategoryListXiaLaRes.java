package com.mky.mkcompany.controller.domain;

import java.io.Serializable;
import java.sql.Timestamp;

/**
 *学科下拉
 */
public class SubjectCategoryListXiaLaRes implements Serializable {
    private static final long serialVersionUID = 1L;

    private String subjectName;     //学科名称

    private String subjectId;     //学科id

    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    public String getSubjectName() {
        return subjectName;
    }

    public void setSubjectName(String subjectName) {
        this.subjectName = subjectName;
    }

    public String getSubjectId() {
        return subjectId;
    }

    public void setSubjectId(String subjectId) {
        this.subjectId = subjectId;
    }
}
