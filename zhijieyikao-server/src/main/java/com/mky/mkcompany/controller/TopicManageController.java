package com.mky.mkcompany.controller;


import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.mky.mkcompany.authorization.annotation.Authorization;
import com.mky.mkcompany.controller.domain.DoPracticeReq;
import com.mky.mkcompany.controller.domain.QuestListReq;
import com.mky.mkcompany.controller.domain.QuestReq;
import com.mky.mkcompany.dic.Constants;
import com.mky.mkcompany.model.*;
import com.mky.mkcompany.service.DoExerciseService;
import com.mky.mkcompany.service.TopicManageService;
import com.mky.mkcompany.tool.FileDownloadUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 题型管理
 */
@RestController
public class TopicManageController {
	private final Logger LOGGER = LoggerFactory.getLogger(getClass());
	@Autowired
	private TopicManageService topicManageService;

	/**
	 *获取题型列表
     */
	@RequestMapping(value="/v0.1/getQuestTypeList/{name}/{page}/{size}" , method = RequestMethod.GET, produces = { "application/json;charset=UTF-8" })
	@Authorization
	public Object getQuestTypeList(@PathVariable("name")String name,
									@PathVariable("page")int page,
									@PathVariable("size")int size) {
		JSONObject res = new JSONObject();
		res=topicManageService.getQuestTypeList(name,page,size);
		res.put("success",true);
		res.put("status",200);
		return res;
	}

	/**
	 *新增,修改题型
	 */
	@RequestMapping(value="/v0.1/saveQuestType" , method = RequestMethod.POST, produces = { "application/json;charset=UTF-8" })
	@Authorization
	public Object saveQuestType(@RequestBody QuestType questType) {
		JSONObject res = new JSONObject();
		res=topicManageService.saveQuestType(questType);
		res.put("message","保存成功！");
		res.put("success",true);
		res.put("status",200);
		return res;
	}

	/**
	 *删除题型
	 */
	@RequestMapping(value="/v0.1/delQuestType/{id}" , method = RequestMethod.DELETE, produces = { "application/json;charset=UTF-8" })
	@Authorization
	public Object delQuestType(@PathVariable("id")String id) {
		JSONObject res = new JSONObject();
		topicManageService.delQuestType(id);
		res.put("message","删除成功！");
		res.put("success",true);
		res.put("status",200);
		return res;
	}


	/**
	 *获取学科分类表-----------------------------------------------------------------------------------------------------
	 */
	@RequestMapping(value="/v0.1/getSubjectCategoryList/{name}/{examType}/{page}/{size}" , method = RequestMethod.GET, produces = { "application/json;charset=UTF-8" })
	@Authorization
	public Object getSubjectCategoryList(@PathVariable("name")String name,
										 @PathVariable("examType")String examType,
										   @PathVariable("page")int page,
										   @PathVariable("size")int size) {
		JSONObject res = new JSONObject();
		res=topicManageService.getSubjectCategoryList(name,examType,page,size);
		res.put("success",true);
		res.put("status",200);
		return res;
	}

	/**
	 *新增,修改学科分类表
	 */
	@RequestMapping(value="/v0.1/saveSubjectCategory" , method = RequestMethod.POST, produces = { "application/json;charset=UTF-8" })
	@Authorization
	public Object saveSubjectCategory(@RequestBody SubjectCategory subjectCategory) {
		JSONObject res = new JSONObject();
		res=topicManageService.saveSubjectCategory(subjectCategory);
		res.put("message","保存成功！");
		res.put("success",true);
		res.put("status",200);
		return res;
	}

	/**
	 *删除学科分类表
	 */
	@RequestMapping(value="/v0.1/delSubjectCategory/{id}" , method = RequestMethod.DELETE, produces = { "application/json;charset=UTF-8" })
	@Authorization
	public Object delSubjectCategory(@PathVariable("id")String id) {
		JSONObject res = new JSONObject();
		topicManageService.delSubjectCategory(id);
		res.put("message","删除成功！");
		res.put("success",true);
		res.put("status",200);
		return res;
	}

	/**
	 *获取章节表---------------------------------------------------------------------------------------------------------
	 */
	@RequestMapping(value="/v0.1/getSubjectChapterList/{examType}/{subjectId}/{page}/{size}" , method = RequestMethod.GET, produces = { "application/json;charset=UTF-8" })
	@Authorization
	public Object getSubjectChapterList(@PathVariable("examType")String examType,
										@PathVariable("subjectId")String subjectId,
										 @PathVariable("page")int page,
										 @PathVariable("size")int size) {
		JSONObject res = new JSONObject();
		res=topicManageService.getSubjectChapterList(examType,subjectId,page,size);
		res.put("success",true);
		res.put("status",200);
		return res;
	}


	/**
	 *新增,修改章节表
	 */
	@RequestMapping(value="/v0.1/saveSubjectChapter" , method = RequestMethod.POST, produces = { "application/json;charset=UTF-8" })
	@Authorization
	public Object saveSubjectChapter(@RequestBody SubjectChapter subjectChapter) {
		JSONObject res = new JSONObject();
		res=topicManageService.saveSubjectChapter(subjectChapter);
		res.put("message","保存成功！");
		res.put("success",true);
		res.put("status",200);
		return res;
	}


	/**
	 *删除章节表
	 */
	@RequestMapping(value="/v0.1/delSubjectChapter/{id}" , method = RequestMethod.DELETE, produces = { "application/json;charset=UTF-8" })
	@Authorization
	public Object delSubjectChapter(@PathVariable("id")String id) {
		JSONObject res = new JSONObject();
		topicManageService.delSubjectChapter(id);
		res.put("message","删除成功！");
		res.put("success",true);
		res.put("status",200);
		return res;
	}


	/**
	 *获取试卷信息表-----------------------------------------------------------------------------------------------------
	 */
	@RequestMapping(value="/v0.1/getTestInfoList/{name}/{examType}/{page}/{size}" , method = RequestMethod.GET, produces = { "application/json;charset=UTF-8" })
	@Authorization
	public Object getTestInfoList(@PathVariable("name")String name,
								  @PathVariable("examType")String examType,
										@PathVariable("page")int page,
										@PathVariable("size")int size) {
		JSONObject res = new JSONObject();
		res=topicManageService.getTestInfoList(name,examType,page,size);
		res.put("success",true);
		res.put("status",200);
		return res;
	}

	/**
	 *新增,修改试卷信息表
	 */
	@RequestMapping(value="/v0.1/saveTestInfo" , method = RequestMethod.POST, produces = { "application/json;charset=UTF-8" })
	@Authorization
	public Object saveTestInfo(@RequestBody TestInfo testInfo) {
		JSONObject res = new JSONObject();
		res=topicManageService.saveTestInfo(testInfo);
		res.put("message","保存成功！");
		res.put("success",true);
		res.put("status",200);
		return res;
	}

	/**
	 *删除试卷信息表
	 */
	@RequestMapping(value="/v0.1/delTestInfo/{id}" , method = RequestMethod.DELETE, produces = { "application/json;charset=UTF-8" })
	@Authorization
	public Object delTestInfo(@PathVariable("id")String id) {
		JSONObject res = new JSONObject();
		topicManageService.delTestInfo(id);
		res.put("message","删除成功！");
		res.put("success",true);
		res.put("status",200);
		return res;
	}


	/**
	 *新增,修改题目表,包括模拟试卷题库，练习题库表，课程题库,根据type区分----------------------------------------------------
	 */
	@RequestMapping(value="/v0.1/saveQuest" , method = RequestMethod.POST, produces = { "application/json;charset=UTF-8" })
	@Authorization
	public Object saveQuest(@RequestBody QuestReq questReq) {
		JSONObject res = new JSONObject();
		res=topicManageService.saveQuest(questReq);
		res.put("message","保存成功！");
		res.put("success",true);
		res.put("status",200);
		return res;
	}

	/**
	 * spring boot 上传excel文件
	 */
	@RequestMapping(method = RequestMethod.POST, value = "/v0.1/uploadExcelFile")
	//@Authorization
	public Object uploadExcelFile(@RequestParam("file") MultipartFile file,
								  RedirectAttributes redirectAttributes, HttpServletRequest request) {
		JSONObject res = new JSONObject();
		LOGGER.info("上传文件开始file="+file);
		String result= topicManageService.readExcelFile(file);
		res.put("success",true);
		res.put("status",200);
		return res;
	}

	/**
	 *获取题目表,包括模拟试卷题库，练习题库表，课程题库,根据type区分
	 */
	@RequestMapping(value="/v0.1/getQuestList" , method = RequestMethod.POST, produces = { "application/json;charset=UTF-8" })
	@Authorization
	public Object getQuestList(@RequestBody QuestListReq questListReq) {
		JSONObject res = new JSONObject();
		res=topicManageService.getQuestList(questListReq);
		res.put("success",true);
		res.put("status",200);
		return res;
	}

	/**
	 *删除题目表,包括模拟试卷题库，练习题库表，课程题库,根据type区分
	 */
	@RequestMapping(value="/v0.1/delQuestList/{id}/{type}" , method = RequestMethod.DELETE, produces = { "application/json;charset=UTF-8" })
	@Authorization
	public Object delQuestList(@PathVariable("id")String id,@PathVariable("type")String type) {
		JSONObject res = new JSONObject();
		topicManageService.delQuestList(id,type);
		res.put("message","删除成功！");
		res.put("success",true);
		res.put("status",200);
		return res;
	}


	/**
	 *获取题型下拉-------------------------------------------------------------------------------------------------------
	 */
	@RequestMapping(value="/v0.1/getQuestTypeListXiaLa" , method = RequestMethod.GET, produces = { "application/json;charset=UTF-8" })
	@Authorization
	public Object getQuestTypeListXiaLa() {
		JSONObject res = new JSONObject();
		res=topicManageService.getQuestTypeListXiaLa();
		res.put("success",true);
		res.put("status",200);
		return res;
	}


	/**
	 *获取学科下拉信息
	 */
	@RequestMapping(value="/v0.1/getSubjectCategoryListXiaLa/{examType}" , method = RequestMethod.GET, produces = { "application/json;charset=UTF-8" })
	@Authorization
	public Object getSubjectCategoryListXiaLa(@PathVariable("examType")String examType) {
		JSONObject res = new JSONObject();
		res=topicManageService.getSubjectCategoryListXiaLa(examType);
		res.put("success",true);
		res.put("status",200);
		return res;
	}

	/**
	 *获取章节下拉信息
	 */
	@RequestMapping(value="/v0.1/getSubjectChapterListXiaLa/{subjectId}" , method = RequestMethod.GET, produces = { "application/json;charset=UTF-8" })
	@Authorization
	public Object getSubjectChapterListXiaLa(@PathVariable("subjectId")String subjectId) {
		JSONObject res = new JSONObject();
		res=topicManageService.getSubjectChapterListXiaLa(subjectId);
		res.put("success",true);
		res.put("status",200);
		return res;
	}

	/**
	 *获取试卷题库,课程题库下拉
	 */
	@RequestMapping(value="/v0.1/getQuestXiaLa/{type}" , method = RequestMethod.GET, produces = { "application/json;charset=UTF-8" })
	@Authorization
	public Object getQuestXiaLa(@PathVariable("type")String type) {
		JSONObject res = new JSONObject();
		res=topicManageService.getQuestXiaLa(type);
		res.put("success",true);
		res.put("status",200);
		return res;
	}
}