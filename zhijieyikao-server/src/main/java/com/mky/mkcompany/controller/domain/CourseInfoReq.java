package com.mky.mkcompany.controller.domain;

/**
 * Created by Lenovo on 2018/9/15.
 */
public class CourseInfoReq {
    /**
     * 考试类型（护士执业、初级护师、主管护师、临床执业医师、临床助理医师、执业中药师、执业西药师、乡村全科执业助理医师）
     */
    private String examType;

    private String courseCategoryId;         //课程分类Id

    private String courseSubjectId;                 //课程科目Id

    private int page;

    private int size;


    public String getExamType() {
        return examType;
    }

    public void setExamType(String examType) {
        this.examType = examType;
    }

    public String getCourseCategoryId() {
        return courseCategoryId;
    }

    public void setCourseCategoryId(String courseCategoryId) {
        this.courseCategoryId = courseCategoryId;
    }

    public String getCourseSubjectId() {
        return courseSubjectId;
    }

    public void setCourseSubjectId(String courseSubjectId) {
        this.courseSubjectId = courseSubjectId;
    }

    public int getPage() {
        return page;
    }

    public void setPage(int page) {
        this.page = page;
    }

    public int getSize() {
        return size;
    }

    public void setSize(int size) {
        this.size = size;
    }
}


