package com.mky.mkcompany.controller;


import com.alibaba.fastjson.JSONObject;
import com.mky.mkcompany.authorization.annotation.Authorization;
import com.mky.mkcompany.controller.domain.CourseInfoReq;
import com.mky.mkcompany.dic.Constants;
import com.mky.mkcompany.model.*;
import com.mky.mkcompany.service.CourseManageService;
import com.mky.mkcompany.service.UserManageService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;

/**
 *用户管理
 */
@RestController
public class UserManageController {
	private final Logger LOGGER = LoggerFactory.getLogger(getClass());
	@Autowired
	private UserManageService userManageService;
	/**
	 *获取用户列表
	 */
	@RequestMapping(value="/v0.1/getUserList/{mobile}/{page}/{size}" , method = RequestMethod.GET, produces = { "application/json;charset=UTF-8" })
	//@Authorization
	public Object getUserList(@PathVariable("mobile")String mobile,
							  @PathVariable("page")int page,
							  @PathVariable("size")int size) {
		JSONObject res = new JSONObject();
		res=userManageService.getUserList(mobile,page,size);
		res.put("success",true);
		res.put("status",200);
		return res;
	}

	/**
	 * 保存用户信息
	 */
	@RequestMapping(value="/v0.1/saveUser" , method = RequestMethod.POST, produces = { "application/json;charset=UTF-8" })
	@Authorization
	public Object saveUser(@RequestBody User user) {
		JSONObject res = new JSONObject();
		res=userManageService.saveUser(user);
		res.put("message","保存成功！");
		res.put("success",true);
		res.put("status",200);
		return res;
	}

	/**
	 *删除用户信息
	 */
	@RequestMapping(value="/v0.1/delUser/{id}/{type}" , method = RequestMethod.DELETE, produces = { "application/json;charset=UTF-8" })
	@Authorization
	public Object delUser(@PathVariable("id")String id,@PathVariable("type")String type) {
		JSONObject res = new JSONObject();
		userManageService.delUser(id,type);
		res.put("message","删除成功！");
		res.put("success",true);
		res.put("status",200);
		return res;
	}

	/**
	 * 修改密码 type 1 管理员  2 普通用户
	 */
	@RequestMapping(value="/v0.1/updatePassword/{userId}/{type}/{newPassword}" , method = RequestMethod.GET, produces = { "application/json;charset=UTF-8" })
	@Authorization
	public Object updatePassword(HttpServletRequest request,
								 @PathVariable String userId,
								 @PathVariable String type,
								 @PathVariable String newPassword) {
		JSONObject res = new JSONObject();
		res=userManageService.updatePassword(type,userId,newPassword);
		res.put("message","修改成功！");
		res.put("success",true);
		res.put("status",200);
		return res;
	}

	/**
	 * 重置密码
	 */
	@RequestMapping(value="/v0.1/resetPassword/{userId}/{password}" , method = RequestMethod.GET, produces = { "application/json;charset=UTF-8" })
	@Authorization
	public Object resetPassword(@PathVariable String userId,
								@PathVariable String password,HttpServletRequest request) {
		JSONObject res = new JSONObject();
		res=userManageService.resetPassword(password,userId);
		res.put("message","重置成功！");
		res.put("success",true);
		res.put("status",200);
		return res;
	}


	/**
	 *获取用户课程信息
	 */
	@RequestMapping(value="/v0.1/getUserCourseList/{mobile}/{page}/{size}" , method = RequestMethod.GET, produces = { "application/json;charset=UTF-8" })
	@Authorization
	public Object getUserCourseList(@PathVariable("mobile")String mobile,
							  @PathVariable("page")int page,
							  @PathVariable("size")int size) {
		JSONObject res = new JSONObject();
		res=userManageService.getUserCourseList(mobile,page,size);
		res.put("success",true);
		res.put("status",200);
		return res;
	}

	/**
	 * 保存用户课程信息
	 */
	@RequestMapping(value="/v0.1/saveUserCourse" , method = RequestMethod.POST, produces = { "application/json;charset=UTF-8" })
	@Authorization
	public Object saveUserCourse(@RequestBody UserCourse userCourse,HttpServletRequest request) {
		JSONObject res = new JSONObject();
		String userId=(String) request.getAttribute(Constants.CURRENT_USER_ID);
		res=userManageService.saveUserCourse(userCourse,userId);
		res.put("message","保存成功！");
		res.put("success",true);
		res.put("status",200);
		return res;
	}

	/**
	 * 取消用户分配的课程
	 */
	@RequestMapping(value="/v0.1/deleteUserCourse" , method = RequestMethod.POST, produces = { "application/json;charset=UTF-8" })
	@Authorization
	public Object deleteUserCourse(@RequestBody UserCourse userCourse,HttpServletRequest request) {
		JSONObject res = new JSONObject();
		String userId=(String) request.getAttribute(Constants.CURRENT_USER_ID);
		userManageService.deleteUserCourse(userCourse);
		res.put("message","取消成功！");
		res.put("success",true);
		res.put("status",200);
		return res;
	}


	/**
	 *获取用户考试类型信息
	 */
	@RequestMapping(value="/v0.1/getUserExamTypeInfo" , method = RequestMethod.GET, produces = { "application/json;charset=UTF-8" })
	@Authorization
	public Object getUserExamTypeInfo(HttpServletRequest request) {
		JSONObject res = new JSONObject();
		String userId=(String) request.getAttribute(Constants.CURRENT_USER_ID);
		res=userManageService.getUserExamTypeInfo(userId);
		res.put("success",true);
		res.put("status",200);
		return res;
	}

	/**
	 * 保存用户考试类型
	 */
	@RequestMapping(value="/v0.1/saveUserExamType" , method = RequestMethod.POST, produces = { "application/json;charset=UTF-8" })
	@Authorization
	public Object saveUserExamType(@RequestBody UserExamType userExamType,HttpServletRequest request) {
		JSONObject res = new JSONObject();
		String userId=(String) request.getAttribute(Constants.CURRENT_USER_ID);
		res=userManageService.saveUserExamType(userExamType,userId);
		res.put("message","保存成功！");
		res.put("success",true);
		res.put("status",200);
		return res;
	}



	/**
	 *获取用户下拉
	 */
	@RequestMapping(value="/v0.1/getUserListXiaLa/{userName}" , method = RequestMethod.GET, produces = { "application/json;charset=UTF-8" })
	@Authorization
	public Object getUserListXiaLa(@PathVariable("userName")String userName) {
		JSONObject res = new JSONObject();
		res=userManageService.getUserListXiaLa(userName);
		res.put("success",true);
		res.put("status",200);
		return res;
	}


	/**
	 * 保存消新闻发布，banner，信息
	 */
	@RequestMapping(value="/v0.1/saveMessageRelease" , method = RequestMethod.POST, produces = { "application/json;charset=UTF-8" })
	@Authorization
	public Object saveMessageRelease(@RequestBody MessageRelease messageRelease,HttpServletRequest request) {
		JSONObject res = new JSONObject();
		String userId=(String) request.getAttribute(Constants.CURRENT_USER_ID);
		res=userManageService.saveMessageRelease(messageRelease,userId);
		res.put("message","保存成功！");
		res.put("success",true);
		res.put("status",200);
		return res;
	}

	/**
	 *获取新闻发布，banner，信息
	 */
	@RequestMapping(value="/v0.1/getMessageRelease/{type}/{page}/{size}" , method = RequestMethod.GET, produces = { "application/json;charset=UTF-8" })
	//@Authorization
	public Object getMessageRelease(@PathVariable("type")int type,@PathVariable("page")int page,
									@PathVariable("size")int size,HttpServletRequest request) {
		JSONObject res = new JSONObject();
		String userId=(String) request.getAttribute(Constants.CURRENT_USER_ID);
		res=userManageService.getMessageRelease(type,page,size,userId);
		res.put("success",true);
		res.put("status",200);
		return res;
	}

	/**
	 *删除新闻发布，banner，信息
	 */
	@RequestMapping(value="/v0.1/delMessageRelease/{id}" , method = RequestMethod.DELETE, produces = { "application/json;charset=UTF-8" })
	@Authorization
	public Object delMessageRelease(@PathVariable("id")String id) {
		JSONObject res = new JSONObject();
		userManageService.delMessageRelease(id);
		res.put("message","删除成功！");
		res.put("success",true);
		res.put("status",200);
		return res;
	}


}