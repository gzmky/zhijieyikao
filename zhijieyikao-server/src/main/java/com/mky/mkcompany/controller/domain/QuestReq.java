package com.mky.mkcompany.controller.domain;

import javax.persistence.Column;
import javax.persistence.Id;
import java.sql.Timestamp;

/**
 * Created by Lenovo on 2018/9/15.
 */
public class QuestReq {

    private String type;

    private String   id;

    private String testId;      //试卷id

    private String courseId;      //课程id

    private String questType;      //题目类型（A、B、C、X），这个不传id,传名称过来，特殊
    /**
     * 考试类型（护士执业、初级护师、主管护师、临床执业医师、临床助理医师、执业中药师、执业西药师、乡村全科执业助理医师）
     */
    private String examType;

    private String title;      //题目标题

    private String describes;      //题目描述

    private String titlePicUrl;      //题目标题图片url地址

    private String optiona;      //选项a，文本或图片

    private String optionb;      //选项b，文本或图片

    private String optionc;      //选项c，文本或图片

    private String optiond;      //选项d，文本或图片

    private String optione;      //选项e，文本或图片

    private Integer optionType;      //选项类型，1 单选 2 多选

    private String rightAnswer;      //正确答案

    private String answerParse;      //答案解析

    private String testCentre;      //考点

    private String chapterId;      //如果是题目，试卷就关联章节id，关联学科章节表,如果是课程就关联课程科目id，关联课程科目表,

    private Timestamp createTime;       //创建时间

    private Timestamp updateTime;        //修改时间

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTestId() {
        return testId;
    }

    public void setTestId(String testId) {
        this.testId = testId;
    }

    public String getCourseId() {
        return courseId;
    }

    public void setCourseId(String courseId) {
        this.courseId = courseId;
    }

    public String getQuestType() {
        return questType;
    }

    public void setQuestType(String questType) {
        this.questType = questType;
    }

    public String getExamType() {
        return examType;
    }

    public void setExamType(String examType) {
        this.examType = examType;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescribes() {
        return describes;
    }

    public void setDescribes(String describes) {
        this.describes = describes;
    }

    public String getTitlePicUrl() {
        return titlePicUrl;
    }

    public void setTitlePicUrl(String titlePicUrl) {
        this.titlePicUrl = titlePicUrl;
    }

    public String getOptiona() {
        return optiona;
    }

    public void setOptiona(String optiona) {
        this.optiona = optiona;
    }

    public String getOptionb() {
        return optionb;
    }

    public void setOptionb(String optionb) {
        this.optionb = optionb;
    }

    public String getOptionc() {
        return optionc;
    }

    public void setOptionc(String optionc) {
        this.optionc = optionc;
    }

    public String getOptiond() {
        return optiond;
    }

    public void setOptiond(String optiond) {
        this.optiond = optiond;
    }

    public String getOptione() {
        return optione;
    }

    public void setOptione(String optione) {
        this.optione = optione;
    }

    public Integer getOptionType() {
        return optionType;
    }

    public void setOptionType(Integer optionType) {
        this.optionType = optionType;
    }

    public String getRightAnswer() {
        return rightAnswer;
    }

    public void setRightAnswer(String rightAnswer) {
        this.rightAnswer = rightAnswer;
    }

    public String getAnswerParse() {
        return answerParse;
    }

    public void setAnswerParse(String answerParse) {
        this.answerParse = answerParse;
    }

    public String getTestCentre() {
        return testCentre;
    }

    public void setTestCentre(String testCentre) {
        this.testCentre = testCentre;
    }

    public String getChapterId() {
        return chapterId;
    }

    public void setChapterId(String chapterId) {
        this.chapterId = chapterId;
    }

    public Timestamp getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Timestamp createTime) {
        this.createTime = createTime;
    }

    public Timestamp getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Timestamp updateTime) {
        this.updateTime = updateTime;
    }
}
