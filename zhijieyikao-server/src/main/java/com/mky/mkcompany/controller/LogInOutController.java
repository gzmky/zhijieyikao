package com.mky.mkcompany.controller;

import com.alibaba.fastjson.JSONObject;
import com.mky.mkcompany.authorization.annotation.Authorization;
import com.mky.mkcompany.controller.domain.UserRequest;
import com.mky.mkcompany.dao.UserDao;
import com.mky.mkcompany.authorization.manager.TokenManager;
import com.mky.mkcompany.dic.Constants;
import com.mky.mkcompany.dic.SecurityDic;
import com.mky.mkcompany.dic.UserLoginDic;
import com.mky.mkcompany.service.LoginOutService;
import com.mky.mkcompany.tool.IpUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.client.ServiceInstance;
import org.springframework.cloud.client.discovery.DiscoveryClient;
import org.springframework.util.Assert;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;

/**
 * 获取和删除token的请求地址，在Restful设计中其实就对应着登录和退出登录的资源映射
 */
@RestController
public class LogInOutController {
    private final Logger LOGGER = LoggerFactory.getLogger(getClass());
    @Autowired
    private UserDao userDao;
    @Autowired
    private TokenManager tokenManager;
    @Autowired
    private LoginOutService loginOutService;
//    @Autowired
//    private LoginOutService loginOutService;


    /**
     * 后台登录
     * @param request
     * @return
     */
    @RequestMapping(value ="/v0.1/userV2",method = RequestMethod.POST)
    public Object loginV2(@RequestBody UserRequest userRequest,HttpServletRequest request) {
        String username=userRequest.getUsername();
        String password=userRequest.getPassword();
        Assert.notNull(password, "password can not be empty");
        Assert.notNull(username, "username can not be empty");
        JSONObject res=new JSONObject();
        res=loginOutService.loginV2(userRequest);
        LOGGER.info("登录成功-username="+username);
        String ip= IpUtil.getIpAddr(request);
        LOGGER.info("/user/login, host-ip:" + ip );
        return res;
    }


    /**
     * 前台登录
     * @param request
     * @return
     */
    @RequestMapping(value ="/v0.1/user",method = RequestMethod.POST)
    public Object login(@RequestBody UserRequest userRequest,HttpServletRequest request) {
        String username=userRequest.getUsername();
        String password=userRequest.getPassword();
        Assert.notNull(password, "password can not be empty");
        Assert.notNull(username, "username can not be empty");
        JSONObject res=new JSONObject();
        res=loginOutService.login(userRequest);
        LOGGER.info("登录成功-username="+username);
        String ip= IpUtil.getIpAddr(request);
        LOGGER.info("/user/login, host-ip:" + ip );
        return res;
    }



    @RequestMapping(value ="/v0.1/user",method = RequestMethod.DELETE)
    @Authorization
    public Object logout(HttpServletRequest request) {
        JSONObject res=new JSONObject();
        String userId=(String) request.getAttribute(Constants.CURRENT_USER_ID);
        LOGGER.info("userId="+userId);
       tokenManager.deleteToken(userId);
        res.put("success",true);
        res.put("status",200);
        res.put("message","注销成功");
        LOGGER.info("注销成功");
        String ip= IpUtil.getIpAddr(request);
        LOGGER.info("/user/logout, host-ip:" + ip );
        return res;
    }
}
