package com.mky.mkcompany.controller.domain;

/**
 * Created by Lenovo on 2018/9/17.
 */
public class MyFalseQuestRes {
    private String questId;
    private String questName;//题干
    private String subjectName;
    private String chapterName;
    private String endTime;//答题时间
    private Integer answerFalseNum;//答错次数
    private Integer answerFalsePersonNum;//答错人数

    public String getQuestId() {
        return questId;
    }

    public void setQuestId(String questId) {
        this.questId = questId;
    }

    public String getQuestName() {
        return questName;
    }

    public void setQuestName(String questName) {
        this.questName = questName;
    }

    public String getSubjectName() {
        return subjectName;
    }

    public void setSubjectName(String subjectName) {
        this.subjectName = subjectName;
    }

    public String getChapterName() {
        return chapterName;
    }

    public void setChapterName(String chapterName) {
        this.chapterName = chapterName;
    }

    public String getEndTime() {
        return endTime;
    }

    public void setEndTime(String endTime) {
        this.endTime = endTime;
    }

    public Integer getAnswerFalseNum() {
        return answerFalseNum;
    }

    public void setAnswerFalseNum(Integer answerFalseNum) {
        this.answerFalseNum = answerFalseNum;
    }

    public Integer getAnswerFalsePersonNum() {
        return answerFalsePersonNum;
    }

    public void setAnswerFalsePersonNum(Integer answerFalsePersonNum) {
        this.answerFalsePersonNum = answerFalsePersonNum;
    }
}
