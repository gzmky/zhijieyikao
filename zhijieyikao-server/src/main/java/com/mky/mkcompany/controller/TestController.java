package com.mky.mkcompany.controller;


import com.alibaba.fastjson.JSONObject;
import com.mky.mkcompany.service.TestService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.math.BigDecimal;

@RestController
public class TestController {
	private final Logger LOGGER = LoggerFactory.getLogger(getClass());
	@Autowired
	private TestService testService;

	@RequestMapping(value="/test/{name}" , method = RequestMethod.GET, produces = { "application/json;charset=UTF-8" })
	//@Authorization
	public Object test(@PathVariable("name")String name) {
		JSONObject res = new JSONObject();

		res.put("success",true);
		return res;
	}
}