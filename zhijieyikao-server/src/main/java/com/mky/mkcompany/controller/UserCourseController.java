package com.mky.mkcompany.controller;


import com.alibaba.fastjson.JSONObject;
import com.mky.mkcompany.authorization.annotation.Authorization;
import com.mky.mkcompany.controller.domain.DoCourseSaveReq;
import com.mky.mkcompany.controller.domain.DoTestSaveReq;
import com.mky.mkcompany.dic.Constants;
import com.mky.mkcompany.service.TestService;
import com.mky.mkcompany.service.UserCourseService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;

@RestController
public class UserCourseController {
	private final Logger LOGGER = LoggerFactory.getLogger(getClass());
	@Autowired
	private UserCourseService userCourseService;

	/**
	 *获取课程分类数据
	 */
	@RequestMapping(value="/v0.1/courseCategory/{examType}" , method = RequestMethod.GET, produces = { "application/json;charset=UTF-8" })
	@Authorization
	public Object getCategoryChapter(@PathVariable("examType")String examType ,
									HttpServletRequest request) {
		JSONObject res = new JSONObject();
		String userId=(String) request.getAttribute(Constants.CURRENT_USER_ID);
		res=userCourseService.getCategoryChapter(examType);
		res.put("success",true);
		return res;
	}

	/**
	 *获取课程科目数据
	 */
	@RequestMapping(value="/v0.1/courseSubject/{courseCategoryId}" , method = RequestMethod.GET, produces = { "application/json;charset=UTF-8" })
	@Authorization
	public Object getCategorySubject(@PathVariable("courseCategoryId")String courseCategoryId ,
									 HttpServletRequest request) {
		JSONObject res = new JSONObject();
		String userId=(String) request.getAttribute(Constants.CURRENT_USER_ID);
		res=userCourseService.getCategorySubject(courseCategoryId);
		res.put("success",true);
		return res;
	}

	/**
	 *获取课程列表数据
     */
	@RequestMapping(value="/v0.1/courseListData/{examType}/{courseCategoryId}/{courseChapterId}/{page}/{size}" , method = RequestMethod.GET, produces = { "application/json;charset=UTF-8" })
	@Authorization
	public Object getCourseListData(@PathVariable("examType")String examType ,
									@PathVariable("courseCategoryId")String courseCategoryId ,
									@PathVariable("courseChapterId")String courseChapterId ,
									@PathVariable("page")int page,
									@PathVariable("size")int size,
									HttpServletRequest request) {
		JSONObject res = new JSONObject();
		String userId=(String) request.getAttribute(Constants.CURRENT_USER_ID);
		res=userCourseService.getCourseListData(examType,courseCategoryId,courseChapterId,userId,page,size);
		res.put("success",true);
		return res;
	}
	/**
	 * 我的课程-获取做课程历史信息
	 */
	@RequestMapping(value="/v0.1/doCourseHistoryInfo/{courseId}" , method = RequestMethod.GET, produces = { "application/json;charset=UTF-8" })
	@Authorization
	public Object getDoCourseHistoryInfo(@PathVariable("courseId")String courseId,
									   HttpServletRequest request) {
		JSONObject res = new JSONObject();
		String userId=(String) request.getAttribute(Constants.CURRENT_USER_ID);
		res=userCourseService.getDoTestHistoryInfo(courseId,userId);
		res.put("status",200);
		res.put("success",true);
		return res;
	}

	/**
	 * 我的课程-获取课程题库信息
	 */
	@RequestMapping(value="/v0.1/courseQuestInfo/{courseId}/{page}/{size}" , method = RequestMethod.GET, produces = { "application/json;charset=UTF-8" })
	@Authorization
	public Object getCourseQuestInfo(@PathVariable("courseId")String courseId,
								   @PathVariable("page")int page,
								   @PathVariable("size")int size,
								   HttpServletRequest request) {
		JSONObject res = new JSONObject();
		String userId=(String) request.getAttribute(Constants.CURRENT_USER_ID);
		res=userCourseService.getCourseQuestInfo(courseId,userId,page,size);
		res.put("status",200);
		res.put("success",true);
		return res;
	}

	/**
	 * 我的课程-做课后题保存退出
	 */
	@RequestMapping(value="/v0.1/doCourseQuest/save" , method = RequestMethod.POST, produces = { "application/json;charset=UTF-8" })
	@Authorization
	public Object doCourseQuestSave(@RequestBody DoCourseSaveReq doCourseSaveReq,
								  HttpServletRequest request) {
		JSONObject res = new JSONObject();
		String userId=(String) request.getAttribute(Constants.CURRENT_USER_ID);
		userCourseService.doCourseQuestSave(doCourseSaveReq,userId);
		res.put("status",200);
		res.put("success",true);
		return res;
	}

	/**
	 * 我的课程-做课后题交卷
	 */
	@RequestMapping(value="/v0.1/doCourseQuest/hand" , method = RequestMethod.POST, produces = { "application/json;charset=UTF-8" })
	@Authorization
	public Object doCourseQuestHand(@RequestBody DoCourseSaveReq doCourseSaveReq,
								  HttpServletRequest request) {
		JSONObject res = new JSONObject();
		String userId=(String) request.getAttribute(Constants.CURRENT_USER_ID);
		userCourseService.doCourseQuestHand(doCourseSaveReq,userId);
		res.put("status",200);
		res.put("success",true);
		return res;
	}

	/**
	 * 我的课程-交卷界面
	 */
	@RequestMapping(value="/v0.1/course/handPaper/result/{courseId}/{handPaperId}" , method = RequestMethod.GET, produces = { "application/json;charset=UTF-8" })
	@Authorization
	public Object getHandPaperResult(@PathVariable("courseId")String courseId,
									 @PathVariable("handPaperId")String handPaperId,
									 HttpServletRequest request) {
		JSONObject res = new JSONObject();
		String userId=(String) request.getAttribute(Constants.CURRENT_USER_ID);
		res=userCourseService.getHandPaperResult(courseId,handPaperId,userId);
		res.put("status",200);
		res.put("success",true);
		return res;
	}

	/**
	 * 我的课程-获取交卷详细答案结果信息
	 */
	@RequestMapping(value="/v0.1/course/handTestResultInfo/{courseId}/{handPaperId}/{page}/{size}" , method = RequestMethod.GET, produces = { "application/json;charset=UTF-8" })
	@Authorization
	public Object getHandTestResultInfo(@PathVariable("courseId")String courseId,
										@PathVariable("handPaperId")String handPaperId,
										@PathVariable("page")int page,
										@PathVariable("size")int size,
										HttpServletRequest request) {
		JSONObject res = new JSONObject();
		String userId=(String) request.getAttribute(Constants.CURRENT_USER_ID);
		res=userCourseService.getHandTestResultInfo(courseId,handPaperId,userId,page,size);
		res.put("status",200);
		res.put("success",true);
		return res;
	}
}