package com.mky.mkcompany.controller.domain;

import javax.persistence.Column;
import javax.persistence.Id;
import java.sql.Timestamp;

/**
 * 课程列表
 */
public class CourseInfoRes implements Comparable<CourseInfoRes> {
    private static final long serialVersionUID = 1L;

    private String   id;

    private String courseCategoryId;      //课程科目id

    private String courseCategoryName;      //课程科目名称

    private String courseSubjectId;      //课程科目id

    private String courseSubjectName;      //课程科目名称

    private String name;      //课程名称

    /**
     * 考试类型（护士执业、初级护师、主管护师、临床执业医师、临床助理医师、执业中药师、执业西药师、乡村全科执业助理医师）
     */
    private String examType;

    private String courseCover; //课程封面

    private String url; //课程视频地址

    private String courseTime; //课程时长   cover

    private String speaker; //主讲人

    private Double schedule; //听课进度

    private String homework; //课后练习(无、正在学习、未学习)

    private Timestamp createTime;       //创建时间

    private Timestamp updateTime;        //修改时间

    private String sortKey; //排序的字段 createTime,绑定createTime过来

    private int sortType; //1 升序  2  降序

    public CourseInfoRes(String sortKey, int sortType){
        this.sortKey=sortKey;
        this.sortType=sortType;
    }

    @Override
    public int compareTo(CourseInfoRes o) {
        double result=0;
        int res;
        switch (sortKey) {
            case "createTime":
                result = this.getCreateTime().compareTo(o.getCreateTime());
                break;
        }
        res=(int)(sortType==1?result:-result);
        return res;
    }

    public String getCourseCategoryId() {
        return courseCategoryId;
    }

    public void setCourseCategoryId(String courseCategoryId) {
        this.courseCategoryId = courseCategoryId;
    }

    public String getCourseCategoryName() {
        return courseCategoryName;
    }

    public void setCourseCategoryName(String courseCategoryName) {
        this.courseCategoryName = courseCategoryName;
    }

    public String getCourseSubjectName() {
        return courseSubjectName;
    }

    public void setCourseSubjectName(String courseSubjectName) {
        this.courseSubjectName = courseSubjectName;
    }

    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCourseSubjectId() {
        return courseSubjectId;
    }

    public void setCourseSubjectId(String courseSubjectId) {
        this.courseSubjectId = courseSubjectId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getExamType() {
        return examType;
    }

    public void setExamType(String examType) {
        this.examType = examType;
    }

    public String getCourseCover() {
        return courseCover;
    }

    public void setCourseCover(String courseCover) {
        this.courseCover = courseCover;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getCourseTime() {
        return courseTime;
    }

    public void setCourseTime(String courseTime) {
        this.courseTime = courseTime;
    }

    public String getSpeaker() {
        return speaker;
    }

    public void setSpeaker(String speaker) {
        this.speaker = speaker;
    }

    public Double getSchedule() {
        return schedule;
    }

    public void setSchedule(Double schedule) {
        this.schedule = schedule;
    }

    public String getHomework() {
        return homework;
    }

    public void setHomework(String homework) {
        this.homework = homework;
    }

    public Timestamp getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Timestamp createTime) {
        this.createTime = createTime;
    }

    public Timestamp getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Timestamp updateTime) {
        this.updateTime = updateTime;
    }

    public String getSortKey() {
        return sortKey;
    }

    public void setSortKey(String sortKey) {
        this.sortKey = sortKey;
    }

    public int getSortType() {
        return sortType;
    }

    public void setSortType(int sortType) {
        this.sortType = sortType;
    }
}
