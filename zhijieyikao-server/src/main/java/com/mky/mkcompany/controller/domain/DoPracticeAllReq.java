package com.mky.mkcompany.controller.domain;

import java.util.List;

/**
 * Created by Lenovo on 2018/9/20.
 */
public class DoPracticeAllReq {
    private List<DoPracticeReq> data;
    private String examType;
    private String subjectCategoryId;
    private String subjectChapterId;
    private Integer seq; //做题到那个题;

    public String getExamType() {
        return examType;
    }

    public void setExamType(String examType) {
        this.examType = examType;
    }

    public String getSubjectCategoryId() {
        return subjectCategoryId;
    }

    public void setSubjectCategoryId(String subjectCategoryId) {
        this.subjectCategoryId = subjectCategoryId;
    }

    public String getSubjectChapterId() {
        return subjectChapterId;
    }

    public void setSubjectChapterId(String subjectChapterId) {
        this.subjectChapterId = subjectChapterId;
    }

    public Integer getSeq() {
        return seq;
    }

    public void setSeq(Integer seq) {
        this.seq = seq;
    }

    public List<DoPracticeReq> getData() {
        return data;
    }

    public void setData(List<DoPracticeReq> data) {
        this.data = data;
    }
}
