package com.mky.mkcompany.controller.domain;

import java.util.List;

/**
 * Created by Lenovo on 2018/9/16.
 */
public class DoCourseSaveReq {
    private String courseId;
    private Integer minute;
    private Integer second;
    private Integer noDoQuestNum;
    private Long startTime;       //开始答卷时间
    private List<DoPracticeReq> data;


    public String getCourseId() {
        return courseId;
    }

    public void setCourseId(String courseId) {
        this.courseId = courseId;
    }

    public Integer getMinute() {
        return minute;
    }

    public void setMinute(Integer minute) {
        this.minute = minute;
    }

    public Integer getSecond() {
        return second;
    }

    public void setSecond(Integer second) {
        this.second = second;
    }

    public Integer getNoDoQuestNum() {
        return noDoQuestNum;
    }

    public void setNoDoQuestNum(Integer noDoQuestNum) {
        this.noDoQuestNum = noDoQuestNum;
    }

    public List<DoPracticeReq> getData() {
        return data;
    }

    public void setData(List<DoPracticeReq> data) {
        this.data = data;
    }

    public Long getStartTime() {
        return startTime;
    }

    public void setStartTime(Long startTime) {
        this.startTime = startTime;
    }
}
