package com.mky.mkcompany.controller;


import com.alibaba.fastjson.JSONObject;
import com.mky.mkcompany.authorization.annotation.Authorization;
import com.mky.mkcompany.controller.domain.DoPracticeAllReq;
import com.mky.mkcompany.controller.domain.DoPracticeReq;
import com.mky.mkcompany.dic.Constants;
import com.mky.mkcompany.service.DoExerciseService;
import com.mky.mkcompany.service.TestService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

/**
 * 做题
 */
@RestController
public class DoExerciseController {
	private final Logger LOGGER = LoggerFactory.getLogger(getClass());
	@Autowired
	private DoExerciseService doExerciseService;

	/**
	 *获取随机练习题库
     */
	@RequestMapping(value="/v0.1/random/practice/{examType}/{page}/{size}" , method = RequestMethod.GET, produces = { "application/json;charset=UTF-8" })
	@Authorization
	public Object getRandomPractice(@PathVariable("examType")String examType,
									@PathVariable("page")int page,
									@PathVariable("size")int size,
									HttpServletRequest request) {
		JSONObject res = new JSONObject();
		String userId=(String) request.getAttribute(Constants.CURRENT_USER_ID);
		res=doExerciseService.getRandomPractice(examType,userId,page,size);
		res.put("success",true);
		res.put("status",200);
		return res;
	}

	/**
	 *获取随机练习题-做过的题do 未做的题noDo 做对的题doTrue 做错的题doFalse 收藏的题collect
	 */
	@RequestMapping(value="/v0.1/random/practice/{examType}/{type}/{page}/{size}" , method = RequestMethod.GET, produces = { "application/json;charset=UTF-8" })
	@Authorization
	public Object getRandomPracticeByType(@PathVariable("examType")String examType,
										  @PathVariable("type")String type,
									@PathVariable("page")int page,
									@PathVariable("size")int size,
									HttpServletRequest request) {
		JSONObject res = new JSONObject();
		String userId=(String) request.getAttribute(Constants.CURRENT_USER_ID);
		res=doExerciseService.getRandomPracticeByType(examType,type,userId,page,size);
		res.put("success",true);
		res.put("status",200);
		return res;
	}

	/**
	 *练习题-保存进度
	*/
	@RequestMapping(value="/v0.1/save/doPractice" , method = RequestMethod.POST, produces = { "application/json;charset=UTF-8" })
	@Authorization
	public Object saveDoPractice(@RequestBody DoPracticeAllReq data,
								 HttpServletRequest request) {
		JSONObject res = new JSONObject();
		String userId=(String) request.getAttribute(Constants.CURRENT_USER_ID);
		doExerciseService.saveDoPractice(data,userId);
		res.put("success",true);
		res.put("status",200);
		return res;
	}

	/**
	 *收藏练习题
	 */
	@RequestMapping(value="/v0.1/collect/practice/{questId}" , method = RequestMethod.POST, produces = { "application/json;charset=UTF-8" })
	@Authorization
	public Object collectPractice(@PathVariable("questId")String questId,
								 HttpServletRequest request) {
		JSONObject res = new JSONObject();
		String userId=(String) request.getAttribute(Constants.CURRENT_USER_ID);
		doExerciseService.collectPractice(questId,userId);
		res.put("success",true);
		res.put("status",200);
		return res;
	}

	/**
	 *取消收藏
	 */
	@RequestMapping(value="/v0.1/cancel/collect/practice/{questId}" , method = RequestMethod.DELETE, produces = { "application/json;charset=UTF-8" })
	@Authorization
	public Object collectCancelPractice(@PathVariable("questId")String questId,
								  HttpServletRequest request) {
		JSONObject res = new JSONObject();
		String userId=(String) request.getAttribute(Constants.CURRENT_USER_ID);
		doExerciseService.collectCancelPractice(questId,userId);
		res.put("success",true);
		res.put("status",200);
		return res;
	}


	/**
	 *获取章节练习首页信息
	 */
	@RequestMapping(value="/v0.1/chapter/practice/homePage/{examType}" , method = RequestMethod.GET, produces = { "application/json;charset=UTF-8" })
	@Authorization
	public Object getChapterPracticeHomePage(@PathVariable("examType")String examType,
									 HttpServletRequest request) {
		JSONObject res = new JSONObject();
		String userId=(String) request.getAttribute(Constants.CURRENT_USER_ID);
		res=doExerciseService.getChapterPracticeHomePage(examType,userId);
		res.put("success",true);
		res.put("status",200);
		return res;
	}
	/**
	 *获取章节练习章节信息
	 */
	@RequestMapping(value="/v0.1/chapter/practice/chapInfo/{subjectCategoryId}/{examType}" , method = RequestMethod.GET, produces = { "application/json;charset=UTF-8" })
	@Authorization
	public Object getChapterPracticeChapInfo( @PathVariable("subjectCategoryId")String subjectCategoryId,
											  @PathVariable("examType")String examType,
											 HttpServletRequest request) {
		JSONObject res = new JSONObject();
		String userId=(String) request.getAttribute(Constants.CURRENT_USER_ID);
		res=doExerciseService.getChapterPracticeChapInfo(examType,userId,subjectCategoryId);
		res.put("success",true);
		res.put("status",200);
		return res;
	}


	/**
	 *获取章节练习题库
	 */
	@RequestMapping(value="/v0.1/chapter/practice/{examType}/{subjectCategoryId}/{subjectChapterId}/{page}/{size}" , method = RequestMethod.GET, produces = { "application/json;charset=UTF-8" })
	@Authorization
	public Object getChapterPractice(@PathVariable("examType")String examType,
									 @PathVariable("subjectCategoryId")String subjectCategoryId,
									 @PathVariable("subjectChapterId")String subjectChapterId,
									@PathVariable("page")int page,
									@PathVariable("size")int size,
									 HttpServletRequest request) {
		JSONObject res = new JSONObject();
		String userId=(String) request.getAttribute(Constants.CURRENT_USER_ID);
		res=doExerciseService.getChapterPractice(examType,userId,subjectCategoryId,subjectChapterId,page,size);
		res.put("success",true);
		res.put("status",200);
		return res;
	}

	/**
	 *获取章节练习题-做过的题do 未做的题noDo 做对的题doTrue 做错的题doFalse 收藏的题collect
	 */
	@RequestMapping(value="/v0.1/chapter/practice/{examType}/{subjectCategoryId}/{subjectChapterId}/{type}/{page}/{size}" , method = RequestMethod.GET, produces = { "application/json;charset=UTF-8" })
	@Authorization
	public Object getChapterPracticeByType(@PathVariable("examType")String examType,
										   @PathVariable("subjectCategoryId")String subjectCategoryId,
										   @PathVariable("subjectChapterId")String subjectChapterId,
										   @PathVariable("type")String type,
										  @PathVariable("page")int page,
										  @PathVariable("size")int size,
										  HttpServletRequest request) {
		JSONObject res = new JSONObject();
		String userId=(String) request.getAttribute(Constants.CURRENT_USER_ID);
		res=doExerciseService.getChapterPracticeByType(examType,type,subjectCategoryId,subjectChapterId,userId,page,size);
		res.put("success",true);
		res.put("status",200);
		return res;
	}
}