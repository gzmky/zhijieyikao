package com.mky.mkcompany.controller;


import com.alibaba.fastjson.JSONObject;
import com.mky.mkcompany.authorization.annotation.Authorization;
import com.mky.mkcompany.controller.domain.DoPracticeReq;
import com.mky.mkcompany.controller.domain.DoTestSaveReq;
import com.mky.mkcompany.dic.Constants;
import com.mky.mkcompany.service.TestService;
import com.mky.mkcompany.service.UserDoTestService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

/**
 * 我的试卷
 */
@RestController
public class UserDoTestController {
	private final Logger LOGGER = LoggerFactory.getLogger(getClass());
	@Autowired
	private UserDoTestService userDoTestService;

	/**
	 * 我的试卷-获取试卷信息
     */
	@RequestMapping(value="/v0.1/myTestInfo/{examType}/{page}/{size}" , method = RequestMethod.GET, produces = { "application/json;charset=UTF-8" })
	@Authorization
	public Object getMyTestInfo(@PathVariable("examType")String examType,
								@PathVariable("page")int page,
								@PathVariable("size")int size,
								HttpServletRequest request) {
		JSONObject res = new JSONObject();
		String userId=(String) request.getAttribute(Constants.CURRENT_USER_ID);
		res=userDoTestService.getMyTestInfo(examType,userId,page,size);
		res.put("status",200);
		res.put("success",true);
		return res;
	}

	/**
	 * 我的试卷-获取做试卷历史信息
	 */
	@RequestMapping(value="/v0.1/doTestHistoryInfo/{testId}" , method = RequestMethod.GET, produces = { "application/json;charset=UTF-8" })
	@Authorization
	public Object getDoTestHistoryInfo(@PathVariable("testId")String testId,
								HttpServletRequest request) {
		JSONObject res = new JSONObject();
		String userId=(String) request.getAttribute(Constants.CURRENT_USER_ID);
		res=userDoTestService.getDoTestHistoryInfo(testId,userId);
		res.put("status",200);
		res.put("success",true);
		return res;
	}

	/**
	 * 我的试卷-获取试卷题库信息
	 */
	@RequestMapping(value="/v0.1/testQuestInfo/{testId}/{page}/{size}" , method = RequestMethod.GET, produces = { "application/json;charset=UTF-8" })
	@Authorization
	public Object getTestQuestInfo(@PathVariable("testId")String testId,
									   @PathVariable("page")int page,
									   @PathVariable("size")int size,
									   HttpServletRequest request) {
		JSONObject res = new JSONObject();
		String userId=(String) request.getAttribute(Constants.CURRENT_USER_ID);
		res=userDoTestService.getTestQuestInfo(testId,userId,page,size);
		res.put("status",200);
		res.put("success",true);
		return res;
	}

	/**
	 * 我的试卷-做试卷保存退出
	 */
	@RequestMapping(value="/v0.1/doTestQuest/save" , method = RequestMethod.POST, produces = { "application/json;charset=UTF-8" })
	@Authorization
	public Object doTestQuestSave(@RequestBody DoTestSaveReq doTestSaveReq,
								   HttpServletRequest request) {
		JSONObject res = new JSONObject();
		String userId=(String) request.getAttribute(Constants.CURRENT_USER_ID);
		userDoTestService.doTestQuestSave(doTestSaveReq,userId);
		res.put("status",200);
		res.put("success",true);
		return res;
	}

	/**
	 * 我的试卷-做试卷交卷
	 */
	@RequestMapping(value="/v0.1/doTestQuest/hand" , method = RequestMethod.POST, produces = { "application/json;charset=UTF-8" })
	@Authorization
	public Object doTestQuestHand(@RequestBody DoTestSaveReq doTestSaveReq,
								  HttpServletRequest request) {
		JSONObject res = new JSONObject();
		String userId=(String) request.getAttribute(Constants.CURRENT_USER_ID);
		userDoTestService.doTestQuestHand(doTestSaveReq,userId);
		res.put("status",200);
		res.put("success",true);
		return res;
	}

	/**
	 * 我的试卷-交卷界面
	 */
	@RequestMapping(value="/v0.1/handPaper/result/{testId}/{handPaperId}" , method = RequestMethod.GET, produces = { "application/json;charset=UTF-8" })
	@Authorization
	public Object getHandPaperResult(@PathVariable("testId")String testId,
									@PathVariable("handPaperId")String handPaperId,
								  HttpServletRequest request) {
		JSONObject res = new JSONObject();
		String userId=(String) request.getAttribute(Constants.CURRENT_USER_ID);
		res=userDoTestService.getHandPaperResult(testId,handPaperId,userId);
		res.put("status",200);
		res.put("success",true);
		return res;
	}

	/**
	 * 我的试卷-获取交卷详细答案结果信息
	 */
	@RequestMapping(value="/v0.1/handTestResultInfo/{testId}/{handPaperId}/{page}/{size}" , method = RequestMethod.GET, produces = { "application/json;charset=UTF-8" })
	@Authorization
	public Object getHandTestResultInfo(@PathVariable("testId")String testId,
									@PathVariable("handPaperId")String handPaperId,
								   @PathVariable("page")int page,
								   @PathVariable("size")int size,
								   HttpServletRequest request) {
		JSONObject res = new JSONObject();
		String userId=(String) request.getAttribute(Constants.CURRENT_USER_ID);
		res=userDoTestService.getHandTestResultInfo(testId,handPaperId,userId,page,size);
		res.put("status",200);
		res.put("success",true);
		return res;
	}

	/**
	 * 我的试卷-我的错题列表数据
	 */
	@RequestMapping(value="/v0.1/myTest/myFalseQuestListInfo/{examType}/{subjectName}/{chapterName}/{page}/{size}" , method = RequestMethod.GET, produces = { "application/json;charset=UTF-8" })
	@Authorization
	public Object getMyFalseQuestListInfo(@PathVariable("examType")String examType,
										@PathVariable("subjectName")String subjectName,
										  @PathVariable("chapterName")String chapterName,
										@PathVariable("page")int page,
										@PathVariable("size")int size,
										HttpServletRequest request) {
		JSONObject res = new JSONObject();
		String userId=(String) request.getAttribute(Constants.CURRENT_USER_ID);
		res=userDoTestService.getMyFalseQuestListInfo(examType,subjectName,chapterName,userId,page,size);
		res.put("status",200);
		res.put("success",true);
		return res;
	}

	/**
	 * 我的试卷-我的错题详细信息
	 */
	@RequestMapping(value="/v0.1/myTest/myFalseQuestInfo/{questId}" , method = RequestMethod.GET, produces = { "application/json;charset=UTF-8" })
	@Authorization
	public Object getMyFalseQuestInfo(@PathVariable("questId")String questId,
										  HttpServletRequest request) {
		JSONObject res = new JSONObject();
		String userId=(String) request.getAttribute(Constants.CURRENT_USER_ID);
		res=userDoTestService.getMyFalseQuestInfo(questId,userId);
		res.put("status",200);
		res.put("success",true);
		return res;
	}

}