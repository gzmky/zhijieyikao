package com.mky.mkcompany.controller.domain;

/**
 * Created by Lenovo on 2018/9/15.
 */
public class DoPracticeReq {
    private String questId;
    private String result;
    private int answerResult;

    public String getQuestId() {
        return questId;
    }

    public void setQuestId(String questId) {
        this.questId = questId;
    }

    public int getAnswerResult() {
        return answerResult;
    }

    public void setAnswerResult(int answerResult) {
        this.answerResult = answerResult;
    }

    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result;
    }
}
