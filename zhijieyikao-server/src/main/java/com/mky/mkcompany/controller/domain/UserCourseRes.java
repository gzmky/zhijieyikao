package com.mky.mkcompany.controller.domain;

import java.sql.Timestamp;

/**
 * 用户课程
 */
public class UserCourseRes implements Comparable<UserCourseRes>{
    private static final long serialVersionUID = 1L;

    private String   id;
    /**
     * 考试类型（护士执业、初级护师、主管护师、临床执业医师、临床助理医师、执业中药师、执业西药师、乡村全科执业助理医师）
     */
    private String examType;

    private String  username;  //用户名

    private String  userId;

    private String mobile; //手机

    private Timestamp createTime;  //创建时间

    private String sortKey; //排序的字段 createTime,绑定createTime过来

    private int sortType; //1 升序  2  降序

    public UserCourseRes(String sortKey, int sortType){
        this.sortKey=sortKey;
        this.sortType=sortType;
    }

    @Override
    public int compareTo(UserCourseRes o) {
        double result=0;
        int res;
        switch (sortKey) {
            case "createTime":
                result = this.getCreateTime().compareTo(o.getCreateTime());
                break;
        }
        res=(int)(sortType==1?result:-result);
        return res;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getSortKey() {
        return sortKey;
    }

    public void setSortKey(String sortKey) {
        this.sortKey = sortKey;
    }

    public int getSortType() {
        return sortType;
    }

    public void setSortType(int sortType) {
        this.sortType = sortType;
    }

    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getExamType() {
        return examType;
    }

    public void setExamType(String examType) {
        this.examType = examType;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public Timestamp getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Timestamp createTime) {
        this.createTime = createTime;
    }
}
