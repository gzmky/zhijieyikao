package com.mky.mkcompany.controller.domain;

import java.io.Serializable;

/**
 *下拉需要的id和name
 */
public class QuestXiaLaRes implements Serializable {
    private static final long serialVersionUID = 1L;

    private String id;     //id

    private String name;     //名称

    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
