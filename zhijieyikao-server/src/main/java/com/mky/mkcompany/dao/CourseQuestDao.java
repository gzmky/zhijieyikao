package com.mky.mkcompany.dao;


import com.mky.mkcompany.model.CourseQuest;
import com.mky.mkcompany.model.TestQuest;
import com.mky.mkcompany.model.UserTest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Transactional
public interface CourseQuestDao extends JpaRepository<CourseQuest, String> {

    CourseQuest findByTitleAndExamType(String title, String examType);

    CourseQuest findById(String id);

    List<CourseQuest> findByChapterIdIn(List<String> chapterIds);

    List<CourseQuest> findByChapterIdInAndExamType(List<String> chapterIds,String examType);

    @Query("from CourseQuest q where q.courseId=:courseId ")
    Page<CourseQuest> findObjsByCourseId(@Param("courseId") String courseId, Pageable pageable);


    @Transactional
    @Modifying
    @Query("delete from CourseQuest where courseId = ?1 ")
    void deleteByCourseId(String courseId);

    @Transactional
    @Modifying
    @Query("delete from CourseQuest where chapterId = ?1 ")
    void deleteByChapterId(String chapterId);

    @Transactional
    @Modifying
    @Query("delete from CourseQuest where chapterId in ?1 ")
    void deleteByChapterIds(List<String> chapterIds);

    @Transactional
    @Modifying
    @Query("delete from CourseQuest where id = ?1 ")
    void deleteById(String id);

}