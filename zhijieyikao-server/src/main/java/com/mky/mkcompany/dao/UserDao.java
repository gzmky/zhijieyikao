package com.mky.mkcompany.dao;

import com.mky.mkcompany.model.User;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Map;
@Transactional
public interface UserDao extends JpaRepository<User, String> {
     User findByUsernameAndDelStatus(String Username,Integer delStatus);

     @Query("from User u where u.username=:username")
     User findUserByUsername(@Param("username") String username);

     Page<User> findByMobileLike(String mobile,Pageable pageable);

     List<User> findByMobileLike(String mobile);

     User findByUsername(String username);

     List<User> findByUsernameLike(String username);

     User findById(String id);

     User findByIdAndPassword(String id,String password);

     List<User> findByIdIn(List<String> ids);

     @Transactional
     @Modifying
     @Query("delete from User where id = ?1 ")
     void deleteById(String id);



}