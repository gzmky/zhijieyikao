package com.mky.mkcompany.dao;

import com.mky.mkcompany.model.*;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Transactional
public interface CourseSubjectDao extends JpaRepository<CourseSubject, String> {
    CourseSubject findByNameAndCourseCategoryId(String name, String courseCategoryId);

    List<CourseSubject> findByCourseCategoryIdIn(List<String> courseCategoryIds);

    List<CourseSubject> findByCourseCategoryId(String courseCategoryId);

    CourseSubject findById(String id);

    List<CourseSubject> findByIdIn(List<String> ids);

    @Transactional
    @Modifying
    @Query("delete from CourseSubject where courseCategoryId = ?1 ")
    void deleteByCourseCategoryId(String courseCategoryId);

    @Transactional
    @Modifying
    @Query("delete from CourseSubject where id = ?1 ")
    void deleteById(String id);


}