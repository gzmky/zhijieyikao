package com.mky.mkcompany.dao;

import com.mky.mkcompany.model.SubjectCategory;
import com.mky.mkcompany.model.SubjectChapter;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Transactional
public interface SubjectChapterDao extends JpaRepository<SubjectChapter, String> {
    @Query("from SubjectChapter s where s.subjectId=:subjectId order by createTime")
    List<SubjectChapter> findSubjectChapter(@Param("subjectId") String subjectId);
    Page<SubjectChapter> findAll(Pageable pageable);

    Page<SubjectChapter> findByName(String name,Pageable pageable);

    SubjectChapter findByNameAndSubjectId(String name,String SubjectId);

    SubjectChapter findById(String id);

    List<SubjectChapter> findBySubjectIdIn(List<String> subjectIds);

    List<SubjectChapter> findBySubjectId(String subjectId);

    SubjectChapter findBySubjectIdAndName(String subjectId,String name);

    List<SubjectChapter> findByNameLike(String name);

    @Transactional
    @Modifying
    @Query("delete from SubjectChapter where subjectId = ?1 ")
    void deleteBySubjectId(String subjectId);

    @Transactional
    @Modifying
    @Query("delete from SubjectChapter where id = ?1 ")
    void deleteById(String id);

}