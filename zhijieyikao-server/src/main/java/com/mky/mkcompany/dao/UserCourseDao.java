package com.mky.mkcompany.dao;

import com.mky.mkcompany.model.UserCourse;
import com.mky.mkcompany.model.UserTest;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Transactional
public interface UserCourseDao extends JpaRepository<UserCourse, String> {
    UserCourse findByExamTypeAndUserId(String examType,String userId);

    List<UserCourse> findByUserIdIn(List<String> userIds);

    @Transactional
    @Modifying
    @Query("delete from UserCourse where userId = ?1 ")
    void deleteByUserId(String userId);
}