package com.mky.mkcompany.dao;

import com.mky.mkcompany.model.UserExamType;
import com.mky.mkcompany.model.UserTest;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.transaction.annotation.Transactional;

@Transactional
public interface UserExamTypeDao extends JpaRepository<UserExamType, String> {
    UserExamType findById(String id);

    UserExamType findByUserId(String userId);
}