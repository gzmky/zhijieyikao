package com.mky.mkcompany.dao;

import com.mky.mkcompany.model.*;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Transactional
public interface CourseCategoryDao extends JpaRepository<CourseCategory, String> {
    CourseCategory findById(String id);

    CourseCategory findByNameAndExamType(String name,String examType);

    List<CourseCategory> findByName(String name);

    Page<CourseCategory> findByNameLikeAndExamType(String name, String examType, Pageable pageable);

    Page<CourseCategory> findByExamType(String examType,Pageable pageable);

    Page<CourseCategory> findByNameLike(String name,Pageable pageable);

    List<CourseCategory> findByExamType(String examType);

    CourseCategory findByExamTypeAndId(String examType,String id);

    List<CourseCategory> findByIdIn(List<String> ids);

    List<CourseCategory> findByNameLike(String name);

    @Transactional
    @Modifying
    @Query("delete from CourseCategory where id = ?1 ")
    void deleteById(String id);


}