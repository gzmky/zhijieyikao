package com.mky.mkcompany.dao;

import com.mky.mkcompany.model.*;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Transactional
public interface PracticQuestDao extends JpaRepository<PracticQuest, String> {

    @Query("from PracticQuest p where p.examType=:examType")
    Page<PracticQuest> findPracticQuestPage(@Param("examType") String examType, Pageable pageable);

    @Query("select count(*) from PracticQuest p,SubjectChapter sc where p.examType=:examType and sc.subjectId=:subjectId and p.chapterId=sc.id")
    long findCountBySubjectId(@Param("examType") String examType,
                                                       @Param("subjectId") String subjectId);

    @Query("select p from PracticQuest p,SubjectChapter sc where p.examType=:examType and sc.subjectId=:subjectId and p.chapterId=sc.id")
    Page<PracticQuest> findPracticQuestPageBySubjectId(@Param("examType") String examType,
                                                       @Param("subjectId") String subjectId,Pageable pageable);

    @Query("select count(*) from PracticQuest p where p.examType=:examType and p.chapterId=:chapterId")
    long findCountByChapterId(@Param("examType") String examType,
                                                       @Param("chapterId") String chapterId);

    @Query("from PracticQuest p where p.examType=:examType and p.chapterId=:chapterId")
    Page<PracticQuest> findPracticQuestPageByChapterId(@Param("examType") String examType,
                                                       @Param("chapterId") String chapterId,Pageable pageable);

    PracticQuest findByTitleAndExamType(String title, String examType);

    PracticQuest findById(String id);

    List<PracticQuest> findByChapterIdIn(List<String> chapterIds);

    List<PracticQuest> findByChapterId(String chapterId);

    List<PracticQuest> findByChapterIdInAndExamType(List<String> chapterIds,String examType);

    @Query("select p from PracticQuest p,SubjectChapter sc where p.examType=:examType and sc.subjectId=:subjectId and p.chapterId=sc.id and p.id not in (:ids)")
    List<PracticQuest> findByIdNotIn(@Param("examType") String examType,
                                     @Param("subjectId") String subjectId,
                                     @Param("ids") List<String> ids);

    @Query("select p from PracticQuest p where p.examType=:examType and p.chapterId=:chapterId and p.id not in (:ids)")
    List<PracticQuest> findByIdNotInAndChapterId(@Param("examType") String examType,
                                     @Param("chapterId") String chapterId,
                                     @Param("ids") List<String> ids);

    List<PracticQuest> findByIdNotIn(List<String> ids);

    @Transactional
    @Modifying
    @Query("delete from PracticQuest where chapterId = ?1 ")
    void deleteByChapterId(String chapterId);

    @Transactional
    @Modifying
    @Query("delete from PracticQuest where chapterId in ?1 ")
    void deleteByChapterIds(List<String> chapterIds);

    @Transactional
    @Modifying
    @Query("delete from PracticQuest where id = ?1 ")
    void deleteById(String id);
}