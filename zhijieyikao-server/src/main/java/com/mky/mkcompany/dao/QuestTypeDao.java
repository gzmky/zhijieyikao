package com.mky.mkcompany.dao;

import com.mky.mkcompany.model.QuestType;
import com.mky.mkcompany.model.SubjectCategory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.transaction.annotation.Transactional;

@Transactional
public interface QuestTypeDao extends JpaRepository<QuestType, String> {
    Page<QuestType> findAll(Pageable pageable);

    Page<QuestType> findByName(String name,Pageable pageable);

    QuestType findByName(String name);

    QuestType findById(String id);
}