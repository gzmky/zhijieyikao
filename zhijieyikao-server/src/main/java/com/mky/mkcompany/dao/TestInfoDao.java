package com.mky.mkcompany.dao;

import com.mky.mkcompany.model.PracticQuest;
import com.mky.mkcompany.model.SubjectCategory;
import com.mky.mkcompany.model.SubjectChapter;
import com.mky.mkcompany.model.TestInfo;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

@Transactional
public interface TestInfoDao extends JpaRepository<TestInfo, String> {
    @Query("from TestInfo t where t.examType=:examType")
    Page<TestInfo> findTestInfoPage(@Param("examType") String examType, Pageable pageable);
    TestInfo findByNameAndExamType(String name, String examType);

    TestInfo findById(String id);

    Page<TestInfo> findByNameLikeAndExamType(String name,String examType,Pageable pageable);

    Page<TestInfo> findByExamType(String examType,Pageable pageable);

    Page<TestInfo> findByNameLike(String name,Pageable pageable);

    @Transactional
    @Modifying
    @Query("delete from TestInfo where id = ?1 ")
    void deleteById(String id);

}