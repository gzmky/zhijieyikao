package com.mky.mkcompany.dao;

import com.mky.mkcompany.model.UserDoCourseQuest;
import com.mky.mkcompany.model.UserDoTestQuest;
import com.mky.mkcompany.model.UserTest;
import com.mky.mkcompany.model.view.UserFalseQuest;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Transactional
public interface UserDoCourseQuestDao extends JpaRepository<UserDoCourseQuest, String> {
    List<UserDoCourseQuest> findByUserIdAndCourseIdAndType(String userId, String courseId, Integer type);

    @Query("select new com.mky.mkcompany.model.view.UserFalseQuest(q.id,sc.name,c.name,q.testCentre) from UserDoCourseQuest uq,CourseQuest q," +
            "CourseSubject c,CourseCategory sc where uq.userId=:userId" +
            " and uq.handPaperId=:handPaperId and uq.answerResult=2 and uq.type=2 and uq.questId=q.id " +
            "and q.chapterId=c.id and c.courseCategoryId=sc.id")
    List<UserFalseQuest> findFalseQuest(@Param("userId") String userId,
                                        @Param("handPaperId") String handPaperId);

    List<UserDoCourseQuest> findByUserIdAndHandPaperIdAndType(String userId,String handPaperId,Integer type);

    @Transactional
    @Modifying
    @Query("delete from UserDoCourseQuest where questId = ?1 ")
    void deleteByQuestId(String questId);

    @Transactional
    @Modifying
    @Query("delete from UserDoCourseQuest where questId in (:questIds) ")
    void deleteByQuestIds(@Param("questIds")List<String> questIds);

    @Transactional
    @Modifying
    @Query("delete from UserDoCourseQuest where courseId = ?1 ")
    void deleteByCourseId(String courseId);


    @Transactional
    @Modifying
    @Query("delete from UserDoCourseQuest where courseId in ?1 ")
    void deleteByCourseIds(List<String> courseIds);

    @Transactional
    @Modifying
    @Query("delete from UserDoCourseQuest where userId = ?1 ")
    void deleteByUserId(String userId);
}