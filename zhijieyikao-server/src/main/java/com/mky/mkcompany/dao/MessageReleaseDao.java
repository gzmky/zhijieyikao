package com.mky.mkcompany.dao;

import com.mky.mkcompany.model.MessageRelease;
import com.mky.mkcompany.model.PracticQuest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Transactional
public interface MessageReleaseDao extends JpaRepository<MessageRelease, String> {

    MessageRelease findById(String id);

    List<MessageRelease> findByIdIn(List<String> ids);

    Page<MessageRelease> findByType(Integer type,Pageable pageable);

    List<MessageRelease> findByType(Integer type);
}