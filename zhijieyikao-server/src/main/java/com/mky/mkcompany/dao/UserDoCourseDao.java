package com.mky.mkcompany.dao;

import com.mky.mkcompany.model.UserDoCourse;
import com.mky.mkcompany.model.UserDoTest;
import com.mky.mkcompany.model.UserTest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Transactional
public interface UserDoCourseDao extends JpaRepository<UserDoCourse, String> {
    UserDoCourse findByUserIdAndHandPaperId(String userId,String handPaperId);

    @Query("from UserDoCourse t where t.courseId=:courseId and t.userId=:userId")
    Page<UserDoCourse> findObjsByCourseIdAndUserId(@Param("courseId") String courseId,
                                               @Param("userId") String userId, Pageable pageable);

    @Transactional
    @Modifying
    @Query("delete from UserDoCourse where courseId = ?1 ")
    void deleteByCourseId(String courseId);

    @Transactional
    @Modifying
    @Query("delete from UserDoCourse where courseId in (:courseIds) ")
    void deleteByCourseIds(@Param("courseIds")List<String> courseIds);

    @Transactional
    @Modifying
    @Query("delete from UserDoCourse where userId = ?1 ")
    void deleteByUserId(String userId);
}