package com.mky.mkcompany.dao;

import com.mky.mkcompany.model.UserDoPracticQuest;
import com.mky.mkcompany.model.UserTest;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.transaction.annotation.Transactional;

@Transactional
public interface UserTestDao extends JpaRepository<UserTest, String> {
    UserTest findByExamTypeAndUserId(String examType,String userId);

    @Transactional
    @Modifying
    @Query("delete from UserTest where userId = ?1 ")
    void deleteByUserId(String userId);
}