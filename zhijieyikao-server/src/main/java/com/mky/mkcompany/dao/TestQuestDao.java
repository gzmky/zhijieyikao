package com.mky.mkcompany.dao;

import com.mky.mkcompany.model.PracticQuest;
import com.mky.mkcompany.model.TestQuest;
import com.mky.mkcompany.model.UserTest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Transactional
public interface TestQuestDao extends JpaRepository<TestQuest, String> {
    @Query("select count(*) from TestQuest q where q.testId=:testId ")
    long findCountByTestId(@Param("testId") String testId);

    @Query("from TestQuest q where q.testId=:testId ")
    Page<TestQuest> findObjsByTestId(@Param("testId") String testId, Pageable pageable);

    TestQuest findByTitleAndExamType(String title,String examType);

    TestQuest findById(String id);

    List<TestQuest> findByChapterIdIn(List<String> chapterIds);

    List<TestQuest> findByChapterIdInAndExamType(List<String> chapterIds,String examType);

    @Transactional
    @Modifying
    @Query("delete from TestQuest where testId = ?1 ")
    void deleteByTestId(String testId);


    @Transactional
    @Modifying
    @Query("delete from TestQuest where chapterId = ?1 ")
    void deleteByChapterId(String chapterId);

    @Transactional
    @Modifying
    @Query("delete from TestQuest where chapterId in ?1 ")
    void deleteByChapterIds(List<String> chapterIds);

    @Transactional
    @Modifying
    @Query("delete from TestQuest where id = ?1 ")
    void deleteById(String id);



}