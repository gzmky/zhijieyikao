package com.mky.mkcompany.dao;

import com.mky.mkcompany.model.PracticQuest;
import com.mky.mkcompany.model.UserCollectPracticQuest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Transactional
public interface UserCollectPracticQuestDao extends JpaRepository<UserCollectPracticQuest, String> {
    @Query("select count(*) from UserCollectPracticQuest ucp,PracticQuest p where p.examType=:examType and p.id=ucp.questId and ucp.userId=:userId")
    long findCountCollQuestByUserId(@Param("examType") String examType,
                                    @Param("userId") String userId);

    @Query("select p from UserCollectPracticQuest ucp,PracticQuest p where p.examType=:examType and p.id=ucp.questId and ucp.userId=:userId")
    Page<PracticQuest> findCollQuestByUserId(@Param("examType") String examType,
                                             @Param("userId") String userId,
                                             Pageable pageable);

    @Query("select count(*) from UserCollectPracticQuest ucp,PracticQuest p,SubjectChapter sc where " +
            "p.examType=:examType and p.id=ucp.questId and ucp.userId=:userId and sc.subjectId=:subjectId and p.chapterId=sc.id")
    long findCountCollQuestByUserIdSubId(@Param("examType") String examType,
                                        @Param("userId") String userId,
                                         @Param("subjectId") String subjectId);

    @Query("select p from UserCollectPracticQuest ucp,PracticQuest p,SubjectChapter sc where " +
            "p.examType=:examType and p.id=ucp.questId and ucp.userId=:userId and sc.subjectId=:subjectId and p.chapterId=sc.id")
    Page<PracticQuest> findCollQuestByUserIdSubId(@Param("examType") String examType,
                                         @Param("userId") String userId,
                                         @Param("subjectId") String subjectId,
                                                  Pageable pageable);

    @Query("select count(*) from UserCollectPracticQuest ucp,PracticQuest p where " +
            "p.examType=:examType and p.id=ucp.questId and ucp.userId=:userId  and p.chapterId=:chapterId")
    long findCountCollQuestByUserIdChapId(@Param("examType") String examType,
                                         @Param("userId") String userId,
                                         @Param("chapterId") String chapterId);

    @Query("select p from UserCollectPracticQuest ucp,PracticQuest p where " +
            "p.examType=:examType and p.id=ucp.questId and ucp.userId=:userId  and p.chapterId=:chapterId")
    Page<PracticQuest> findCollQuestByUserIdChapId(@Param("examType") String examType,
                                          @Param("userId") String userId,
                                          @Param("chapterId") String chapterId,
                                                   Pageable pageable);

    UserCollectPracticQuest findByUserIdAndQuestId(String userId,String questId);

    @Query("select ucp from UserCollectPracticQuest ucp,PracticQuest p where p.examType=:examType and p.id=ucp.questId and ucp.userId=:userId")
    List<UserCollectPracticQuest> findCollQuestsByUserId(@Param("examType") String examType,
                                                        @Param("userId") String userId);

    @Transactional
    @Modifying
    @Query("delete from UserCollectPracticQuest where questId = ?1 ")
    void deleteByQuestId(String questId);

    @Transactional
    @Modifying
    @Query("delete from UserCollectPracticQuest where questId in (:questIds) ")
    void deleteByQuestIds(@Param("questIds")List<String> questIds);

    @Transactional
    @Modifying
    @Query("delete from UserCollectPracticQuest where userId = ?1 ")
    void deleteByUserId(String userId);
}