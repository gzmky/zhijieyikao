package com.mky.mkcompany.dao;


import com.mky.mkcompany.model.AdminUser;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.transaction.annotation.Transactional;

public interface AdminUserDao extends JpaRepository<AdminUser, String> {
     AdminUser findByUsername(String username);

     @Transactional
     @Modifying
     @Query("delete from AdminUser where id = ?1 ")
     void deleteById(String id);

}