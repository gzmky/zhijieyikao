package com.mky.mkcompany.dao;

import com.mky.mkcompany.model.CourseInfo;
import com.mky.mkcompany.model.SubjectCategory;
import com.mky.mkcompany.model.UserTest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Transactional
public interface CourseInfoDao extends JpaRepository<CourseInfo, String> {
    CourseInfo findByNameAndExamType(String name, String examType);

    List<CourseInfo> findByCourseSubjectId(String courseSubjectId);

    @Query("select c from CourseInfo c,CourseSubject cs where c.courseSubjectId=cs.id and cs.courseCategoryId=:courseCategoryId ")
    Page<CourseInfo> findObjsByCategoryId(@Param("courseCategoryId")String courseCategoryId, Pageable pageable);

    Page<CourseInfo> findByExamType(String examType, Pageable pageable);

    Page<CourseInfo> findByCourseSubjectId(String courseSubjectId, Pageable pageable);

    List<CourseInfo> findByCourseSubjectIdIn(List<String> courseSubjectIds);

    @Transactional
    @Modifying
    @Query("delete from CourseInfo where courseSubjectId = ?1 ")
    void deleteByCourseSubjectId(String courseSubjectId);


    @Transactional
    @Modifying
    @Query("delete from CourseInfo where courseSubjectId in ?1 ")
    void deleteByCourseSubjectIds(List<String> courseSubjectIds);

    @Transactional
    @Modifying
    @Query("delete from CourseInfo where id = ?1 ")
    void deleteById(String id);

    CourseInfo findById(String id);


}