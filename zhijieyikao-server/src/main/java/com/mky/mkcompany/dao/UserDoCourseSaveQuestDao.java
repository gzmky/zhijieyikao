package com.mky.mkcompany.dao;

import com.mky.mkcompany.model.UserDoCourseSaveQuest;
import com.mky.mkcompany.model.UserDoTestSaveQuest;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Transactional
public interface UserDoCourseSaveQuestDao extends JpaRepository<UserDoCourseSaveQuest, String> {

    UserDoCourseSaveQuest findByUserIdAndCourseId(String userId,String courseId);

    @Transactional
    @Modifying
    @Query("delete from UserDoCourseSaveQuest where courseId = ?1 ")
    void deleteByCourseId(String courseId);

    @Transactional
    @Modifying
    @Query("delete from UserDoCourseSaveQuest where courseId in ?1 ")
    void deleteByCourseIds(List<String> courseIds);

    @Transactional
    @Modifying
    @Query("delete from UserDoCourseSaveQuest where userId = ?1 ")
    void deleteByUserId(String userId);

}