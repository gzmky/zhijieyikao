package com.mky.mkcompany.dao;

import com.mky.mkcompany.model.UserDoTest;
import com.mky.mkcompany.model.UserTest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Transactional
public interface UserDoTestDao extends JpaRepository<UserDoTest, String> {
    @Query("select count(*) from UserDoTest t where t.testId=:testId and t.userId=:userId")
    long findCountByTestIdAndUserId(@Param("testId") String testId,
                           @Param("userId") String userId);

    @Query("from UserDoTest t where t.testId=:testId and t.userId=:userId")
    Page<UserDoTest> findObjsByTestIdAndUserId(@Param("testId") String testId,
                                               @Param("userId") String userId, Pageable pageable);

    UserDoTest findByUserIdAndHandPaperId(String userId,String handPaperId);

    @Transactional
    @Modifying
    @Query("delete from UserDoTest where userId = ?1 ")
    void deleteByUserId(String userId);
}