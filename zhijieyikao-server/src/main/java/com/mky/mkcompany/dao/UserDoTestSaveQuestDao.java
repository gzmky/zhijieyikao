package com.mky.mkcompany.dao;

import com.mky.mkcompany.model.TestInfo;
import com.mky.mkcompany.model.UserDoTestSaveQuest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

@Transactional
public interface UserDoTestSaveQuestDao extends JpaRepository<UserDoTestSaveQuest, String> {

    UserDoTestSaveQuest findByUserIdAndTestId(String userId,String testId);

    @Transactional
    @Modifying
    @Query("delete from UserDoTestSaveQuest where testId = ?1 ")
    void deleteByTestId(String testId);

    @Transactional
    @Modifying
    @Query("delete from UserDoTestSaveQuest where userId = ?1 ")
    void deleteByUserId(String userId);

}