package com.mky.mkcompany.dao;

import com.mky.mkcompany.model.QuestType;
import com.mky.mkcompany.model.SubjectCategory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Transactional
public interface SubjectCategoryDao extends JpaRepository<SubjectCategory, String> {
    @Query("from SubjectCategory s where s.examType=:examType order by createTime")
    List<SubjectCategory> findSubjectCategory(@Param("examType") String examType);

    Page<SubjectCategory> findAll(Pageable pageable);

    Page<SubjectCategory> findByName(String name,Pageable pageable);

    List<SubjectCategory> findByName(String name);

    Page<SubjectCategory> findByNameLike(String name,Pageable pageable);

    Page<SubjectCategory> findByExamType(String examType,Pageable pageable);

    List<SubjectCategory> findByExamType(String examType);

    Page<SubjectCategory> findByNameLikeAndExamType(String name,String examType,Pageable pageable);

    SubjectCategory findByNameAndExamType(String name,String examType);

    SubjectCategory findByIdAndExamType(String id,String examType);

    List<SubjectCategory> findByIdIn(List<String> ids);

    SubjectCategory findById(String id);

    @Query("from SubjectCategory where examType=:examType group by name")
    List<SubjectCategory> findSubjectCategoryGroupByExamType(@Param("examType") String examType);

    List<SubjectCategory> findByNameLike(String name);

    @Transactional
    @Modifying
    @Query("delete from SubjectCategory where id = ?1 ")
    void deleteById(String id);


}