package com.mky.mkcompany.dao;

import com.mky.mkcompany.model.CourseCategory;
import com.mky.mkcompany.model.UserQuestSeq;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Transactional
public interface UserQuestSeqDao extends JpaRepository<UserQuestSeq, String> {

    UserQuestSeq findByExamTypeAndUserIdAndSubjectCategoryIdAndSubjectChapterId(String examType,
                                                                                String userId,String subjectCategoryId,String subjectChapterId);
}