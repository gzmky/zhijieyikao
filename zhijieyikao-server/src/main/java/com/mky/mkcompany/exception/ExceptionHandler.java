package com.mky.mkcompany.exception;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Component;
import org.springframework.web.method.HandlerMethod;
import org.springframework.web.servlet.HandlerExceptionResolver;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.view.json.MappingJackson2JsonView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.HashMap;
import java.util.Map;

/**
 * 异常处理
 * 
 * @author herim
 *
 */
@Component
public class ExceptionHandler implements HandlerExceptionResolver {

	public static final Log log = LogFactory.getLog(ExceptionHandler.class);

	private String view = "redirect:/client/404.html";

	@Override
	public ModelAndView resolveException(HttpServletRequest request, HttpServletResponse response, Object object,
										 Exception ex) {
		HandlerMethod method = null;
		// ResourceHttpRequestHandler handler = null;
		if (ex instanceof InvalidException) {
			ModelAndView mv = new ModelAndView();
			mv.setViewName("redirect:/client/invalid.html");
			return mv;
		}
		if (ex instanceof WafException) {
			WafException waf = (WafException) ex;
			ErrorMessage errorMessage = waf.getResponseEntity().getBody();
			if (errorMessage != null) {
				String message = "";
				ModelAndView mv = new ModelAndView();
				MappingJackson2JsonView mjjv = new MappingJackson2JsonView();
				Map<String, Object> map = new HashMap<String, Object>();
				map.put("exceptionDesc", errorMessage.getCode());
				map.put("message", errorMessage.getMessage());
				map.put("success", false);
				map.put("status", waf.getResponseEntity().getStatusCode().value());
				mjjv.setAttributesMap(map);
				mv.setView(mjjv);
				// response.setStatus(waf.getResponseEntity().getStatusCode().value());
				return mv;
			}
		}
//		if (ex instanceof UnauthenticatedException) {
//			return new ModelAndView("session", null);
//		}
//		if (object instanceof HandlerMethod) {
//			method = (HandlerMethod) object;
//			log.info(DateUtils.getYYYY_MM_dd(new Date()) + " Exception occur:[" + method.getBean().getClass().getName()
//					+ "@" + method.getMethod().getName() + "] detail info below:" + ex.getMessage());
//			ResponseBody rb = method.getMethodAnnotation(ResponseBody.class);
//			Object bean = method.getBean();
//			RestController rc = bean.getClass().getAnnotation(RestController.class);
//			if (rb == null && rc == null) {// 非Json
//				Map<String, Object> model = new HashMap<String, Object>();
//				model.put("exception", this.convertMessage(ex));
//				return new ModelAndView(view, model);
//			} else {
//				ModelAndView mv = new ModelAndView();
//				MappingJackson2JsonView mjjv = new MappingJackson2JsonView();
//				Map<String, Object> map = new HashMap<String, Object>();
//				map.put("message", convertMessage(ex));
//				map.put("status", 500);
//				response.setStatus(500);
//				mjjv.setAttributesMap(map);
//				mv.setView(mjjv);
//				return mv;
//
//			}
//		} else if (object instanceof ResourceHttpRequestHandler) {
//			// log.info(DateUtil.getNow()+" detail info
//			// below:"+exception.getMessage());
//			return null;
//		}
		log.error(ex.getMessage(), ex);
		return new ModelAndView(view);

	}

	private String convertMessage(Throwable e) {
		e.printStackTrace();
		String errorMessage = e.getMessage();
//		if (e instanceof UnauthorizedException) {
//			errorMessage = "权限不足！";
//		} else if (e instanceof DataIntegrityViolationException) {
//			errorMessage = "存在引用,拒绝执行！";
//		} else {
//			errorMessage = "操作失败！";
//		}

		return errorMessage;
	}

}
