package com.mky.mkcompany.authorization.resolvers;

import com.mky.mkcompany.dao.UserDao;
import com.mky.mkcompany.authorization.annotation.CurrentUser;
import com.mky.mkcompany.dic.SecurityDic;
import com.mky.mkcompany.model.User;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.MethodParameter;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.support.WebDataBinderFactory;
import org.springframework.web.context.request.NativeWebRequest;
import org.springframework.web.context.request.RequestAttributes;
import org.springframework.web.method.support.HandlerMethodArgumentResolver;
import org.springframework.web.method.support.ModelAndViewContainer;
import org.springframework.web.multipart.support.MissingServletRequestPartException;

import java.math.BigInteger;

/**
 * 增加方法注入，将含有CurrentUser注解的方法参数注入当前登录用户
 */
@Component
public class CurrentUserMethodArgumentResolver implements HandlerMethodArgumentResolver {
    private final static Logger LOGGER = Logger.getLogger(CurrentUserMethodArgumentResolver.class);
    @Autowired
    private UserDao userDao;

    @Override
    public boolean supportsParameter(MethodParameter parameter) {
        //如果参数类型是User并且有CurrentUser注解则支持
        if (parameter.getParameterType().isAssignableFrom(User.class) &&
                parameter.hasParameterAnnotation(CurrentUser.class)) {
            return true;
        }
        return false;
    }

    @Override
    public Object resolveArgument(MethodParameter parameter, ModelAndViewContainer mavContainer, NativeWebRequest webRequest, WebDataBinderFactory binderFactory) throws Exception {
        //取出鉴权时存入的登录用户Id
        String currentUserId = (String) webRequest.getAttribute(SecurityDic.CURRENT_USER_ID, RequestAttributes.SCOPE_REQUEST);
       LOGGER.info("userId="+currentUserId);
        if (currentUserId != null) {
            //从数据库中查询并返回
            return userDao.findOne(currentUserId);
        }
        throw new MissingServletRequestPartException(SecurityDic.CURRENT_USER_ID);
    }
}
